#include "picojpeg.h"
#include <stdint.h>
#include "render.h"
#include "ff.h"

class image : public bloc{
public:
  virtual image();
  virtual ~image(){};
  void imgToEink(uint8_t* b, uint8_t x, uint8_t y);
  virtual void render(uint8_t* buffer, uint16_t offset, uint8_t cols){}
  virtual void end(){};
  virtual void render(uint8_t* buffer, uint16_t offset, uint8_t cols, uint8_t bpp){}
  virtual int getMinHeight(){}
  virtual int getHeight(){return element_height;}

protected:
    FIL bitmap;
    uint8_t x;
    uint8_t y;
    uint8_t* line_buffer;


};

class jpgImage : public image{
public:
  virtual jpgImage(){}
  virtual jpgImage(char* imgpath);
  virtual ~jpgImage(){}
  int initialize();
  int decodeNextMCU();
  int decode(char*);
  void MCUtoBuffer();
private:
  FIL jpg_file;
  pjpeg_image_info_t jpg_info;
  uint8_t *mcu_buffer;
  uint8_t current_mcu_row, current_mcu_col;

};
