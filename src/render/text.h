#include "render.h"
#include "fonts.h"
/**
 * @addtogroup text
 * \brief Renderització i gestió del text.
 * \ingroup render
 * @{
 */
#ifndef __TEXT__
#define __TEXT__
using namespace std;
struct letter{
  char chr;
  uint16_t y;
};
/** \brief Classe per a gestionar i renderitzar paraules. */
class textWord : public segment{
public:
  textWord(struct font *f = normal_text.f_regular);
  ~textWord(){}
  int readWord(char* c);
  void render(uint8_t* buffer, uint16_t offset, uint8_t cols);
  void renderChar(uint8_t *buffer, letter &l, int offset, uint8_t cols);
  int addChar(uint8_t c);
  void setPosX(int x);
  int getPosX();
protected:

  struct font* w_font;
  letter w_char_list[40];
  uint16_t w_posX;
  uint8_t w_num_chars;
  uint8_t w_last_char;
};


enum justify_t{Left, Right, Center, Fill};
/** \brief Classe per a gestionar i renderitzar línies. */
class line : public bloc{
public:
  line(struct font_group *base_font = &normal_text, justify_t align = Fill, int marginL=10, int marginR=10);
  ~line();
  void render(uint8_t* buffer, uint16_t offset, uint8_t cols);
  int readline(char *c);
  int getMinHeight();

  int addWord(textWord* w);
  textWord* addChar(char c);
  void setPos(int);
  void setBold(bool bold);
  void setItalic(bool italic);
  void toggleBold();
  void toggleItalic();
  void justify();
  void end();
protected:
  int checkWordFit(textWord* w);
  int flush(char c);
  textWord* newWord(char c);

  list<textWord> l_word_list;
  justify_t l_justify;
  struct font* l_font;
  struct font_group* l_fonts;
  uint16_t l_current_width;
  uint16_t l_margin_left;
  uint16_t l_margin_right;

  uint8_t l_word_count;
  bool l_bold;
  bool l_italic;
  textWord* l_current_word;
};
/** \brief Classe per a gestionar i renderitzar paràgrafs. */
class paragraph : public bloc{
public:
  paragraph(struct font_group *base_fonts = &normal_text, justify_t align = Fill, int marginL=10, int marginR=10, int maxHeight=790);
  ~paragraph();
  textWord* addChar(char c);
  void render(uint8_t* buffer, uint16_t offset, uint8_t cols);
  virtual int getMinHeight();
  void setBold(bool bold);
  void setItalic(bool italic);
  void toggleBold();
  void toggleItalic();
  void end();
  int getHeight();
  line* newLine(textWord* w);
protected:
  int checkLineFit(line* w);
  void clear(uint8_t *buffer, uint8_t cols);
  line* tryAppendLine();
  line* newLine(char c);
  void initLine(char c=0);

  list<line> p_line_list;
  justify_t p_justify;
  struct font* p_font;
  struct font_group* p_fonts;
  uint16_t p_margin_left;
  uint16_t p_margin_right;
  uint16_t p_max_height;
  uint8_t p_line_spacing;

  line* p_current_line;
  bool p_bold;
  bool p_italic;


};

class listEntry : public paragraph{
public:
  listEntry(int depth=1, struct font_group *base_fonts = &normal_text, justify_t align = Fill, int marginL=10, int marginR=10, int maxHeight=790);
  void render(uint8_t* buffer, uint16_t offset, uint8_t cols);

private:
  textWord point;
  int e_depth;
};
/**
 * @}
 *
 *
 */

#endif
