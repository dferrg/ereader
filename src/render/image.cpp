#include "image.h"
#include "ff.h"
#include "usbserial.h"
#define JPG_WORK_SZ 3100
//#define f_size(fp) ((fp)->obj.objsize)
static uint8_t work_d[JPG_WORK_SZ];

static int image_filesize;
static int image_fileoffset;

#define min(a,b) ((a<b) ? a : b)
int file_read_size(FIL* f){
  return f_size(f);
}
unsigned char pjpeg_need_bytes_callback(unsigned char* pBuf, unsigned char buf_size, unsigned char *pBytes_actually_read, void *pCallback_data)
{
   uint32_t n, nr;
   pCallback_data;

   n = min(image_filesize - image_fileoffset, buf_size);
   f_read(pCallback_data, pBuf, n, &nr);
   if (n  != nr)
      return PJPG_STREAM_READ_ERROR;
   *pBytes_actually_read = (unsigned char)(nr);
   image_fileoffset += n;
   return 0;
}/*
static int jpg_outfunc(JDEC* out_jdec,uint8_t*out_buffer,JRECT*rect){

  for (int i=rect->top; i<rect->bottom; i++){

    fseek(image_fileout, out_jdec->width*i+rect->left, SEEK_SET);
    std::cout << rect->left << " " << rect->right << " " << rect->top << " " << rect->bottom << std::endl;
    fwrite(&out_buffer[(i-rect->top)*(rect->right-rect->left)], rect->right-rect->left, 1, image_fileout);

  }
  return 1;
}*/

image::image(){

}
void image::imgToEink(uint8_t* buffer, uint8_t x, uint8_t y){

    for (int i=0; i<x; i++)
        for(int j=0; j<y; j+=4){
            uint8_t aux[4];
            aux[0]=0; aux[1]=0; aux[2]=0; aux[3]=0;
            for (int k=0; k<4; k++){
                aux[0] |= ((buffer[i*y+j+k] && 0xC0) >> k*2) ;
                aux[1] |= (((buffer[i*y+j+k] && 0x30) << 2) >> k*2) ;
                aux[2] |= (((buffer[i*y+j+k] && 0x0C) << 4) >> k*2) ;
                aux[3] |= (((buffer[i*y+j+k] && 0x03) << 6) >> k*2) ;
            }
            for (int k=0; k<4; k++)
                buffer[i*y+j+k] = aux[k];

        }
}
jpgImage::jpgImage(char* imgpath):jpgImage(){
  f_open(&jpg_file, imgpath, FA_READ);
}


int jpgImage::initialize(){

  image_fileoffset=0;
    image_filesize=file_read_size(&jpg_file);

  int r;
  if(r=pjpeg_decode_init(&jpg_info, pjpeg_need_bytes_callback, &jpg_file, 0)) {  return -1;}

  jpg_info.m_MCUWidth=jpg_info.m_MCUWidth;
  mcu_buffer= new uint8_t[jpg_info.m_MCUWidth*jpg_info.m_width];
  return 0;
}

void jpgImage::MCUtoBuffer(){

    for (int i=0; i< jpg_info.m_MCUHeight && //for all pixels in mcu
        i+current_mcu_row*jpg_info.m_MCUHeight<jpg_info.m_height; i++){ //as long as they don't exceed the image
      int rowstart=i*jpg_info.m_width;
      rowstart+=current_mcu_col*jpg_info.m_MCUWidth;
      for (int j=0; j< jpg_info.m_MCUWidth && current_mcu_col*jpg_info.m_MCUWidth+j<jpg_info.m_width; j++){
        mcu_buffer[rowstart+j]
          =(jpg_info.m_pMCUBufR[i*jpg_info.m_MCUWidth+j]/*/3
            +jpg_info.m_pMCUBufG[i*jpg_info.m_MCUWidth+j]/3
            +jpg_info.m_pMCUBufB[i*jpg_info.m_MCUWidth+j]/3*/);
      }

  }
}
int jpgImage::decodeNextMCU(){
  if(pjpeg_decode_mcu()) return 0;
  MCUtoBuffer();

  int rw;
  current_mcu_col++;
  if(current_mcu_col==jpg_info.m_MCUSPerRow){
      current_mcu_col=0;
      current_mcu_row++;
      int height= (current_mcu_row<jpg_info.m_MCUSPerCol) ?
                    jpg_info.m_MCUHeight*jpg_info.m_width :
                    jpg_info.m_height-jpg_info.m_MCUHeight*(current_mcu_row-1);
      f_write(&bitmap,mcu_buffer,jpg_info.m_MCUWidth*jpg_info.m_width, &rw);
  }

 // print("MCU DECODED\r\n");

  return 1;
}
int jpgImage::decode(char* imgpath){

  if(f_open(&bitmap,imgpath,FA_CREATE_ALWAYS | FA_WRITE )) return -1;
  print("CREATED FILE\r\n");

  uint8_t z=0;
  uint32_t r;
  //f_expand(&bitmap, jpg_info.m_width*jpg_info.m_height, 1);

  current_mcu_row=current_mcu_col=0;

  print("START DECODING\r\n");

  while(decodeNextMCU());
  print("DECODING ENDED\r\n");

  f_close(&bitmap);
  f_close(&jpg_file);
  print("FILES CLOSED\r\n");
  return 0;
}
char file[]="asdf.jpg";
char fileout[]="asdf.raw";
/*int main(){
  jpgImage a(file);
  a.initialize();
  a.decode(fileout);
}*/
