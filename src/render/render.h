#include "list.hpp"
#include <stdint.h>
using namespace std;
#ifndef __RENDER__
#define __RENDER__ 0

class element;
class block;

/**
 * \defgroup render
 * \addtogroup render
 * @{
 * @copydoc element
 */

/** \brief Classe per a agrupar els elements que conformin una pàgina. */
class bloc;
struct margin{
  uint16_t top;
  uint16_t bottom;
  uint16_t left;
  uint16_t right;
};


class frame{
    public:
        frame();
        ~frame();
        void render(uint8_t* buffer, uint16_t offset, uint8_t cols);
        void render();
        int getHeight();
        void addMargin();
        void addMargin(int margin);
        int appendElement(bloc &next_element, int maxHeight=800);
    protected:
        margin f_margin;
        list<bloc> f_element_list;
        uint16_t f_current_height;
};


/** \brief Classe base per a tots els elements de renderització. */
class element{
    public:
      element();
      virtual void render(uint8_t* buffer, uint16_t offset, uint8_t cols);
      //declarar variables publiques elimina la necessitat de setters i getters que consumeixen memòria.
      uint16_t element_posY;
      uint16_t element_height;
      uint8_t element_bpp;
};


/** \brief Base per als elements de tipus bloc. */
class bloc : public element{
public:
  bloc();
  virtual ~bloc();
  void setBackground(uint8_t color);
  virtual void end(){};
  //void render(uint8_t* buffer, uint16_t offset, uint8_t cols, uint8_t bpp);
  virtual int getMinHeight();
  virtual int getHeight(){return element_height;}
  void setPos(uint16_t offset);
  void setHeight(uint16_t height);
  void setBpp(uint8_t bpp);
protected:
  uint16_t background_color;
};


/** \brief Base per als elements de tipus segment. */
class segment : public element{
public:
  segment(){}
  ~segment(){}
  void render(uint8_t* buffer, uint8_t offset, uint8_t cols);

  uint16_t segment_width;
};
/**
 *
 *@}
 */
 #endif
