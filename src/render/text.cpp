#include "text.h"


/***************************/
/*    INICIALITZADORS      */
/*     I DESTRUCTORS       */
/***************************/
/** \brief Constructor */
textWord::textWord(struct font *f){
  w_font=f;
  w_posX=0;
  w_last_char=' ';
  w_num_chars=0;
  segment_width=0;

}
/** \brief Constructor */
line::line(struct font_group *f, justify_t align, int marginL, int marginR){
  l_fonts=f;
  l_font=f->f_regular;
  l_justify=align;
  l_margin_left=marginL;
  l_margin_right=marginR;
  l_current_width=marginL;
  l_current_word=NULL;
  l_bold=l_italic=false;
  l_word_count=0;
}
/** \brief Destructor */
line::~line(){
}
/** \brief Constructor */
paragraph::paragraph(struct font_group *base_font, justify_t align, int marginL, int marginR, int maxHeight)
:bloc{}
{
  p_fonts=base_font;
  p_font=base_font->f_regular;
  p_justify=align;
  p_margin_left=marginL;
  p_margin_right=marginR;
  p_max_height=maxHeight;
  p_current_line=NULL;
  p_line_spacing=p_font->lineSpacing;
  element_height=(p_font->size-normal_text.f_regular->size)*4;
  p_bold=0;
  p_italic=0;
}
listEntry::listEntry(int depth, struct font_group *base_font, justify_t align, int marginL, int marginR, int maxHeight)
:paragraph(base_font, align, marginL, marginR, maxHeight),
point(base_font->f_regular)
{
  if(depth)
    point.addChar(128+depth%2);

  e_depth=depth;

  point.addChar(' ');
  p_margin_left+=(p_font->size+20)*depth*0.5;
  point.setPosX(p_margin_left-p_font->size*0.7);

}
/** \brief Destructor */
paragraph::~paragraph(){
  //if(p_current_line) delete p_current_line;
}

/***************************/
/*         PARSERS         */
/***************************/

/** \brief Afegeix un caràcter al final de la paraula.
 * @param c caràcter a insertar. Si està fora dels límits de la font es tractarà com un espai.
 *
 * @return NULL o 0 si s'ha inserit el caràcter. 1 si s'ha arribat al final de la paraula.
 */
int textWord::addChar(uint8_t c){
  //check stop conditins
  if(c==' ' || c=='\n' || c=='\0')
    return 0;

  //Inicialitza el nou caracter
  w_char_list[w_num_chars].chr=c;
  if(w_num_chars)
    segment_width+=computeKerning(w_font, w_last_char, c);
  w_char_list[w_num_chars].y=segment_width;

  //Actualitza dades paraula
  segment_width+=getCharInfo(w_font,c)->w;
  w_num_chars++;
  w_last_char=c;
  return 1;
}

/** \brief Llegeix una paraula sencera des d'una string. */
int textWord::readWord(char* c){
  int l=0;
  while(addChar(c[l])) l++;
  return l;
}

/** \brief Afegeix un caràcter a l'ultima paraula.
 * @param c caràcter a insertar. Si està fora dels límits de la font es tractarà com un espai.
 *
 * @return NULL o 0 mentre el caràcter s'insereix correctament.\n
 * Quan acaba una paraula i aquesta no cap a la línia, es retorna el punter a l'objecte textWord, que sempre serà una paraula sencera.
 */
textWord* line::addChar(char c){
  if(flush(c)) return NULL;
  if(!l_current_word)
    l_current_word=newWord(c);

  else if(!l_current_word->addChar(c) || c=='\0' || c=='\n'){ //Add char to word and check stop
    if(checkWordFit(l_current_word)){
      this->l_current_width+=l_current_word->segment_width;
      this->l_current_width+=getCharInfo(l_font,' ')->w;
      l_word_list.append(*l_current_word);
      l_word_count++;
    }else return l_current_word;
    l_current_word=NULL;

   }
   return NULL ;
}
/** \brief Llegeix una línia sencera des d'una string. */
int line::readline(char *c){
  int l=0, l_current_width=l_margin_left;

  while(c[l]!='\0' && c[l]!='\n' && !addChar(c[l])){
    l++;
  }
  return l;
}
/** \brief Salta els caràcters que no puguin formar part de la següent paraula.
 * @return 1 si el caràcter s'ha de saltar. 0 si es pot incloure a la paraula.'
 */
int line::flush(char c){

  if(l_current_word==NULL && (c==' ' || c=='\n' || c=='\0')) return 1;
  return 0;
}
/** \brief Afegeix un caràcter a l'ultima paraula.
 * @param c caràcter a insertar. Si està fora dels límits de la font es tractarà com un espai.
 *
 * @return NULL o 0 mentre el caràcter s'insereix correctament.\n
 * Quan s'intenta iniciar una línia que no cap al paràgraf (fi de pàgina), retorna el punter a un objecte line ja inicialitzat.
 */
textWord *paragraph::addChar(char c){
  if(!p_current_line){
    newLine(c);
    return NULL;
  }
  else{
    textWord* ret=p_current_line->addChar(c);
    if (ret){
       p_current_line->justify();
       if (newLine(ret)){
         ret=new textWord(*ret);
         delete(p_current_line);
         return ret;
       }
    }
  }
  return NULL ;

}


void line::end(){
  //if(p_current_line)
}
void paragraph::end(){
  //if(p_current_line) p_current_line->end();
  //addChar(' ');
  addChar('\n');
  element_height+=20;
}
/*****************************/
/*  STRUCTURE INITIAILZATION */
/*****************************/
/** \brief Afegeix una paraula a l'objecte línia. */
int line::addWord(textWord* w){
  w->setPosX(this->l_current_width);
  w->element_posY=element_posY;
  l_current_width+=w->segment_width+getCharInfo(l_font,' ')->w;
  l_word_count++;
  l_word_list.append(*w);
  return 0;
}
/** \brief Inicialitza una paraula segons la línia actual. */
textWord* line::newWord(char c){
  font*f=l_fonts->f_regular;
  if(l_bold) f=l_fonts->f_bold;
  if(l_italic) f=l_fonts->f_italic;
  textWord* current_word = new textWord(f);
  current_word->element_posY=element_posY;
  current_word->element_height=element_height;
  current_word->element_bpp=element_bpp;
  current_word->setPosX(this->l_current_width);
  current_word->addChar(c);
  return current_word;
}
/** \brief Inicialitza una línia segons el paràgraf actual. */
void paragraph::initLine(char c){
    p_current_line->setHeight(p_font->size);
    p_current_line->setPos(element_height+element_posY);
    p_current_line->setBpp(element_bpp);
    if (c) p_current_line->addChar(c);

    if(p_bold) p_current_line->toggleBold();
    if(p_italic) p_current_line->toggleItalic();

}
line* paragraph::newLine(char c){

  p_current_line = new line(p_fonts,Fill, p_margin_left, p_margin_right);
  initLine(c);
  return tryAppendLine();
}
/** \brief Inicialitza una línia segons el paràgraf actual. */
line* paragraph::newLine(textWord*w){

  p_current_line = new line(p_fonts,Fill, p_margin_left, p_margin_right);
  initLine();
  p_current_line->addWord(w);
  return tryAppendLine();
}

/** \brief Comprova si la paraula cap al final de la línia.
 * @return 1 si hi cap, 0 si no.
 */
int line::checkWordFit(textWord* w){
  return this->l_current_width+w->segment_width+l_margin_right<600;
}

/** \brief Comprova si la línia cap al final del paràgraf.
 * @return 1 si hi cap, 0 si no.
 */
int paragraph::checkLineFit(line* w){
  return element_posY+element_height+p_font->size+p_line_spacing<p_max_height;
}

line* paragraph::tryAppendLine(){
  if(checkLineFit(p_current_line)){
    p_line_list.append(*p_current_line);
    element_height+=p_font->size+p_line_spacing;
    return NULL;
  }else return p_current_line;
}
/** \brief Corregeix els espais en funció de la justificació de la línia. */
void line::justify(){
  int remain=600-l_margin_right-l_current_width+getCharInfo(l_font,' ')->w;
  item<textWord>* it=l_word_list.getFirst();
  int recorregut=l_word_list.getLast()->get()->getPosX()-l_margin_left;
  int l=0;
  float adj=((float)remain)/((float)l_word_count-1);

  while(it!=NULL){
    int cx=it->get()->getPosX();
    switch(l_justify){
      case Fill:
        it->get()->setPosX(cx+adj*l);//(remain*(cx-l_margin))/recorregut);
        l++;
        break;
      case Right:
        it->get()->setPosX(cx+remain);
        break;
      case Center:
        it->get()->setPosX(cx+remain/2);
        break;
    }
    it=it->getNext();
  }
}

/*************************/
/*     RENDERITZACIO     */
/*************************/

/** \brief Renderitza la regió indicada del caràcter sobre el buffer.
 *
 * @param buffer Array sobre la que escriure les dades. S'assumeix que el format bpp coincideix amb el de la classe.
 * @param l Objecte letter a escriure.
 * @paeam offset Desplaçament del buffer sobre la pantalla en columnes.
 * @param cols Nombre màxim de columnes a escriure
*/
void textWord::renderChar(uint8_t *buffer, letter &l, int offset, uint8_t cols){
  struct font_char* ci=getCharInfo(w_font, l.chr);
  uint8_t *data=&(w_font->bitmaps[ci->bitmapOffset]);

  for(int i=(offset<0 ? -offset : 0); (i+offset)<cols && i<ci->w; i++)
    for(int j=0; j<ci->h; j++)
      buffer[(i+offset)*200 + j+element_posY/4+ci->posY] |=
        data[i*ci->h + j];
}
/**
 * @copydoc element::render()
 */
void textWord::render(uint8_t* buffer, uint16_t offset, uint8_t cols){

  //item<letter>* c=w_char_list.getFirst();
  int ind=0;
  while(w_char_list[ind].y+w_posX < offset+cols){
     this->renderChar(buffer, w_char_list[ind], w_char_list[ind].y+w_posX-offset, cols);
    ind++;
    if(ind==w_num_chars) return;
  }
}
/**
 * @copydoc element::render()
 */
void line::render(uint8_t* buffer, uint16_t offset, uint8_t cols){
  item<textWord>* it=l_word_list.getFirst();

  while(it!=NULL){

    it->get()->render(buffer, offset, cols);
    it=it->getNext();
  }
}
/** \brief Esborra la regió del buffer corresponent al paràgraf. */
void paragraph::clear(uint8_t *buffer, uint8_t cols){
  for (int i=(element_posY)/4; i<(element_posY+element_height)/4; i+=1)
    for (int j=0; j<cols; j++)
      buffer[j*200+i]=0;

}
/**
 * @copydoc element::render()
 */
void paragraph::render(uint8_t* buffer, uint16_t offset, uint8_t cols){
  //clear(buffer,cols);
  item<line>* c_l=p_line_list.getFirst();
  while(c_l!=NULL){
    c_l->get()->render(buffer, offset, cols);
    c_l=c_l->getNext();
  }
}
void listEntry::render(uint8_t* buffer, uint16_t offset, uint8_t cols)
{
  point.element_posY=element_posY;
  if(e_depth)
  point.render(buffer, offset, cols);
  paragraph::render(buffer,offset,cols);
}
/************************/
/*      SETTERS &       */
/*       GETTERS        */
/************************/

void textWord::setPosX(int x){
  w_posX=x;
}
int textWord::getPosX(){
  return w_posX;
}
void line::setBold(bool bold){
  l_bold=bold;
}
void line::setPos(int pos){
    element_posY=pos;
    item<textWord>* w=l_word_list.getFirst();
    while(w){
      w->get()->element_posY=pos;
      w=w->getNext();
    }
}
void line::setItalic(bool italic){
  l_italic=italic;
}
void line::toggleBold(){
  l_bold=!l_bold;
}
void line::toggleItalic(){
  l_italic=!l_italic;
}
int line::getMinHeight(){
  return l_fonts->f_regular->size;
}
int paragraph::getMinHeight(){
  return p_fonts->f_regular->size+p_line_spacing+element_height;
}
void paragraph::setBold(bool bold){
  p_bold=bold;
  if(p_current_line) p_current_line->setBold(bold);
}
void paragraph::setItalic(bool italic){
  p_italic=italic;
  if(p_current_line) p_current_line->setItalic(italic);
}
void paragraph::toggleBold(){
  p_bold=!p_bold;
  if(p_current_line) p_current_line->setBold(p_bold);

}
void paragraph::toggleItalic(){
  p_italic=!p_italic;
  if(p_current_line) p_current_line->setItalic(p_italic);

}

int paragraph::getHeight(){
  return element_height;
}
