#include "render.h"
#include "fonts.h"
#include "text.h"
#include "epdsys.h"
#include <stdlib.h>

frame::frame(){
  f_margin.top=10;
  f_margin.bottom=10;
  f_margin.left=10;
  f_margin.right=10;

  f_current_height=f_margin.top;
}

frame::~frame(){
}
/** \brief Renderitza la regió indicada de cada element sobre el buffer.
 *
 * @param buffer Array sobre la que escriure les dades. S'assumeix que el format bpp coincideix amb el de la classe.'
 * @param offset Desplaçament del buffer sobre la pantalla en columnes.
 * @param cols Nombre màxim de columnes a escriure
*/
void frame::render(uint8_t* buffer, uint16_t offset, uint8_t cols){
  for (int i=0; i<cols*200; i++)
      buffer[i]=0;
  item<bloc>* e=f_element_list.getFirst();
  while(e!=NULL){
    e->get()->render(buffer, offset, cols);
    e=e->getNext();
  }
}

void frame::render(){

  writeArea_t a={0,0,600,800};
  uint8_t *buff=s_eink_start_write(a, 2);
  for (int i=0; i<600; i+=LINES_PER_ITERATION){
    render(buff,600-LINES_PER_ITERATION-i,LINES_PER_ITERATION);
    //for(int j=0; j<2000; j++) buff[j]=0xff;
    buff=s_eink_write_buffer();
  }
}

/** \brief Retorna l'altura actual del frame. */
int frame::getHeight(){
  f_current_height=f_margin.top;
  item<bloc>* e=f_element_list.getFirst();
  while(e!=NULL){
    f_current_height+=e->get()->getHeight();
    //f_current_height+=e->get()->b_margin.bottom;
    e=e->getNext();
  }
  return f_current_height;
}

/** \brief Afegeix un element al frame i l'inicialitza.
 * @param next_element Element allotjat en memòria dinàmica.
 *
 * @return Retorna 0 si l'element es pot afegir. Si no hi cap retorna 1.
 */
int frame::appendElement( bloc &next_element, int max){
  f_current_height=getHeight();

  next_element.setPos(f_current_height);
  if(f_current_height+f_margin.bottom+next_element.getMinHeight()<max){
    f_element_list.append(next_element);
    return 1;
  }else return 0;
}
/** \brief Afegeix un marge en alçada després de l'últim element. */
void frame::addMargin(){

}
element::element(){

}


/** \brief Defineix els bits per pixel de l'element. */
void bloc::setBpp(uint8_t bpp){
  element_bpp=bpp;
}
/** \brief Defineix la posició vertical de l'element. */
void bloc::setPos(uint16_t offset){
  element_posY=offset;
}
/** \brief Defineix l'altura de l'element. */
void bloc::setHeight(uint16_t height){
  element_height=height;
}

bloc::bloc(){
  element_bpp=2;
  element_posY=0;
  //exit(0);
}

bloc::~bloc(){

}

int bloc::getMinHeight(){
  return 0;
}
void bloc::setBackground(uint8_t color){
  background_color=color;
}
/** \brief Renderitza la regió indicada de l'element sobre el buffer.
 *
 * @param buffer Array sobre la que escriure les dades. S'assumeix que el format bpp coincideix amb el de la classe.'
 * @param offset Desplaçament del buffer sobre la pantalla en columnes.
 * @param cols Nombre màxim de columnes a escriure
*/
void element::render(uint8_t* buffer, uint16_t offset, uint8_t cols){
  struct font* f=normal_text.f_regular;
  struct font_char* ci=&(f->charLookUp['M'-' ']);
  uint8_t *data=&(f->bitmaps[ci->bitmapOffset]);

  for (int i=0; i<cols; i++ ){
    for (int j=element_posY; j<element_posY+11; j+=4){
      buffer[i*100*element_bpp + (j*element_bpp)/8] =data[i*3+(j-element_posY)/4];//|= background_color << (j%((8/c_bpp)))*c_bpp;
    }
  }
}
/**
 * @copydoc element::render()
 */
 /*
void bloc::render(uint8_t* buffer, uint16_t offset, uint8_t cols){
  struct font* f=normal_text.f_regular;
  struct font_char* ci=&(f->charLookUp['M'-' ']);
  uint8_t *data=&(f->bitmaps[ci->bitmapOffset]);

  for (int i=0; i<cols; i++ ){
    for (int j=element_posY; j<element_posY+11; j+=4){
      buffer[i*100*element_bpp + (j*element_bpp)/8] =data[i*3+(j-element_posY)/4];//|= background_color << (j%((8/c_bpp)))*c_bpp;
    }
  }
}*/
