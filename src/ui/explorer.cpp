#include "explorer.h"
#include "usbserial.h"
#include "markdown.h"
dir_explorer::dir_explorer(char* fn){
    e_directory=new DIR_t;

    char buff[10];
    int re;
    print("\n\rFatFs open dir: ");

    re=f_opendir(e_directory,fn);
    //f_rewinddir(e_directory);

    itoa(re,buff, 16);
    print("\r\n");
    print("\n\rFatFs open dir: ");
    print("0x");
    print(buff);
    print("\n\r");
    print("\r\n");
    e_current_page=0;
}
dir_explorer::dir_explorer(DIR_t* dir){
    e_directory=dir;
    e_current_page=0;
}
explorer::explorer(char* fn){
    //e_directory=new DIR_t;

    //f_opendir(e_directory,fn);
    int i;
    for(i=0; i<260 && fn[i]!=0; i++)
        current_filename[i]=fn[i];
    if(current_filename[i-1]=='/') i--;
    current_filename[i]=0;
    base_dir_len=i;
}
uint8_t dir_explorer::checkType(char*s){
    int last_p=0;
    for (int i=0; i<255 && s[i]!='\0'; i++) if(s[i]=='.') last_p=i;
    if(s[last_p+1]=='m' && s[last_p+2]=='d' && s[last_p+3]==0) return 1;
    return 0;
}
uint8_t dir_explorer::checkHidden(char* s){
    return s[0]=='.';
}


int dir_explorer::fillPage(int index){
    e_start_point.append(*(new int(index)));

    e_current_menu=new menu(index>0);
    FILINFO* fi=new FILINFO;
    f_rewinddir(e_directory);
    for (int i=0; i<index;i++){
        if(f_readdir(e_directory, fi)) return 0;
    }

    while(!f_readdir(e_directory, fi)){
        if(!fi->fname[0]) return index;
        index++;
        if(!checkType(fi->fname) && fi->fattrib!=AM_DIR) continue;
        if(checkHidden(fi->fname)) continue;

        if(!e_current_menu->addOption(fi->fname,index)){
            e_current_menu->addNextOption();
            return index;
        }
    }
    return index;
}
FILINFO* dir_explorer::readEntry(int index){
    FILINFO* fi=new FILINFO;
    f_rewinddir(e_directory);
    for (int i=0; i<index-1;i++){
        if(f_readdir(e_directory, fi)) return 0;
    }
    if(!f_readdir(e_directory, fi))
        return fi;

    return NULL;
}
FILINFO* dir_explorer::start(){
    int index=0;
    do{
        index=fillPage(index);
        int mv=e_current_menu->start();
        delete e_current_menu;

        switch(mv){
            case MENU_NEXT_PAGE: break;
            case MENU_PREV_PAGE:
                e_start_point.pop();
                index=e_start_point.pop();
                break;
            case MENU_BACK:
                return NULL;
            default:
                return readEntry(mv);
        }
    }while(1);
    return 0;
}
DIR_t* dir_explorer::getDir(){
    return e_directory;
}
static void strConcat(char* s1, char* s2, int maxlen){
    int i1, i2;
    for (i1=0; i1<maxlen && s1[i1]!=0; i1++);
    for (i2=0; s2[i2]!=0 && i1+i2<maxlen; i2++)
        s1[i1+i2]=s2[i2];
    s1[i1+i2]=0;
}
void explorer::enterDir(FILINFO* fi){
    strConcat(current_filename, "/", 260);
    strConcat(current_filename, fi->fname,260);
}

void explorer::back(){
    int lastslash=0, i1;
    for (i1=0; i1<260 && current_filename[i1]!=0; i1++)
        if(current_filename[i1]=='/' && current_filename[i1+1]!=0) lastslash=i1;
    current_filename[lastslash]=0;
}

int explorer::start(){
    FILINFO* fi;
    while(1){

        directory=new dir_explorer(current_filename);
        print(current_filename);
        fi=directory->start();
        if(fi==NULL){
            back();
        }else if(fi==MENU_BACK){
            back();
        }
        else if(fi->fattrib==AM_DIR){
            print("ENTER DIR");
            enterDir(fi);
            delete(fi);

        }else{
          print("OPENING DOCUMENT");
          enterDir(fi);
          print(current_filename);
          mdParser* m=new mdParser(current_filename);
          m->start();
          delete(fi);
          delete(m);
          back();
        }
        delete directory;

    }

    return 0;
}
