#include "text.h"

class menuentry : public paragraph{
public:
  menuentry(void*d=0);
  ~menuentry();
  void render(uint8_t* buffer, uint16_t offset, uint8_t cols);
  void select(){me_highlight=true;}
  void deselect(){me_highlight=false;}
  void setHihglight(bool h){me_highlight=h;}
  virtual int getMinHeight();
  void* ret(){ return me_data;};
private:
  bool me_highlight;
  void* me_data;

};
#define MENU_PREV_PAGE -3
#define MENU_NEXT_PAGE -2
#define MENU_BACK      -1

class menu : public frame{
public:
  menu(bool back=false);
  virtual ~menu();
  uint8_t addOption(char* name, void* return_data);
  void addPrevOption();
  void addNextOption(){m_nextp=1;}
  void* start();
  menuentry* getEntryAt(int n);

protected:
    int active_element;
    int n_items;
    bool m_prevp;
    bool m_nextp;

};
