#include "menu.h"
#include "ff.h"

#ifdef PC_SIM
typedef DIR *DIR_t;
#else
typedef DIR DIR_t;
#endif

class dir_explorer{
public:
    dir_explorer(char* directory);
    dir_explorer(DIR_t*);
    FILINFO* start();
    DIR_t* getDir();
    uint8_t checkType(char*);
    uint8_t checkHidden(char*);
    int show_page(int pageStart);
    int fillPage(int index);
    FILINFO* readEntry(int n);
private:
    menu* e_current_menu;
    DIR_t* e_directory;
    list<int> e_start_point;
    int e_current_page;
};
class explorer{
public:
    explorer(char* directory);
    void enterDir(FILINFO*);
    void back();
    int start();
    void construct_filename();
    uint8_t a;
private:
    dir_explorer* directory;
    char current_filename[260];
    int base_dir_len;
    DIR_t* e_directory;

};
