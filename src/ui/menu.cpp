#include "menu.h"
#include "buttons.h"
#include "epdsys.h"
menuentry::menuentry(void* data)
:paragraph()
{
  me_highlight=false;
  element_height+=20    ;
  me_data=data;
}

menuentry::~menuentry(){

}

void menuentry::render(uint8_t* buffer, uint16_t offset, uint8_t cols)
{
  paragraph::render(buffer, offset, cols);
  for(int i=0; i<cols; i++){
    buffer[i*200+element_posY/4]=0xC0;
  }

  if(me_highlight){
    for (int i=(element_posY)/4; i<(element_posY+element_height)/4; i+=1)
      for (int j=0; j<cols; j++){
        uint16_t c=0xaa^(buffer[j*200+i]);
        buffer[j*200+i]=c;
      }
  }
}

int menuentry::getMinHeight(){
    return paragraph::getMinHeight()+20;
}
menu::menu(bool p){
    active_element=0;
    n_items=0;
    m_prevp=p;
}
menu::~menu(){

}

uint8_t menu::addOption(char* s, void* d){
    menuentry* m=new menuentry(d);
    if(f_element_list.getFirst()==NULL) m->select();
    if(!appendElement(*m, 800)) { delete m; return 0;}
    while ((*s)!=0){
        m->addChar(*s);
        s++;
    }
    m->end();
    n_items++;
    return 1;
}

menuentry* menu::getEntryAt(int n){
    list<bloc>* es=&f_element_list;
    return es->getItemAt(active_element)->get();
}
void* menu::start(){
    int rr=0;
    writeArea_t a={0,0,0,0};
    s_eink_clear(a, 0x0F);
    s_eink_clear(a, 0x0F);
    render();
    render();
    while(1){
        rr=0;
        button_event e;
        while(xQueueReceive(buttonsQueue, &e, 200000)==pdFALSE);

        if(e==BTN_DWN){
            getEntryAt(active_element)->deselect();
            if(active_element+1<n_items || (!m_nextp && !m_prevp)) active_element=(active_element+1)%n_items;
            else if(m_nextp) return MENU_NEXT_PAGE;

            getEntryAt(active_element)->select();
            rr=1;
        }
        if(e==BTN_UP){
            getEntryAt(active_element)->deselect();
            if(active_element>0 || (!m_nextp && !m_prevp)) active_element=active_element>0 ? active_element-1 : n_items-1;
            else if(m_prevp) return MENU_PREV_PAGE;

            getEntryAt(active_element)->select();
            rr=1;
        }
        if(e==BTN_OK) return getEntryAt(active_element)->ret();

        if(e==BTN_BACK) return MENU_BACK;

        if (rr){

          writeArea_t a={0,0,0,0};
          s_eink_clear(a, 0x0F);
          s_eink_clear(a, 0x0F);
          render();
          render();
        }
    }
}
