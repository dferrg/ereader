#include "ff.h"
struct dbdata{
    uint32_t key;
    uint16_t crc;
};

unsigned int MurmurHash2 ( const void * key, int len, unsigned int seed );

#define hash(data, len) MurmurHash2(data, len, 0x12345678)

uint32_t hashStr(char* data);

uint16_t calcula_checksum(uint16_t* data, uint16_t len);

#define DB_OK           0
#define DB_NOT_FOUND    1
#define DB_FS_ERROR     2
#define DB_CRC_ERROR    3

class dbtable{
public:
    dbtable(char* table_name, uint16_t item_size);
    uint8_t getEntry(uint32_t key, dbdata* buffer);
    uint8_t getEntry(char * string_key, dbdata* buffer);
    uint8_t addEntry(uint32_t key, dbdata * data);
    uint8_t addEntry(char * string_key, dbdata * data);
protected:
    uint8_t readNextEntry(FIL*, uint32_t *buff);
    uint8_t readNextHeader(FIL*, uint32_t *buff);

    void copyBuffer(uint32_t *dst,uint32_t *src);
    uint16_t size;
    char path[260];
};  
