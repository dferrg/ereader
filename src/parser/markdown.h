#include "parser.h"

/**
 * \addtogroup parser
 *
 * @{
 */

/** \brief Classe per a la lectura de fitxers tipus markdown o txt.
 */
class mdParser : public parser{
public:
    mdParser(char* filepath);
    int readPage(int page, int save=1);
    int setPage(int page);
    void render(uint8_t *buff, uint16_t offset, uint8_t cols);
    void render();
    void start();
protected:
  void saveProgress(int page);
  int parse_paragraph(uint8_t r, font_group* fg=NULL);
  int parse_title();
  int parse_body();
  int checkBoldItalic(char c);
  font_group* getTitleFont(int n);
  uint32_t history_index;
  char last_char;
  bool underscore_italic;
};

/**@}*/
