#include "list.hpp"
#include "ff.h"
#include "render.h"
#include "text.h"

/**
 * \addtogroup parser
 *
 * @{
 */

/** \brief Classe base per als lectors de documents.
 */
#define ELEMENT_BODY        0x01
#define ELEMENT_TITLE       0x02
#define ELEMENT_PARAGRAPH   0x03
#define ELEMENT_IMAGE       0x04
#define ELEMENT_BLOCK       0x05
#define ELEMENT_BOLD        0x06
#define ELEMENT_ITALIC      0x07

typedef uint8_t element_type;
class parser{
public:
    parser(){}
    parser(char* filepath){}
    ~parser(){if(p_frame) delete(p_frame);}
    virtual int readPage(int page){};
    virtual int setPage(int page){};
    virtual void render(uint8_t *buff, uint16_t offset, uint8_t cols){};
    virtual void render(){};
    virtual void start(){};
protected:
    frame *p_frame;
    frame *p_next_frame;
    bloc *p_current_element;
    int p_current_page;
    element_type p_element_tree[20];
    uint8_t p_current_depth;
    FIL p_file;
};

/**@}*/
