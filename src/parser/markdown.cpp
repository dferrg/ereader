#include "markdown.h"
#include "epdsys.h"
#include "buttons.h"
#include "db.h"
#include "stdio.h"
struct mdhist : dbdata{
   int page;
};
/** \brief Inicialitzador.
 * Obre l'arxiu filepath i llegeix la primera pàgina.
 */
mdParser::mdParser(char* filepath){
    f_open(&p_file, filepath, FA_READ);
    p_current_page=0;
    p_current_depth=0;
    p_element_tree[p_current_depth] = ELEMENT_BODY;
    p_current_element=NULL;
    p_frame=NULL;
    underscore_italic=0;

    dbtable hist("md_hist", sizeof(mdhist));
    mdhist entry;
    int r;
    if(r=hist.getEntry(filepath, &entry)){
        readPage(1,0);
        entry.page=1;
        hist.addEntry(filepath, &entry);
    }else{
        printf("   %u\r\n",entry.page);

        readPage(entry.page,0);
    }
    history_index=entry.key;

}

void mdParser::saveProgress(int page){
    dbtable hist("md_hist", sizeof(mdhist));
    mdhist entry;
    entry.page=page;
    hist.addEntry(history_index, &entry);

}
/** \brief Llegeix una pàgina des de l'arxiu.
 * Aquesta funció només llegeix l'arxiu i la desa a memòria, no la imprimeix per pantalla.
 * **Nota: Per llegir una pàgina s'han d'haver llegit totes les anteriors.
 * @params page Número de la pàgina a carregar, començant per la 1.
 *
 * @return 0 si la pàgina s'ha carregat correctament.
 */

int mdParser::readPage(int page, int save){
    printf("%u\r\n",page);

    if(p_current_page==page) return 0;
    if(page<1)return 0;
    //if(p_current_page!=page-1) return -1;
    if(save){ saveProgress(page);}

    if(page<p_current_page | page>p_current_page+1) readPage(page-1,0);
    printf(" %u\r\n",page);

    if(page==1){
        f_rewind(&p_file);
        p_current_depth=0;
        p_element_tree[p_current_depth] = ELEMENT_BODY;
        p_current_element=NULL;
        underscore_italic=0;
    }
    p_current_page=page;
    if(p_frame) delete p_frame;
    p_frame=new frame;
    while(1)
    switch(p_element_tree[p_current_depth] ){
        case ELEMENT_BODY:
            if(parse_body()) return 1;
            break;
        case ELEMENT_PARAGRAPH:
            if(parse_paragraph(0)) return 1;
    }
    return 0;
}
int mdParser::parse_body(){
  char c;
  int read;
  uint8_t nspaces=0;
  while(c=utf2iso(&p_file)){
    if(c==' ' || c=='\t'){ nspaces++; continue;}
    switch(c){
      case '\n': break;
      case '#':
        p_current_depth+=1;
        p_element_tree[p_current_depth]=ELEMENT_PARAGRAPH;
        p_element_tree[p_current_depth+1]=0;
        if(parse_title()) return 1;
        break;
      case '-':
      case '*':
      case '+':
        p_current_depth+=1;
        p_element_tree[p_current_depth]=ELEMENT_PARAGRAPH;
        p_element_tree[p_current_depth+1]=0xfc&(nspaces<<2);
        if(parse_paragraph(0)) return 1;
        break;
      default:

        p_current_depth+=1;
        p_element_tree[p_current_depth]=ELEMENT_PARAGRAPH;
        p_element_tree[p_current_depth+1]=0;
        if(parse_paragraph(c)) return 1;
        break;
    }
    nspaces=0;
  }
  return 1;
}

int mdParser::parse_paragraph(uint8_t r, font_group* fg){

    if(!fg) fg=&normal_text;
    if(!(p_element_tree[p_current_depth+1]&0x03)){
        p_current_element = new listEntry(p_element_tree[p_current_depth+1]>>2,fg);
        p_element_tree[p_current_depth+1]|=1;

        if(!p_frame->appendElement(*p_current_element)) {
          p_current_element->setPos(10);
          if(r) ((paragraph*)p_current_element)->addChar(r);
          return 1;
        }
    }else{
      p_frame->appendElement(*p_current_element);
      p_element_tree[p_current_depth+1]|=1;
    }

    uint8_t c;
    c=r;
    while (c==' ' || c=='\n' || c== 0) c=utf2iso(&p_file);

    textWord *resta=NULL;;

    do{

        switch(c){
          case '\n':
            ((paragraph*)p_current_element)->addChar(c);
            if(testChar(&p_file)=='\n')
              ((paragraph*)p_current_element)->end();
            p_element_tree[p_current_depth+1]=0;
            p_element_tree[p_current_depth]=0;
            p_current_depth-=1;

            return 0;
          case '\\':
            if(c=utf2iso(&p_file))
              ((paragraph*)p_current_element)->addChar(c);
            break;
          case '*':
          case '_':
            if(checkBoldItalic(c))
              break;
          default:
            resta=((paragraph*)p_current_element)->addChar(c);

            if(resta){
              p_current_element = new listEntry(p_element_tree[p_current_depth+1]>>2,fg);
              p_current_element->setPos(10);
              ((paragraph*)p_current_element)->newLine(resta);
              p_element_tree[p_current_depth+1]|=1;
              return 1;
            }
        }
        last_char=c;
    }while(c=utf2iso(&p_file));

    p_element_tree[p_current_depth+1]=0;
    p_element_tree[p_current_depth]=0;
    p_current_depth-=1;

    return 1;
}

int mdParser::checkBoldItalic(char c){
  if(last_char=='\\') return 0;
  if(c=='_' && last_char!=' ' && !underscore_italic) return 0;
  char c2;
  int read;
  if(c2=utf2iso(&p_file)){
    if(c==c2) ((paragraph*)p_current_element)->toggleBold();
    else{
      if(c=='_') underscore_italic^=1;
      ((paragraph*)p_current_element)->toggleItalic();
      ((paragraph*)p_current_element)->addChar(c2);
    }
  }
  return 1;
}
int mdParser::parse_title(){
    int t=1;
    char c;
    int read;
    while(c=utf2iso(&p_file)){
      if(c=='#') t++;
      else break;
    }
    return parse_paragraph(c,getTitleFont(t));
}

font_group* mdParser::getTitleFont(int n){
  switch(n){
    case 1: return &title;
    case 2: return &title2;
    case 3: return &title3;
    case 4: return &title4;
    case 5: return &title5;
    case 6: return &title6;
    default: return &normal_text;
  }
}
int mdParser::setPage(int page){

}
/** \brief Renderitza la regió indicada de la pàgina actual sobre el buffer.
 *
 * @param buffer Array sobre la que escriure les dades. S'assumeix que el format bpp coincideix amb el de la classe.
 * @paeam offset Desplaçament del buffer sobre la pantalla en columnes.
 * @param cols Nombre màxim de columnes a escriure
*/
void mdParser::render(uint8_t *buff, uint16_t offset, uint8_t cols){

}
/** \brief Renderitza la pantalla sencera. */
void mdParser::render(){
  if(p_frame) p_frame->render();
}

void mdParser::start(){
    writeArea_t a={0,0,600,0};
    s_eink_clear_cycle(a, 0x0F);
    int page=p_current_page;
    button_event e;

    readPage(page);
    render();
    render();
    while(1){
        while(xQueueReceive(buttonsQueue, &e, 200000)==pdFALSE);

        if(e==BTN_DWN){
            s_eink_clear_cycle(a, 0x0F);
            page++;
            readPage(page);
            render();
            render();
        }
        if(e==BTN_BACK){
            return;
        }
        if(e==BTN_UP){
            s_eink_clear_cycle(a, 0x0F);
            page--;
            readPage(page);
            render();
            render();

        }
    }

    vTaskDelay(10000);
}
