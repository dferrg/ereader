#include "parser.h"

/**
 * \addtogroup parser
 *
 * @{
 */

/** \brief Classe per a la lectura de fitxers tipus html.
 */
class htmlParser : public parser{
public:
    htmlParser(char* filepath);
    int readPage(int page);
    int setPage(int page);
    void render(uint8_t *buff, uint16_t offset, uint8_t cols);
    void render();
protected:
  int parse_paragraph(uint8_t r, font_group* fg=NULL);
  int parse_title();
  int parse_body();
  font_group* getTitleFont(int n);
};

/**@}*/
