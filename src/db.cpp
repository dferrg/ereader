#include "db.h"
#include <stdio.h>
unsigned int MurmurHash2 ( const void * key, int len, unsigned int seed )
{
	// 'm' and 'r' are mixing constants generated offline.
	// They're not really 'magic', they just happen to work well.

	const unsigned int m = 0x5bd1e995;
	const int r = 24;

	// Initialize the hash to a 'random' value

	unsigned int h = seed ^ len;

	// Mix 4 bytes at a time into the hash

	const unsigned char * data = (const unsigned char *)key;

	while(len >= 4)
	{
		unsigned int k = *(unsigned int *)data;

		k *= m;
		k ^= k >> r;
		k *= m;

		h *= m;
		h ^= k;

		data += 4;
		len -= 4;
	}

	// Handle the last few bytes of the input array

	switch(len)
	{
	case 3: h ^= data[2] << 16;
	case 2: h ^= data[1] << 8;
	case 1: h ^= data[0];
	        h *= m;
	};

	// Do a few final mixes of the hash to ensure the last few
	// bytes are well-incorporated.

	h ^= h >> 13;
	h *= m;
	h ^= h >> 15;

	return h;
}
uint32_t hashStr(char* str){
    int i;
    for(i=0; str[i]!=0; i++);
    if(i>0) if(str[i-1]=='/') i--;
    return hash(str, i);
}
uint16_t calcula_checksum(uint16_t* data, uint16_t len){
    uint32_t cs=0;
    for(int i=0; i<len/2; i++){
        cs+=data[i];
        cs=cs+(cs>>16);
        cs&=0xFFFF;
    }
    return ~((int16_t)cs);
}


static void strConcat(char* s1, char* s2, int maxlen){
    int i1, i2;
    for (i1=0; i1<maxlen && s1[i1]!=0; i1++);
    for (i2=0; s2[i2]!=0 && i1+i2<maxlen; i2++)
        s1[i1+i2]=s2[i2];
    s1[i1+i2]=0;
}

dbtable::dbtable(char* table_name, uint16_t item_size){
    if(f_stat(".cache", NULL)) f_mkdir(".cache");

    path[0]=0;
    strConcat(path,".cache/", 260);
    strConcat(path, table_name, 260);


    FIL table_file;

    if(f_stat(path, NULL)){
        f_open(&table_file, path, FA_WRITE| FA_OPEN_APPEND);

        f_close(&table_file);
    }

    size=item_size;
}
uint8_t dbtable::getEntry(uint32_t key, dbdata* data){
    FIL table_file;
    f_open(&table_file, path, FA_READ );

    while(readNextEntry(&table_file, (uint32_t*)data)){

        if(data->key==key){
            f_close(&table_file);
            if(calcula_checksum((uint16_t*)data, size)) return DB_CRC_ERROR;
            else return DB_OK;
        }
    }
    f_close(&table_file);
    return DB_NOT_FOUND;
}
uint8_t dbtable::getEntry(char * string_key, dbdata* buffer){

    return getEntry(hashStr(string_key), buffer);
}
uint8_t dbtable::addEntry(uint32_t key, dbdata * data){
    FIL table_file;
    int pos=0, written;
    dbdata aux;

    f_open(&table_file, path, FA_READ | FA_WRITE);
    f_rewind(&table_file);
    while(readNextHeader(&table_file, (uint32_t*)&aux)){

        if(aux.key==key){
            break;
        }
        pos+=size;
    }

    f_lseek(&table_file,pos);
    data->key=key;

    data->crc=0;
    data->crc=calcula_checksum((uint16_t*)data, size);
    f_write(&table_file, (uint32_t*)data, size, &written);
    f_close(&table_file);

    if(written!=size) return DB_FS_ERROR;
    return DB_OK;
}
uint8_t dbtable::addEntry(char * string_key, dbdata * data){
    return addEntry(hashStr(string_key), data);

}

uint8_t dbtable::readNextEntry(FIL* fp, uint32_t *buff){
    int read=0;

    f_read(fp, buff, size, &read);
    return read;
}
uint8_t dbtable::readNextHeader(FIL* fp, uint32_t *buff){
    int read=0;
    uint16_t g;
    f_read(fp, buff, sizeof(dbdata), &read);

    if(!read) return 0;
    while (read<size){
        int lr=0;
        f_read(fp, &g, 2, &lr);
        read+=2;
    }

    return read;
}
