#include <cstddef>
#ifndef LIST_CLASS
#define LIST_CLASS
template <class T> class item{
public:
  item(T &data);
  ~item();
  T* get();
  void setNext(item<T>* next);
  item<T>* getNext();

private:
  T* i_data;
  item<T> *i_next;
};
template <class T> class list{
public:
  list(){
    first=NULL;
    }

  ~list();
  void append(T &data);
  item<T>* getItemAt(int n);
  
  T getValueAt(int n);
  item<T>* getFirst();
  item<T>* getLast();
  T pop();

private:
  item<T>* first;
};


template <class T>
item<T>::item(T &data){
    i_data=&data;
    i_next=NULL;
}

template <class T>
item<T>::~item(){
  delete i_data;
}

template <class T>
T* item<T>::get(){
    return i_data;
}

template <class T>
void item<T>::setNext(item<T>* next){
    i_next=next;
}

template <class T>
item<T>* item<T>::getNext(){
    return i_next;
}

template <class T>
list<T>::~list(){
    item<T> *c=first;
    item<T> *aux;

    if(c!=NULL)
      do{
        aux=c->getNext();
        delete c;
        c=aux;
      }while(c!=NULL);
    first=NULL;

}
template <class T>
void list<T>::append(T &data){
    if(first==NULL){
        first=new item<T>(data);
    }else{
        item<T> *c=first;
        while(c->getNext()!=NULL){
            c=c->getNext();
        }
        c->setNext(new item<T>(data));
    }
}

template <class T>
item<T>* list<T>::getItemAt(int n){
    item<T>* i=getFirst();
    while(i && n){
        i=i->getNext();
        n--;
    }
    return i;
}

template <class T>
item<T>* list<T>::getLast(){
    item<T> *c=first;
    if(!c) return NULL;
    while(c->getNext()!=NULL){
        c=c->getNext();
    }
    return c;
}

template <class T>
T list<T>::getValueAt(int n){
    return this.getItemAt(n)->get();
}

template <class T>
item<T>* list<T>::getFirst(){
    return first;
}
template <class T>
T list<T>::pop(){
    item<T> *c=first;
    if(!c) return NULL;
    if(!c->getNext()){
        first=NULL;
        T aux=*(c->get());
        delete(c);
        return aux;
    } 
    item<T>* last;
    while(c->getNext()!=NULL){
        last=c->getNext()->getNext();
        if(!last){
            last=c->getNext();
            c->setNext(NULL);

            T aux=*(last->get());
            delete(last);
            return aux;
        }
        c=c->getNext();
    }
    return NULL;
}

#endif
