from xml.etree import ElementTree
import argparse
from ttf2c import Font

parser = argparse.ArgumentParser(description='Genera fonts bitmap en codi C.')

parser.add_argument('-c','--config-file', help="Genera fonts en funció de la configuració xml.")
args=parser.parse_args()

document = ElementTree.parse(args.config_file)

h = open("fonts.h", "w")

includes=[]
assignments={}

for font in document.findall("fontgroup"):
    size=int(font.attrib['size'])
    regular=font.find("regular")
    bold=font.find("bold")
    italic=font.find("italic")

    if(regular!=None): regular=regular.text.strip()
    if(bold!=None): bold=bold.text.strip()
    if(italic!=None): italic=italic.text.strip()

    f=Font(regular,size,bold,italic)

    includes.append(f.name+".h")

    alias=[a.text for a in font.findall("alias")]
    alias.append(font.attrib['name'])
    font_name=f.name
    for i in alias:
        assignments[i]=font_name+"_group"


for i in includes:
    h.write("#include \""+i+"\"\n")

h.write("\n")

for i in assignments.keys():
    h.write("#define "+i+" "+assignments[i]+"\n")

h.close()
