from freetype import *
import numpy
import argparse
#import cv2
## \defgroup fontgen Generador de fonts
# \addtogroup fontgen
# \brief Scripts python per a la conversió de fonts TTF a bitmap en capçaleres i codi font c.
# Com que renderitzar fonts TTF és una tasca massa complexa computacionalment per a un microcontrolador, es converteixen aquestes a bitmap en temps de compilació usant scripts en python. 
#
# Un script s'encarrega de convertir fonts individuals, en el tamany demanat, a capçaleres i codi font C, usant una array per guardar els bitmaps. L'altre script llegeix un arxiu de configuració en XML i genera totes les fonts necessàries per al lector, en funció d'aquesta configuració.
#
# La codificació usada és ISO-8859-15, que conté tot l'alfabet català i símbols d'us comú. Es podria canviar de forma força simple aquesta codificació.
#
# Es pot consultar el format de l'arxiu de configuració a [gitlab](https://gitlab.com/dferrg/ereader/-/blob/master/src/fonts/fonts.xml).
# @{

## Caràcters extra no definits a la codificació.
undefined_characters=[bytes([0xe2,0x97, 0xa6]).decode('utf8'), # empty bullet
                      bytes([0xe2,0x80, 0xa2]).decode('utf8')  ]


# Dilate function to remove cv2 dependency
def dilate(I, kernelSize, iterations):
    kernel=numpy.ones((int(kernelSize),int(kernelSize)),numpy.uint8)
    ret=I.copy()
    iCol = I.shape[0];
    iRow = I.shape[1];
 
    eRow = kernel.shape[0]
    eCol = kernel.shape[1]
 
    for it in range(iterations):
 
      for i in range(0, iCol):
        for j in range(0, iRow):
 
            for m in range (0, eCol):
                for n in range (0, eRow):
                    ki=int(m-(eCol-1)/2)
                    kj=int(n-(eRow-1)/2)
                    if (i+ki >= 0 and j+kj>=0 and i+ki < iCol and j+kj < iRow):
                        if(ret[i][j]<I[i+ki][j+kj]):
                            ret[i][j]=I[i+ki][j+kj]
 
    return ret


## Comprova si un caràcter és imprimible.
#  @param s caràcter
#  @param codec Codificació usada
def isprintable(s, codec='utf8'):
    try: s.decode(codec,'strict')
    except UnicodeDecodeError: return False
    else: return True


def letters(input):
    return ''.join(filter(str.isalpha, input))


def openFont(font, size):
    font = Face(font)
    font.set_char_size( 64*size)
    return font

def renderChar(font, char):
    slot = font.glyph
    font.load_char(char)
    h=slot.bitmap.rows
    w=slot.bitmap.width
    bm=numpy.array(slot.bitmap.buffer, dtype='ubyte').reshape(h,w)
    return bm
    
def aten(r):
    if (r>70): return r-70
    else: return 0

def bits(x):
    data = []
    for i in range(8):
        data.append(255 if int((x & 1) == 1) else 0)
        x = x >> 1
    data.reverse()
    return data

## Converteix el bitmap de 8 bits per pixels als 2bpp usats per la pantalla.
#  @param self The object pointer.
def a8To2(array, offset):
    arr=[]
    for i in range(offset):
        arr.append(0)

    arr=arr+array.tolist()
    lr=[]
    for i in range(len(arr)):
        if(i%4):
            lr[-1]+=(aten(arr[i])&0xC0)>>((i%4)*2)
        else:
            lr.append(aten(arr[i])&0xC0)
    return lr

## \brief converteix caràcters individuals a bitmap.
# Classe usada per Font per a generar el bitmap de cada caràcter, en funció de la font i el tamany, i en el format adeqüat per a l'eReader. 
class character:
    def __init__(self, character, font, size):
        self.char=character
        self.font=font
        self.fontSize=size
        self.bitmap=[]
        self.posX=0
        self.posY=0
        self.h=0
        self.w=0
        self.kerningLeft=[]
        self.kerningRight=[]
        self.render()
        
    ## Renderització del glyph a imatge.
    #  @param self The object pointer.
    def render(self):

        slot = self.font.glyph

        if(self.char>127 and self.char<128+len(undefined_characters)):
            self.font.load_char(undefined_characters[self.char-128], FT_LOAD_RENDER | FT_LOAD_TARGET_MONO)
        elif(isprintable(bytes([self.char]),"iso-8859-15")):
            self.font.load_char(bytes([self.char]).decode("iso-8859-15"),FT_LOAD_RENDER | FT_LOAD_TARGET_MONO)
        else:
            self.font.load_char(" ", FT_LOAD_RENDER | FT_LOAD_TARGET_MONO)    

        self.posX=slot.bitmap_left
        self.posY=self.fontSize-(slot.bitmap_top)
        self.h=slot.bitmap.rows
        self.w=slot.bitmap.width

        pitch  = slot.bitmap.pitch

        data = []
        for i in range(slot.bitmap.rows):
            row = []
            for j in range(slot.bitmap.pitch):
                row.extend(bits(slot.bitmap.buffer[i*slot.bitmap.pitch+j]))
            data.extend(row[:slot.bitmap.width])
        self.bitmap = numpy.array(data, dtype="ubyte").reshape(slot.bitmap.rows, slot.bitmap.width)
        self.bitmap=numpy.transpose(self.bitmap)
        return
        if (len(slot.bitmap.buffer)!=self.h*self.w):
            self.h=0
            self.w=0
            self.bitmap=numpy.array([[]])
            return

        self.bitmap=numpy.array(slot.bitmap.buffer, dtype='byte').reshape(self.h,self.w)
        self.bitmap=numpy.transpose(self.bitmap)

    ## Obté el contorn del caràcter, que s'utiliza per a calcular el kerning.
    #  @param self The object pointer.
    #  @param baseline Línia base de la font.
    def calculaKerning(self,baseline):
        self.kerningSegments=[  [0,self.fontSize/6+1],
                                [self.fontSize/6,self.fontSize/2],
                                [self.fontSize/2,5*self.fontSize/6],
                                [5*self.fontSize/6-1,self.fontSize]]
        dilatedBitmap=self.bitmap
        if(self.h>0 and self.w>0):
            kernelSize=3
#            kernel=numpy.ones((int(kernelSize),int(kernelSize)),numpy.uint8)
            dilatedBitmap=dilate(self.bitmap, kernelSize, 1)

        if(self.char==' '):
            self.kerningRight=[0,0,0,0]
            self.kerningLeft=[0,0,0,0]
            return

        self.posY-=baseline   
        for i in range(len(self.kerningSegments)): #per cada segment de kerning
            segment=self.kerningSegments[i]
            
            self.kerningLeft.append(self.w)
            self.kerningRight.append(self.w)
            for j in range(int(segment[0]),int(segment[1])): #per cada línia de píxels
            
                if(j<self.posY or j>=self.h+self.posY):
                    continue
                
                jv=j-self.posY

                #calcula kerning esquerre
                for k in range(self.w):#per tot l'ample
                    if (dilatedBitmap[k][jv] > 0):
                        self.kerningLeft[i]=min(self.kerningLeft[i], k)
                        break
                
                for k in range(self.w-1,-1,-1):#per tot l'ample
                    if (dilatedBitmap[k][jv] > 0):
                        self.kerningRight[i]=min(self.kerningRight[i],self.w-k)
                        break
    ## Escriu les propietats de renderització del caràcter al codi font C.
    #  @param self The object pointer.
    #  @param file Arxiu sobre el que escriure.
    #  @param offset Posició del caràcter al bitmap de la font.
    def writeProperties(self, file,offset):
        w=self.w
        if self.char==32:
            w=int(1+self.fontSize/2)
        file.write("\t{"+
                str(offset)+", "+
                str(int(self.posY/4))+", "+
                str(max(self.posX,0))+", "+
                str(self.dataLineLen())+", "+
                str(w)+", "+
                self.kerningStr()+"},  \t// \""+chr(self.char)+"\" \n")
        
        return offset + self.dataSize()
    ## Converteix el contorn a codi font C.
    #  @param self The object pointer.
    def kerningStr(self):
        r="{"
        for i in range(4):
            r+=str(self.kerningLeft[i])+", "
            
        r+="}, {"
        for i in range(4):
            r+=str(self.kerningRight[i])+", "
        
        r+="}"
        return r

    ## Calcula la mida en bytes d'una línia del caràcter.
    #  @param self The object pointer.
    def dataLineLen(self):
        return int((self.h+(self.posY%4)-1)/4)+1
        
    ## Calcula el tamany total en bytes del caràcter.
    #  @param self The object pointer.

    def dataSize(self):
        return self.dataLineLen()*self.w
        
    # Escriu el bitmap al codi font C.
    #  @param self The object pointer.
    #  @param file Arxiu sobre el que escriure.
    def writeBitmap(self, file):
        file.write("\n/*     "+chr(self.char)+"      */\n\n")
        for i in self.bitmap:
            l=a8To2(i, (self.posY)%4)
            file.write("\t")
            for j in l:
                file.write(hex(j)+", ")
            file.write(" \n")
        
## \brief Converteix una font a bitmap.
# Classe usada per a convertir fonts TTF a bitmaps en ROM. Es llegeixen tots els caràcters necessaris (ISO-8859-15) i es genera una capçalera i el codi font, on s'escriuen les propietats de renderització de cada caràcter i el bitmap sencer e la font.
class Font:
    def __init__(self, path, size, bold=None, italic=None):
        self.font = Face(path)

        self.bold = None
        if(bold): 
            self.bold=Face(bold)
            self.bold.set_char_size( 64*size)
        
        self.italic = None
        if(italic):
            self.italic=Face(italic)
            self.italic.set_char_size( 64*size)

        self.size=size
        self.font.set_char_size( 64*size)

        self.name=letters(path.split("/")[-1].split(".")[0])
        self.name=self.name+"_"+str(size)
        self.writeHeader()
        self.writeSource();
        if(self.bold): self.writeSource("_bold", self.bold)
        if(self.italic): self.writeSource("_italic", self.italic)
        
    ## Genera la capçalera de la font.
    #  @param self The object pointer.
    def writeHeader(self):
        f = open(self.name+".h", "w")

        f.write("#ifndef _FONT_"+self.name+"_\n")
        f.write("#define _FONT_"+self.name+"_\n")

        f.write("#include \"font.h\"\n\n")
        f.write("extern const struct font "+self.name+";\n")
        f.write("extern const uint8_t "+self.name+"_bitmap[];\n")
        f.write("extern const struct font_char "+self.name+"_char_LU[];\n\n")

        if(self.bold):
            f.write("extern const struct font "+self.name+"_bold;\n")
            f.write("extern const uint8_t "+self.name+"_bold_bitmap[];\n")
            f.write("extern const struct font_char "+self.name+"_bold_char_LU[];\n\n")

        if(self.italic):
            f.write("extern const struct font "+self.name+"_italic;\n")
            f.write("extern const uint8_t "+self.name+"_italic_bitmap[];\n")
            f.write("extern const struct font_char "+self.name+"_italic_char_LU[];\n\n")

        f.write("extern struct font_group "+self.name+"_group;\n")
        f.write("#endif")

        f.close()        
    ## Genera el codi font.
    #  @param self The object pointer.
    #  @param suffix Usat quan una mateixa font s'ha de generar en cursiva o negreta.
    def writeSource(self, suffix="", file=None):
        isregular=file
        if(file==None):
            file=self.font
        f = open(self.name+suffix+".c", "w")
        f.write("#include \""+self.name+".h\"\n")
        #descriptor font
        f.write("const font "+self.name+suffix+"={\n")
        f.write("\t"+str(self.size)+", //size\n")
        f.write("\t"+str(int(self.size/2))+", //line spacing\n")
        f.write("\t"+str(int(numpy.sqrt(self.size)))+", //char spacing\n")
        f.write("\t"+self.name+suffix+"_char_LU, //char lookup address\n")
        f.write("\t"+self.name+suffix+"_bitmap, //bitmaps\n")
        f.write("\n};\n\n")
        
        if(isregular==None):
            f.write("struct font_group "+self.name+"_group{\n")
            f.write("\t&"+self.name+",\n")
            if (self.bold): f.write("\t&"+self.name+"_bold,\n")
            else: f.write("\t&"+self.name+",\n")
            if (self.italic): f.write("\t&"+self.name+"_italic,\n")
            else: f.write("\t&"+self.name+",\n")
            f.write("};\n")
        f.write("const font_char "+self.name+suffix+"_char_LU[]={\n")
        baseline=200
        #troba linia base
        for i in range(0x20,0xff):
            c=character(i,file,self.size)
            baseline=min(baseline,c.posY)
            
        offset=0
        #escriu la taula de descripcio de caracters
        for i in range(0x20,0xff):
            c=character(i,file,self.size)
            c.calculaKerning(baseline)
            offset=c.writeProperties(f,offset)    
        f.write("\n};\n\n")
        
        #escriu els mapes de bits
        f.write("const uint8_t "+self.name+suffix+"_bitmap[]={\n")
        for i in range(0x20,0xff):
            c=character(i,file,self.size)
            c.calculaKerning(baseline)
            c.writeBitmap(f)
        f.write("\n};\n")

        f.close()      

# @}
""""

parser = argparse.ArgumentParser(description='Genera fonts bitmap en codi C.')


parser.add_argument("--dir",default=".", help="directori de sortida",nargs="?")

parser.add_argument('-s','--size', 
                    help='tamanys a generar')
parser.add_argument('-g','--group', help="Genera grup de fonts. Requereix --bold i --italic",action="store_true")
parser.add_argument("-f", "--font", help="font TTF")
parser.add_argument("-b", "--bold", help="bold font TTF")
parser.add_argument("-i", "--italic", help="italic font TTF")
args=parser.parse_args()

if(args.size == None):
    print("No s'ha especificat cap tamany de lletra")
sizes=int(args.size)
if(args.font==None):
    print("És necessari proveir una font.")
if(args.group and (args.bold==None or args.italic==None)):
    print("És necessari proveir font cursiva i negreta amb l'opció -g'")
    exit()


f=Font(args.font,sizes,args.bold,args.italic)

"""
