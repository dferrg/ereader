#include "font.h"
#include "FreeRTOS.h"
#include "ff.h"

extern size_t xFreeBytesRemaining;
uint16_t sizec;
int computeKerning(struct font* f, char l, char r){
    int min=2000;
    uint8_t* kl=(uint8_t*)&getCharInfo(f, l)->kerningR;
    uint8_t* kr=(uint8_t*)&getCharInfo(f, r)->kerningL;
    for (int i=0; i<4; i++){
        if(kl[i]+kr[i] <min ) min=kl[i]+kr[i];
    }

    return f->charSpacing-min;
}
struct font_char* getCharInfo(struct font *f, uint8_t c){
    if(c < ' ' || c > 255) c=' ';
    //std::cout << (uint16_t)c << std::endl;
    return &f->charLookUp[c-' '];
}
uint32_t utf8_iso_8859_15[]={
0xc280, 0xc281, 0xc282, 0xc283, 0xc284, 0xc285, 0xc286, 0xc287, 0xc288, 0xc289, 0xc28a, 0xc28b, 0xc28c, 0xc28d, 0xc28e, 0xc28f,
0xc290, 0xc291, 0xc292, 0xc293, 0xc294, 0xc295, 0xc296, 0xc297, 0xc298, 0xc299, 0xc29a, 0xc29b, 0xc29c, 0xc29d, 0xc29e, 0xc29f,
0xc2a0, 0xc2a1, 0xc2a2, 0xc2a3, 0xe282ac, 0xc2a5, 0xc5a0, 0xc2a7, 0xc5a1, 0xc2a9, 0xc2aa, 0xc2ab, 0xc2ac, 0xc2ad, 0xc2ae, 0xc2af,
0xc2b0, 0xc2b1, 0xc2b2, 0xc2b3, 0xc5bd, 0xc2b5, 0xc2b6, 0xc2b7, 0xc5be, 0xc2b9, 0xc2ba, 0xc2bb, 0xc592, 0xc593, 0xc5b8, 0xc2bf,
0xc380, 0xc381, 0xc382, 0xc383, 0xc384, 0xc385, 0xc386, 0xc387, 0xc388, 0xc389, 0xc38a, 0xc38b, 0xc38c, 0xc38d, 0xc38e, 0xc38f,
0xc390, 0xc391, 0xc392, 0xc393, 0xc394, 0xc395, 0xc396, 0xc397, 0xc398, 0xc399, 0xc39a, 0xc39b, 0xc39c, 0xc39d, 0xc39e, 0xc39f,
0xc3a0, 0xc3a1, 0xc3a2, 0xc3a3, 0xc3a4, 0xc3a5, 0xc3a6, 0xc3a7, 0xc3a8, 0xc3a9, 0xc3aa, 0xc3ab, 0xc3ac, 0xc3ad, 0xc3ae, 0xc3af,
0xc3b0, 0xc3b1, 0xc3b2, 0xc3b3, 0xc3b4, 0xc3b5, 0xc3b6, 0xc3b7, 0xc3b8, 0xc3b9, 0xc3ba, 0xc3bb, 0xc3bc, 0xc3bd, 0xc3be, 0xc3bf,
};
uint8_t readchar(FIL* f){
  char c;
  int read;
  f_read(f, &c, 1, &read);
  if(read) return c;
  else return 0;
}

uint8_t utf2iso(FIL* s){

    uint8_t c;
    int nbytes=0;
    uint32_t utf=0;
    c=readchar(s);
    if((c)<128) return c;

    //Obté el nombre de bytes a llegir per a l'actual caràcter
    if(c>=0XC0 && c<0XE0) nbytes=2;
    else if(c>=0xE0 && c<0xF0) nbytes=3;
    else nbytes=4;


    //Obté els bytes
    do{


        nbytes--;
        utf |= ((uint32_t)c)<<(8*nbytes);
        if(nbytes) c=readchar(s);
    }while(nbytes>0&&c);

    //busca els bytes a la taula
    for (int i=0; i<128; i++){
        if(utf8_iso_8859_15[i]==utf)
          return i+128;
    }
    return ' ';
}

uint8_t testChar(FIL* f){
    int p=f_tell(f);
    uint8_t c=readchar(f);
    f_lseek(f, p);
    return c;
}
void * operator new( size_t size )
{
    sizec+=size;
    //std::cout << xFreeBytesRemaining <<std::endl;
    void *r =  pvPortMalloc(size);
    return r;
}

void * operator new[]( size_t size )
{

    return pvPortMalloc( size );
}

void operator delete( void * ptr )
{
    vPortFree( ptr );
}

void operator delete[]( void * ptr )
{
    vPortFree( ptr );
}
