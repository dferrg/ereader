#include <stdint.h>
#include "ff.h"
#ifndef _FONT_
#define _FONT_
/**
 * @addtogroup fonts
 * \brief Gestió de fonts de text.
 * \ingroup render
 * @{
 */

/** \brief Estructura de dades pel kerning. 4 valors per a 4 altures del contorn. */
struct kerning_contour{
    uint8_t high_despl;
    uint8_t midHigh_despl;
    uint8_t midLow_despl;
    uint8_t low_despl;
};

/** \brief Descriptor del caracter d'una font. 
 *
 * **Nota:** Les mesures en vertical estàn en bytes, a 4 píxels cada un.
*/
struct font_char{
    /** \brief Desplaçament dins el bitmap de la font. */
    int bitmapOffset; 
    /** \brief Desplaçament del bitmap en alçada respecte l'origen. */
    int posY;
    /** \brief Desplaçament del bitmap en amplada respecte l'origen. */
    int posX;
    /** \brief Alçada del bitmap. */
    int h;
    /** \brief Amplada del bitmap. */
    int w;
    /** \brief Contorn esquerre. */
    struct kerning_contour kerningL;
    /** \brief Contorn dret. */
    struct kerning_contour kerningR;
};

struct font{
    int size; //altura de cada línia en píxels
    int lineSpacing; //interliniat en píxels
    int charSpacing; //en píxels
    struct font_char* charLookUp; //descriptors de cada caràcter
    uint8_t* bitmaps;
} ;

struct font_group{
    struct font* f_regular;
    struct font* f_bold;
    struct font* f_italic;
};
int computeKerning( struct font* f, char l, char r);
struct font_char* getCharInfo(struct font* f, uint8_t c);
uint8_t utf2iso(FIL* s);
uint8_t testChar(FIL* s);

/**
 * @}
 *
 *
 */
#endif
