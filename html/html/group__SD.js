var group__SD =
[
    [ "sdio_init", "group__SD.html#ga0902cb06fb3a2aac6ffe23f318863a86", null ],
    [ "SD_init", "group__SD.html#gac87c0ab5d0363dbdcac287fc9d6826ce", null ],
    [ "sdio_dma_init", "group__SD.html#ga492dc29fff5283298238a1f6c7acd885", null ],
    [ "sdio_cmd_send", "group__SD.html#ga54d19457b8c4d2f29744acdf624ad8ba", null ],
    [ "sdio_read_block", "group__SD.html#gaa06dd95d069ed9c156ca410343b41fdc", null ],
    [ "sdio_write_block", "group__SD.html#ga438af3f01443489dbc0bc79b939970b2", null ],
    [ "sdio_write_blocks", "group__SD.html#ga0b5344573b78632ce82ff954b571f5b4", null ],
    [ "sdio_read_blocks", "group__SD.html#gac1c9e59e9d1712ff70b1d105a56bad81", null ]
];