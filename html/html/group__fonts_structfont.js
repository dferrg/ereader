var group__fonts_structfont =
[
    [ "size", "group__fonts.html#a3f484bc38fdfd67c3d8dcfa49bb6e356", null ],
    [ "lineSpacing", "group__fonts.html#a8351e286e56b12d313804fae1800ac4b", null ],
    [ "charSpacing", "group__fonts.html#a26e5950e5a37a0f706f7a99cd074cd88", null ],
    [ "charLookUp", "group__fonts.html#aaed29237c3e84f4993e2372495c89332", null ],
    [ "bitmaps", "group__fonts.html#ad55e42056d3c892161d8ed39bd0095a2", null ]
];