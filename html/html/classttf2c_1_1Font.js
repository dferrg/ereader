var classttf2c_1_1Font =
[
    [ "__init__", "group__fontgen.html#ga1922e75cb76fe6b75c2ace9eb7df9539", null ],
    [ "writeHeader", "group__fontgen.html#ga1df19a5801800051d278affa694c3bbb", null ],
    [ "writeSource", "group__fontgen.html#ga3ea97cf093b8fe5ec622f21d770a72e5", null ],
    [ "font", "group__fontgen.html#ga9d18ac2bb3674331f7cc2e017144c1da", null ],
    [ "bold", "group__fontgen.html#ga9c85516c52fc177c637a601b0aabaac5", null ],
    [ "italic", "group__fontgen.html#gaf3ceabb9c804336bf90f740f4756f9e4", null ],
    [ "size", "group__fontgen.html#gac104aae0bd55b75616a807b400d38e02", null ],
    [ "name", "group__fontgen.html#gae360c5229bb7fc1bef9988040e073db1", null ]
];