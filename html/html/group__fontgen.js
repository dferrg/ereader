var group__fontgen =
[
    [ "Explorador d'arxius", "galeria.html#autotoc_md6", null ],
    [ "Lectura d'arxius", "galeria.html#autotoc_md7", null ],
    [ "Prototip", "galeria.html#autotoc_md8", null ],
    [ "Carcassa", "galeria.html#autotoc_md9", [
      [ "Nota: nstruccions provades a una instal·lació fresca de Xubuntu 20.10", "instalacio.html#autotoc_md33", null ]
    ] ],
    [ "character", "classttf2c_1_1character.html", [
      [ "__init__", "group__fontgen.html#gaf793debe5ad667272e833759a19db418", null ],
      [ "render", "group__fontgen.html#gaf982b8e104a50edcd34a56f29d51c1fe", null ],
      [ "calculaKerning", "group__fontgen.html#gaee1e47a516857ef9eab3c54ea57ad266", null ],
      [ "writeProperties", "group__fontgen.html#ga0a66eb4222e460746dbf018d83d2f60e", null ],
      [ "kerningStr", "group__fontgen.html#ga0d08f6fbe05c7ea3ee4ed483894a18a2", null ],
      [ "dataLineLen", "group__fontgen.html#ga99391f2218db32532e20e7cf0a174a8c", null ],
      [ "dataSize", "group__fontgen.html#gaf6587aa301d300077ee438dfaec3394c", null ],
      [ "writeBitmap", "group__fontgen.html#ga7fa9307b9972a8b41bd55ebef28f7d76", null ],
      [ "char", "group__fontgen.html#gad1fb56e5872a794de3955a9270cd360b", null ],
      [ "font", "group__fontgen.html#gae8ec8858b60e54af96ec377b27b14734", null ],
      [ "fontSize", "group__fontgen.html#gaf86e94cd2f1367cbe47ce16250081daa", null ],
      [ "bitmap", "group__fontgen.html#gae817e049afe8f378d7704cf310f22f49", null ],
      [ "posX", "group__fontgen.html#ga45f974889e66be24f5056268912e8312", null ],
      [ "posY", "group__fontgen.html#gafe6ae12b28de66fdacb539e3012e151a", null ],
      [ "h", "group__fontgen.html#ga632d1b3b0e1b26594d1d96cc1ee8e0a3", null ],
      [ "w", "group__fontgen.html#gad40bfc09867ebc2199ffac73bfa3aaff", null ],
      [ "kerningLeft", "group__fontgen.html#ga69a301ae9ed50992b998c6d7daec67e6", null ],
      [ "kerningRight", "group__fontgen.html#ga9f356b09af6dd92570c6a5b7f382943b", null ],
      [ "kerningSegments", "group__fontgen.html#ga54dfe6106772ae4f6d8216771258f9c3", null ]
    ] ],
    [ "Font", "classttf2c_1_1Font.html", [
      [ "__init__", "group__fontgen.html#ga1922e75cb76fe6b75c2ace9eb7df9539", null ],
      [ "writeHeader", "group__fontgen.html#ga1df19a5801800051d278affa694c3bbb", null ],
      [ "writeSource", "group__fontgen.html#ga3ea97cf093b8fe5ec622f21d770a72e5", null ],
      [ "font", "group__fontgen.html#ga9d18ac2bb3674331f7cc2e017144c1da", null ],
      [ "bold", "group__fontgen.html#ga9c85516c52fc177c637a601b0aabaac5", null ],
      [ "italic", "group__fontgen.html#gaf3ceabb9c804336bf90f740f4756f9e4", null ],
      [ "size", "group__fontgen.html#gac104aae0bd55b75616a807b400d38e02", null ],
      [ "name", "group__fontgen.html#gae360c5229bb7fc1bef9988040e073db1", null ]
    ] ],
    [ "isprintable", "group__fontgen.html#ga633d6106e13cc09333a1bad56aebc786", null ],
    [ "a8To2", "group__fontgen.html#ga63f71f0acd4401082d2d4e691004a39f", null ],
    [ "render", "group__fontgen.html#gaf982b8e104a50edcd34a56f29d51c1fe", null ],
    [ "calculaKerning", "group__fontgen.html#gaee1e47a516857ef9eab3c54ea57ad266", null ],
    [ "writeProperties", "group__fontgen.html#ga0a66eb4222e460746dbf018d83d2f60e", null ],
    [ "kerningStr", "group__fontgen.html#ga0d08f6fbe05c7ea3ee4ed483894a18a2", null ],
    [ "dataLineLen", "group__fontgen.html#ga99391f2218db32532e20e7cf0a174a8c", null ],
    [ "dataSize", "group__fontgen.html#gaf6587aa301d300077ee438dfaec3394c", null ],
    [ "writeHeader", "group__fontgen.html#ga1df19a5801800051d278affa694c3bbb", null ],
    [ "writeSource", "group__fontgen.html#ga3ea97cf093b8fe5ec622f21d770a72e5", null ],
    [ "undefined_characters", "group__fontgen.html#ga9811e6092e052afd4befbcb07e3456a8", null ]
];