var group__fonts_structfont__char =
[
    [ "bitmapOffset", "group__fonts.html#ab036719c6e652ad99e2e14777a3079f7", null ],
    [ "posY", "group__fonts.html#a821b1f693e226a7e697af1f358748a47", null ],
    [ "posX", "group__fonts.html#a546602826d7b01b3eb397300c74353c5", null ],
    [ "h", "group__fonts.html#ae33b89946959069e164d54248553f0b5", null ],
    [ "w", "group__fonts.html#a51e96db4fef20a20ee8bb5147477dd68", null ],
    [ "kerningL", "group__fonts.html#a88f04c247eb98c621194c2ab9a88a767", null ],
    [ "kerningR", "group__fonts.html#a26e548aa93aa4d69f0687c00e94a01eb", null ]
];