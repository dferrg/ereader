var searchData=
[
  ['calculakerning_15',['calculaKerning',['../group__fontgen.html#gaee1e47a516857ef9eab3c54ea57ad266',1,'ttf2c::character']]],
  ['carcasa_20impresa_20en_203d_16',['Carcasa impresa en 3D',['../carcasa.html',1,'']]],
  ['cdcacm_5fcontrol_5frequest_17',['cdcacm_control_request',['../group__USB.html#ga7a1ef6217e30fe1526475af5b25a3d4c',1,'cdcacm_control_request(usbd_device *usbd_dev, struct usb_setup_data *req, uint8_t **buf, uint16_t *len, void(**complete)(usbd_device *usbd_dev, struct usb_setup_data *req)):&#160;usbserial.c'],['../group__USB.html#ga7a1ef6217e30fe1526475af5b25a3d4c',1,'cdcacm_control_request(usbd_device *usbd_dev, struct usb_setup_data *req, uint8_t **buf, uint16_t *len, void(**complete)(usbd_device *usbd_dev, struct usb_setup_data *req)):&#160;usbserial.c']]],
  ['cdcacm_5fdata_5frx_5fcb_18',['cdcacm_data_rx_cb',['../group__USB.html#gade2ddffa83cffe8a0de0971f32c6b86e',1,'usbserial.c']]],
  ['cdcacm_5fset_5fconfig_19',['cdcacm_set_config',['../group__USB.html#ga2ca52001c753bc2716554c1ea7d82684',1,'cdcacm_set_config(usbd_device *usbd_dev, uint16_t wValue):&#160;usbserial.c'],['../group__USB.html#ga2ca52001c753bc2716554c1ea7d82684',1,'cdcacm_set_config(usbd_device *usbd_dev, uint16_t wValue):&#160;usbserial.c']]],
  ['character_20',['character',['../classttf2c_1_1character.html',1,'ttf2c']]],
  ['checklinefit_21',['checkLineFit',['../classparagraph.html#ac9b786d548217b9c751f3b2f6ed438bf',1,'paragraph']]],
  ['checkwordfit_22',['checkWordFit',['../classline.html#a54ca0b0e0bdca108265e9bed651c95bb',1,'line']]],
  ['clear_23',['clear',['../classparagraph.html#a0ac633c3ebdfc9117998365c705abe26',1,'paragraph']]],
  ['compilació_20del_20codi_20font_24',['Compilació del codi font',['../instalacio.html',1,'']]],
  ['control_20d_27alimentacions_25',['Control d&apos;alimentacions',['../Alimentacions.html',1,'infdev']]]
];
