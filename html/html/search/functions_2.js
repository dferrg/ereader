var searchData=
[
  ['calculakerning_200',['calculaKerning',['../group__fontgen.html#gaee1e47a516857ef9eab3c54ea57ad266',1,'ttf2c::character']]],
  ['cdcacm_5fcontrol_5frequest_201',['cdcacm_control_request',['../group__USB.html#ga7a1ef6217e30fe1526475af5b25a3d4c',1,'cdcacm_control_request(usbd_device *usbd_dev, struct usb_setup_data *req, uint8_t **buf, uint16_t *len, void(**complete)(usbd_device *usbd_dev, struct usb_setup_data *req)):&#160;usbserial.c'],['../group__USB.html#ga7a1ef6217e30fe1526475af5b25a3d4c',1,'cdcacm_control_request(usbd_device *usbd_dev, struct usb_setup_data *req, uint8_t **buf, uint16_t *len, void(**complete)(usbd_device *usbd_dev, struct usb_setup_data *req)):&#160;usbserial.c']]],
  ['cdcacm_5fdata_5frx_5fcb_202',['cdcacm_data_rx_cb',['../group__USB.html#gade2ddffa83cffe8a0de0971f32c6b86e',1,'usbserial.c']]],
  ['cdcacm_5fset_5fconfig_203',['cdcacm_set_config',['../group__USB.html#ga2ca52001c753bc2716554c1ea7d82684',1,'cdcacm_set_config(usbd_device *usbd_dev, uint16_t wValue):&#160;usbserial.c'],['../group__USB.html#ga2ca52001c753bc2716554c1ea7d82684',1,'cdcacm_set_config(usbd_device *usbd_dev, uint16_t wValue):&#160;usbserial.c']]],
  ['checklinefit_204',['checkLineFit',['../classparagraph.html#ac9b786d548217b9c751f3b2f6ed438bf',1,'paragraph']]],
  ['checkwordfit_205',['checkWordFit',['../classline.html#a54ca0b0e0bdca108265e9bed651c95bb',1,'line']]],
  ['clear_206',['clear',['../classparagraph.html#a0ac633c3ebdfc9117998365c705abe26',1,'paragraph']]]
];
