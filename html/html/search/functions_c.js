var searchData=
[
  ['paragraph_219',['paragraph',['../classparagraph.html#aae0f97ed63d6f1553d2f3b3462762456',1,'paragraph']]],
  ['pmic_5finit_220',['PMIC_init',['../group__alimentacio.html#gaf70911e62c08224b2f34d380964f731b',1,'PMIC_init():&#160;TPS65186.c'],['../group__alimentacio.html#gaf70911e62c08224b2f34d380964f731b',1,'PMIC_init():&#160;TPS65186.c']]],
  ['power_5fbattery_5fcharged_5fcb_221',['power_battery_charged_cb',['../group__alimentacio.html#gaafd6f56f5de855c22e10313cab458c55',1,'power.h']]],
  ['power_5fget_5fbattery_5fpercentage_222',['power_get_battery_percentage',['../group__alimentacio.html#ga101b1d8dc71f3bd752953861f66e4e56',1,'power.h']]],
  ['power_5fget_5fbattery_5fvoltage_223',['power_get_battery_voltage',['../group__alimentacio.html#ga178fce301ae6abd58f08cab3940390af',1,'power_get_battery_voltage():&#160;power.c'],['../group__alimentacio.html#ga178fce301ae6abd58f08cab3940390af',1,'power_get_battery_voltage():&#160;power.c']]],
  ['power_5finit_224',['power_init',['../group__alimentacio.html#ga1dc48d9f3436d555e44455ddef993630',1,'power_init():&#160;power.c'],['../group__alimentacio.html#ga1dc48d9f3436d555e44455ddef993630',1,'power_init():&#160;power.c']]],
  ['power_5foff_225',['power_off',['../group__alimentacio.html#gafeda4407b9d28026e4de548fbcc6e99c',1,'power_off():&#160;power.c'],['../group__alimentacio.html#gafeda4407b9d28026e4de548fbcc6e99c',1,'power_off():&#160;power.c']]],
  ['print_226',['print',['../group__USB.html#ga0adfc0f80fe1f584b8db1e5280f25fe0',1,'print(char *):&#160;finestra.c'],['../group__USB.html#ga0adfc0f80fe1f584b8db1e5280f25fe0',1,'print(char *a):&#160;finestra.c'],['../group__USB.html#ga0adfc0f80fe1f584b8db1e5280f25fe0',1,'print(char *a):&#160;finestra.c'],['../group__USB.html#ga0adfc0f80fe1f584b8db1e5280f25fe0',1,'print(char *a):&#160;usbserial.c']]]
];
