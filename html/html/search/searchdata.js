var indexSectionsWithContent =
{
  0: "abcdefghijklmnprstuw~",
  1: "bcdefhijklmprstw",
  2: "abcdfgijklmnprstuw~",
  3: "bhkpuw",
  4: "abdefgprstu",
  5: "abcdgilps"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "functions",
  3: "variables",
  4: "groups",
  5: "pages"
};

var indexSectionLabels =
{
  0: "Tot",
  1: "Estructures de Dades",
  2: "Funcions",
  3: "Variables",
  4: "Grups",
  5: "Pàgines"
};

