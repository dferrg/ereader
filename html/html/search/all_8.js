var searchData=
[
  ['image_51',['image',['../classimage.html',1,'']]],
  ['informe_20de_20progrés_20i_52',['Informe de progrés I',['../infprog1.html',1,'informes']]],
  ['informe_20de_20progrés_20ii_53',['Informe de progrés II',['../infprog2.html',1,'informes']]],
  ['informe_20final_54',['Informe final',['../inffin.html',1,'informes']]],
  ['informe_20inicial_55',['Informe inicial',['../infinic.html',1,'informes']]],
  ['informes_56',['Informes',['../informes.html',1,'arxius']]],
  ['informes_20de_20desenvolupament_57',['Informes de desenvolupament',['../infdev.html',1,'informes']]],
  ['initline_58',['initLine',['../classparagraph.html#a81403dce8eb754c50b5efd7aa3622116',1,'paragraph']]],
  ['isprintable_59',['isprintable',['../group__fontgen.html#ga633d6106e13cc09333a1bad56aebc786',1,'ttf2c']]],
  ['item_60',['item',['../classitem.html',1,'']]]
];
