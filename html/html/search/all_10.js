var searchData=
[
  ['s_5feink_5fclear_113',['s_eink_clear',['../group__ED060SC4.html#gac1cb6055354c8fd72ea24bb37abbf453',1,'s_eink_clear(writeArea_t area, uint32_t color):&#160;epdsys.c'],['../group__ED060SC4.html#gac1cb6055354c8fd72ea24bb37abbf453',1,'s_eink_clear(writeArea_t area, uint32_t color):&#160;epdsys.c'],['../group__ED060SC4.html#gac1cb6055354c8fd72ea24bb37abbf453',1,'s_eink_clear(writeArea_t area, uint32_t color):&#160;epdsys.c']]],
  ['s_5feink_5fstart_5fwrite_114',['s_eink_start_write',['../group__ED060SC4.html#gaf366d67f9173e3cc42075be69bbfaa0d',1,'s_eink_start_write(writeArea_t area, uint8_t bitDepth):&#160;epdsys.c'],['../group__ED060SC4.html#gaf366d67f9173e3cc42075be69bbfaa0d',1,'s_eink_start_write(writeArea_t area, uint8_t bitDepth):&#160;epdsys.c'],['../group__ED060SC4.html#gaf366d67f9173e3cc42075be69bbfaa0d',1,'s_eink_start_write(writeArea_t area, uint8_t bitDepth):&#160;epdsys.c']]],
  ['s_5feink_5fwrite_5fbuffer_115',['s_eink_write_buffer',['../group__ED060SC4.html#gab4fc2962a5e02e881fcb3768f6301e74',1,'s_eink_write_buffer():&#160;epdsys.c'],['../group__ED060SC4.html#gab4fc2962a5e02e881fcb3768f6301e74',1,'s_eink_write_buffer():&#160;epdsys.c'],['../group__ED060SC4.html#gab4fc2962a5e02e881fcb3768f6301e74',1,'s_eink_write_buffer():&#160;epdsys.c']]],
  ['screen_5finit_116',['screen_init',['../group__ED060SC4.html#ga3c051104f4d82fc399ff071d7bf203aa',1,'screen_init():&#160;epdsys.c'],['../group__ED060SC4.html#ga3c051104f4d82fc399ff071d7bf203aa',1,'screen_init():&#160;epdsys.c']]],
  ['screen_5ftask_117',['screen_task',['../group__ED060SC4.html#ga0c9fd8729034fc5b41f159b3762be486',1,'screen_task(void *args __attribute__((unused))):&#160;epdsys.c'],['../group__ED060SC4.html#ga0c9fd8729034fc5b41f159b3762be486',1,'screen_task(void *args __attribute__((unused))):&#160;epdsys.c'],['../group__ED060SC4.html#ga0c9fd8729034fc5b41f159b3762be486',1,'screen_task(void *args __attribute__((unused))):&#160;epdsys.c']]],
  ['screencmd_118',['screenCmd',['../structscreenCmd.html',1,'']]],
  ['sd_119',['SD',['../group__SD.html',1,'']]],
  ['sd_5finit_120',['SD_init',['../group__SD.html#gac87c0ab5d0363dbdcac287fc9d6826ce',1,'SD_init():&#160;SD.c'],['../group__SD.html#gac87c0ab5d0363dbdcac287fc9d6826ce',1,'SD_init():&#160;SD.c']]],
  ['sdio_121',['SDIO',['../SD.html',1,'infdev']]],
  ['sdio_5fcmd_5fsend_122',['sdio_cmd_send',['../group__SD.html#ga54d19457b8c4d2f29744acdf624ad8ba',1,'sdio_cmd_send(uint8_t cmd, uint32_t arg, uint8_t wait_resp):&#160;SD.c'],['../group__SD.html#ga54d19457b8c4d2f29744acdf624ad8ba',1,'sdio_cmd_send(uint8_t cmd, uint32_t arg, uint8_t wait_resp):&#160;SD.c']]],
  ['sdio_5fdma_5finit_123',['sdio_dma_init',['../group__SD.html#ga492dc29fff5283298238a1f6c7acd885',1,'sdio_dma_init():&#160;SD.c'],['../group__SD.html#ga492dc29fff5283298238a1f6c7acd885',1,'sdio_dma_init():&#160;SD.c']]],
  ['sdio_5finit_124',['sdio_init',['../group__SD.html#ga0902cb06fb3a2aac6ffe23f318863a86',1,'sdio_init():&#160;SD.c'],['../group__SD.html#ga0902cb06fb3a2aac6ffe23f318863a86',1,'sdio_init():&#160;SD.c']]],
  ['sdio_5fread_5fblock_125',['sdio_read_block',['../group__SD.html#gaa06dd95d069ed9c156ca410343b41fdc',1,'sdio_read_block(uint8_t data[], uint32_t address):&#160;SD.c'],['../group__SD.html#gaa06dd95d069ed9c156ca410343b41fdc',1,'sdio_read_block(uint8_t data[], uint32_t address):&#160;SD.c']]],
  ['sdio_5fread_5fblocks_126',['sdio_read_blocks',['../group__SD.html#gac1c9e59e9d1712ff70b1d105a56bad81',1,'sdio_read_blocks(uint8_t data[], uint32_t address, uint32_t n_blocks):&#160;SD.c'],['../group__SD.html#gac1c9e59e9d1712ff70b1d105a56bad81',1,'sdio_read_blocks(uint8_t data[], uint32_t address, uint32_t n_blocks):&#160;SD.c']]],
  ['sdio_5fwrite_5fblock_127',['sdio_write_block',['../group__SD.html#ga438af3f01443489dbc0bc79b939970b2',1,'sdio_write_block(uint8_t data[], uint32_t address):&#160;SD.c'],['../group__SD.html#ga438af3f01443489dbc0bc79b939970b2',1,'sdio_write_block(uint8_t data[], uint32_t address):&#160;SD.c']]],
  ['sdio_5fwrite_5fblocks_128',['sdio_write_blocks',['../group__SD.html#ga0b5344573b78632ce82ff954b571f5b4',1,'sdio_write_blocks(uint8_t data[], uint32_t address, uint32_t n_blocks):&#160;SD.c'],['../group__SD.html#ga0b5344573b78632ce82ff954b571f5b4',1,'sdio_write_blocks(uint8_t data[], uint32_t address, uint32_t n_blocks):&#160;SD.c']]],
  ['segment_129',['segment',['../classsegment.html',1,'']]],
  ['setbpp_130',['setBpp',['../classbloc.html#ac7d97ef9c2243eccb6fd5ce82f3a4665',1,'bloc']]],
  ['setheight_131',['setHeight',['../classbloc.html#a8d64d3a4123c902b17b5f13e4dd0badd',1,'bloc']]],
  ['setpos_132',['setPos',['../classbloc.html#a3efc537b8157cd345a53c6302e0b0269',1,'bloc']]],
  ['sistema_20operatiu_133',['Sistema Operatiu',['../SO.html',1,'infdev']]]
];
