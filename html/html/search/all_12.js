var searchData=
[
  ['undefined_5fcharacters_136',['undefined_characters',['../group__fontgen.html#ga9811e6092e052afd4befbcb07e3456a8',1,'ttf2c']]],
  ['usb_137',['USB',['../group__USB.html',1,'']]],
  ['usb_5finit_138',['usb_init',['../group__USB.html#ga60af0d8621d025f018ba0f56bca00a93',1,'usb_init():&#160;dummy.c'],['../group__USB.html#ga60af0d8621d025f018ba0f56bca00a93',1,'usb_init():&#160;dummy.c'],['../group__USB.html#ga60af0d8621d025f018ba0f56bca00a93',1,'usb_init():&#160;usb.c']]],
  ['usbdfu_5fcontrol_5frequest_139',['usbdfu_control_request',['../group__USB.html#ga4ef6a4aac52187a499c151449e2af490',1,'usbdfu_control_request(usbd_device *usbd_dev, struct usb_setup_data *req, uint8_t **buf, uint16_t *len, void(**complete)(usbd_device *usbd_dev, struct usb_setup_data *req)):&#160;usbdfu.c'],['../group__USB.html#ga4ef6a4aac52187a499c151449e2af490',1,'usbdfu_control_request(usbd_device *usbd_dev, struct usb_setup_data *req, uint8_t **buf, uint16_t *len, void(**complete)(usbd_device *usbd_dev, struct usb_setup_data *req)):&#160;usbdfu.c']]]
];
