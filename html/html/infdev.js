var infdev =
[
    [ "Driver ED060SC4", "Pantalla.html", [
      [ "Driver D'alt nivell", "Pantalla.html#autotoc_md22", null ],
      [ "Referències", "Pantalla.html#autotoc_md23", null ]
    ] ],
    [ "Control d'alimentacions", "Alimentacions.html", [
      [ "Alimentació general i bateria", "Alimentacions.html#autotoc_md10", null ],
      [ "PMIC", "Alimentacions.html#autotoc_md11", [
        [ "Circuit", "Alimentacions.html#autotoc_md12", null ],
        [ "Software", "Alimentacions.html#autotoc_md13", null ],
        [ "Problemàtiques", "Alimentacions.html#autotoc_md14", null ]
      ] ]
    ] ],
    [ "SDIO", "SD.html", [
      [ "Driver de perifèric", "SD.html#autotoc_md28", null ],
      [ "Driver SD", "SD.html#autotoc_md29", null ],
      [ "FatFS", "SD.html#autotoc_md30", null ],
      [ "Altres aspectes", "SD.html#autotoc_md31", null ],
      [ "Referències", "SD.html#autotoc_md32", null ]
    ] ],
    [ "Prototip", "Prototip.html", [
      [ "Forma", "Prototip.html#autotoc_md24", null ],
      [ "Alimentacions", "Prototip.html#autotoc_md25", null ],
      [ "Assignació de pins", "Prototip.html#autotoc_md26", null ],
      [ "Connectivitat", "Prototip.html#autotoc_md27", null ]
    ] ],
    [ "Bootloader", "bootloader.html", [
      [ "Complicacions", "bootloader.html#autotoc_md15", null ],
      [ "Referències", "bootloader.html#autotoc_md16", null ]
    ] ],
    [ "Sistema Operatiu", "SO.html", [
      [ "Codi base", "SO.html#autotoc_md17", null ],
      [ "FatFs", "SO.html#autotoc_md18", null ],
      [ "IDLE", "SO.html#autotoc_md19", null ],
      [ "Pantalla", "SO.html#autotoc_md20", null ],
      [ "Referències", "SO.html#autotoc_md21", null ]
    ] ]
];