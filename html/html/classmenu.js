var classmenu =
[
    [ "menu", "classmenu.html#a4d5f8ec7876bb44301748c2917f87aed", null ],
    [ "~menu", "classmenu.html#a5efbd22f23289b42ed68d2a9bb431f35", null ],
    [ "addOption", "classmenu.html#a017c2abc01edd1500d2ae08bd388184f", null ],
    [ "addPrevOption", "classmenu.html#a0898fc13f9f0c9133c9c98dc540bb0cb", null ],
    [ "addNextOption", "classmenu.html#a675d0dc5c291ce4fb424e26c659d4d61", null ],
    [ "start", "classmenu.html#ac3e2e76904446ccf6945915271a9d62f", null ],
    [ "getEntryAt", "classmenu.html#ab20179e71c709e62e079398a69e7b035", null ],
    [ "active_element", "classmenu.html#a687b3de4555828f401bf55540622f4e6", null ],
    [ "n_items", "classmenu.html#a457aca75dd7504d42e8dd80334ac741f", null ],
    [ "m_prevp", "classmenu.html#ac376f38cbb8e6ce041254ca45e366e4a", null ],
    [ "m_nextp", "classmenu.html#ae7ad6c07647e0503ff39f69b52524211", null ]
];