var classframe =
[
    [ "frame", "classframe.html#a86ec0b54d9b3730a6ff91e91efffeb62", null ],
    [ "~frame", "classframe.html#ac86c6ed097f3c345a5c64d1277c56462", null ],
    [ "render", "classframe.html#a4096e1c9a225cd7056d0a2434e88b875", null ],
    [ "render", "classframe.html#adc8cb21a5f81507ca0291a51efa0efd4", null ],
    [ "getHeight", "classframe.html#afc069a09e62324689a6912d168239371", null ],
    [ "addMargin", "classframe.html#a5794d03bae3541d123eeb7ce55708ae5", null ],
    [ "addMargin", "classframe.html#ab45b9a800d73f0a73fc1e3aca06f9371", null ],
    [ "appendElement", "classframe.html#ae82d9db40b4c160e35123ff564d6a094", null ],
    [ "f_margin", "classframe.html#a4e46ccea167f001b5a2a4259e0c44030", null ],
    [ "f_element_list", "classframe.html#a8d671aefb7c909c7738bc5f778226e9e", null ],
    [ "f_current_height", "classframe.html#a668a3f19fdc6fa997b4a9683bf88943f", null ]
];