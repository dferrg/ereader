var classhtmlParser =
[
    [ "htmlParser", "classhtmlParser.html#a9d0643e9ed17a5fce408ecd773863852", null ],
    [ "readPage", "classhtmlParser.html#abbbabbbba10c196fdc57627b0a10fd1f", null ],
    [ "setPage", "classhtmlParser.html#a37259577cdf772a54baedca71ee5cf57", null ],
    [ "render", "classhtmlParser.html#a1f992bf970a13610780b675d386e76f2", null ],
    [ "render", "classhtmlParser.html#ad721b36e5159c132aa305162fb3bd140", null ],
    [ "parse_paragraph", "classhtmlParser.html#a7b0d1b63ef30f1f3aa28e0003dc93c20", null ],
    [ "parse_title", "classhtmlParser.html#a4692af404e382d5ddbe0104c946195e0", null ],
    [ "parse_body", "classhtmlParser.html#a341db99f8767610e84716f6d32e3ebd8", null ],
    [ "getTitleFont", "classhtmlParser.html#a2abce6b723638719257c1b22cb3720c3", null ]
];