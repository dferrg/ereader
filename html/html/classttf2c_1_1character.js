var classttf2c_1_1character =
[
    [ "__init__", "group__fontgen.html#gaf793debe5ad667272e833759a19db418", null ],
    [ "render", "group__fontgen.html#gaf982b8e104a50edcd34a56f29d51c1fe", null ],
    [ "calculaKerning", "group__fontgen.html#gaee1e47a516857ef9eab3c54ea57ad266", null ],
    [ "writeProperties", "group__fontgen.html#ga0a66eb4222e460746dbf018d83d2f60e", null ],
    [ "kerningStr", "group__fontgen.html#ga0d08f6fbe05c7ea3ee4ed483894a18a2", null ],
    [ "dataLineLen", "group__fontgen.html#ga99391f2218db32532e20e7cf0a174a8c", null ],
    [ "dataSize", "group__fontgen.html#gaf6587aa301d300077ee438dfaec3394c", null ],
    [ "writeBitmap", "group__fontgen.html#ga7fa9307b9972a8b41bd55ebef28f7d76", null ],
    [ "char", "group__fontgen.html#gad1fb56e5872a794de3955a9270cd360b", null ],
    [ "font", "group__fontgen.html#gae8ec8858b60e54af96ec377b27b14734", null ],
    [ "fontSize", "group__fontgen.html#gaf86e94cd2f1367cbe47ce16250081daa", null ],
    [ "bitmap", "group__fontgen.html#gae817e049afe8f378d7704cf310f22f49", null ],
    [ "posX", "group__fontgen.html#ga45f974889e66be24f5056268912e8312", null ],
    [ "posY", "group__fontgen.html#gafe6ae12b28de66fdacb539e3012e151a", null ],
    [ "h", "group__fontgen.html#ga632d1b3b0e1b26594d1d96cc1ee8e0a3", null ],
    [ "w", "group__fontgen.html#gad40bfc09867ebc2199ffac73bfa3aaff", null ],
    [ "kerningLeft", "group__fontgen.html#ga69a301ae9ed50992b998c6d7daec67e6", null ],
    [ "kerningRight", "group__fontgen.html#ga9f356b09af6dd92570c6a5b7f382943b", null ],
    [ "kerningSegments", "group__fontgen.html#ga54dfe6106772ae4f6d8216771258f9c3", null ]
];