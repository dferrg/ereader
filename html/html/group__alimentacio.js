var group__alimentacio =
[
    [ "power_get_battery_voltage", "group__alimentacio.html#ga178fce301ae6abd58f08cab3940390af", null ],
    [ "power_get_battery_percentage", "group__alimentacio.html#ga101b1d8dc71f3bd752953861f66e4e56", null ],
    [ "power_off", "group__alimentacio.html#gafeda4407b9d28026e4de548fbcc6e99c", null ],
    [ "power_init", "group__alimentacio.html#ga1dc48d9f3436d555e44455ddef993630", null ],
    [ "power_battery_charged_cb", "group__alimentacio.html#gaafd6f56f5de855c22e10313cab458c55", null ],
    [ "PMIC_init", "group__alimentacio.html#gaf70911e62c08224b2f34d380964f731b", null ]
];