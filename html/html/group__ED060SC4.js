var group__ED060SC4 =
[
    [ "writeArea_t", "group__ED060SC4.html#structwriteArea__t", [
      [ "x", "group__ED060SC4.html#ga92ee8fc0ca83798e91a1ec071ae6f400", null ],
      [ "y", "group__ED060SC4.html#ga0773c3e5bcd35dce24c7523bac6afac1", null ],
      [ "width", "group__ED060SC4.html#gab2ca52d9a77ac169ec9faa7adfcd5bdf", null ],
      [ "height", "group__ED060SC4.html#gabc4fb4a84ec19a4bb45504029fa22d37", null ]
    ] ],
    [ "screen_init", "group__ED060SC4.html#ga3c051104f4d82fc399ff071d7bf203aa", null ],
    [ "screen_task", "group__ED060SC4.html#ga0c9fd8729034fc5b41f159b3762be486", null ],
    [ "s_eink_clear", "group__ED060SC4.html#gac1cb6055354c8fd72ea24bb37abbf453", null ],
    [ "s_eink_start_write", "group__ED060SC4.html#gaf366d67f9173e3cc42075be69bbfaa0d", null ],
    [ "s_eink_write_buffer", "group__ED060SC4.html#gab4fc2962a5e02e881fcb3768f6301e74", null ]
];