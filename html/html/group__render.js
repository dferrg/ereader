var group__render =
[
    [ "Fonts", "group__fonts.html", "group__fonts" ],
    [ "Text", "group__text.html", "group__text" ],
    [ "margin", "group__render.html#structmargin", [
      [ "top", "group__render.html#acc2c07c0ee01dfe919b696036c74c131", null ],
      [ "bottom", "group__render.html#a428c92ebaefde201b7e75caa2f7b7b24", null ],
      [ "left", "group__render.html#af2d537f8178e08fcd77048e2062ada68", null ],
      [ "right", "group__render.html#ad28e0fef6395577067cde32ef2d12861", null ]
    ] ],
    [ "frame", "classframe.html", [
      [ "frame", "classframe.html#a86ec0b54d9b3730a6ff91e91efffeb62", null ],
      [ "~frame", "classframe.html#ac86c6ed097f3c345a5c64d1277c56462", null ],
      [ "render", "classframe.html#a4096e1c9a225cd7056d0a2434e88b875", null ],
      [ "render", "classframe.html#adc8cb21a5f81507ca0291a51efa0efd4", null ],
      [ "getHeight", "classframe.html#afc069a09e62324689a6912d168239371", null ],
      [ "addMargin", "classframe.html#a5794d03bae3541d123eeb7ce55708ae5", null ],
      [ "addMargin", "classframe.html#ab45b9a800d73f0a73fc1e3aca06f9371", null ],
      [ "appendElement", "classframe.html#ae82d9db40b4c160e35123ff564d6a094", null ],
      [ "f_margin", "classframe.html#a4e46ccea167f001b5a2a4259e0c44030", null ],
      [ "f_element_list", "classframe.html#a8d671aefb7c909c7738bc5f778226e9e", null ],
      [ "f_current_height", "classframe.html#a668a3f19fdc6fa997b4a9683bf88943f", null ]
    ] ],
    [ "element", "classelement.html", [
      [ "element", "classelement.html#ab855762fbfbeb7307a78943b10c3afbb", null ],
      [ "render", "classelement.html#a76fd848c5a80367e7961e89e99e23065", null ],
      [ "element_posY", "classelement.html#a27729a82cb440e738ab7b51d3324f878", null ],
      [ "element_height", "classelement.html#ac0be8ce6e9d90ad4fd1d2be6d6e051e1", null ],
      [ "element_bpp", "classelement.html#aa3fb60a2524fa745429a643400f81f50", null ]
    ] ],
    [ "bloc", "classbloc.html", [
      [ "bloc", "classbloc.html#a682c96fc43ec058e2229bad05776c01b", null ],
      [ "~bloc", "classbloc.html#a68257893dd884fac2e0fb0b279262566", null ],
      [ "setBackground", "classbloc.html#aae2a96db187eb1eb1674602e1c358189", null ],
      [ "end", "classbloc.html#aeb5aedb32e5eccef50d2ed01884d8a8a", null ],
      [ "getMinHeight", "classbloc.html#ae1960a3afeae95f3c6cb71a55ceed8ad", null ],
      [ "getHeight", "classbloc.html#a3a365821e63962cc4912b94ae334fce5", null ],
      [ "setPos", "classbloc.html#a3efc537b8157cd345a53c6302e0b0269", null ],
      [ "setHeight", "classbloc.html#a8d64d3a4123c902b17b5f13e4dd0badd", null ],
      [ "setBpp", "classbloc.html#ac7d97ef9c2243eccb6fd5ce82f3a4665", null ],
      [ "background_color", "classbloc.html#a2cbd1dcb7c82ef95ea2b087654319923", null ]
    ] ],
    [ "segment", "classsegment.html", [
      [ "segment", "classsegment.html#a52879a61f133f8cd3b0afbb1a4ca5de2", null ],
      [ "~segment", "classsegment.html#a5ab70d66c0abd454ce548c087e9299c3", null ],
      [ "render", "classsegment.html#a48b1696c0f89e3cd7ecb1d75475e4cd6", null ],
      [ "segment_width", "classsegment.html#a31ee05fa13abbd22219c3107860c8127", null ]
    ] ]
];