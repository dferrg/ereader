var group__parser =
[
    [ "htmlParser", "classhtmlParser.html", [
      [ "htmlParser", "classhtmlParser.html#a9d0643e9ed17a5fce408ecd773863852", null ],
      [ "readPage", "classhtmlParser.html#abbbabbbba10c196fdc57627b0a10fd1f", null ],
      [ "setPage", "classhtmlParser.html#a37259577cdf772a54baedca71ee5cf57", null ],
      [ "render", "classhtmlParser.html#a1f992bf970a13610780b675d386e76f2", null ],
      [ "render", "classhtmlParser.html#ad721b36e5159c132aa305162fb3bd140", null ],
      [ "parse_paragraph", "classhtmlParser.html#a7b0d1b63ef30f1f3aa28e0003dc93c20", null ],
      [ "parse_title", "classhtmlParser.html#a4692af404e382d5ddbe0104c946195e0", null ],
      [ "parse_body", "classhtmlParser.html#a341db99f8767610e84716f6d32e3ebd8", null ],
      [ "getTitleFont", "classhtmlParser.html#a2abce6b723638719257c1b22cb3720c3", null ]
    ] ],
    [ "mdParser", "classmdParser.html", [
      [ "mdParser", "classmdParser.html#aa2008205f437c5ab728c6eaebff5d27f", null ],
      [ "readPage", "classmdParser.html#aa718c6d4e8a129bff837335100f01829", null ],
      [ "setPage", "classmdParser.html#a91099dbc3813074540ce10a368c0fd13", null ],
      [ "render", "classmdParser.html#a87b13980ef07861378a0e70ab674dc70", null ],
      [ "render", "classmdParser.html#a409d81f959564a09a55ea64aa353ac91", null ],
      [ "start", "classmdParser.html#a45a0778272694d30e6a1d425360348ae", null ],
      [ "saveProgress", "classmdParser.html#a47223d52faa0852c2a3a883be38ec74d", null ],
      [ "parse_paragraph", "classmdParser.html#afd14c8f35fb79541875c1fcc482a3601", null ],
      [ "parse_title", "classmdParser.html#af54d5c6fa29929092d8803322e6ed91d", null ],
      [ "parse_body", "classmdParser.html#a93614a0a631a2f27b54e9723d8ca85bf", null ],
      [ "checkBoldItalic", "classmdParser.html#a4bdcb573ca7fa74d39aef8a5b1079e24", null ],
      [ "getTitleFont", "classmdParser.html#a93b4174ead57b1926397da59e67070c0", null ],
      [ "history_index", "classmdParser.html#a956e025df1952ce082ee55d29a63e9ed", null ],
      [ "last_char", "classmdParser.html#ad272317ae6609d76df2e7b4af4e8ab78", null ],
      [ "underscore_italic", "classmdParser.html#a21bd288ea522008a1df14d4d2cd148ca", null ]
    ] ],
    [ "parser", "classparser.html", [
      [ "parser", "classparser.html#ac4cb16e924a735dfb5837772afa1a1a9", null ],
      [ "parser", "classparser.html#ae9cbc100fd4b8d11f4a4359c4f3185bb", null ],
      [ "~parser", "classparser.html#acdd4eb1b51b876954c2f7605f65388ce", null ],
      [ "readPage", "classparser.html#acfd8aa607ed08498a8d98f9cf7190830", null ],
      [ "setPage", "classparser.html#aad08aedcb21ba0512eb8b77c9fb31f29", null ],
      [ "render", "classparser.html#a6b6588ea34a8c0e553348b780913b52f", null ],
      [ "render", "classparser.html#a9d6c5f86140ed3c3c583298e3ad7fe50", null ],
      [ "start", "classparser.html#ab6ce288aab1ca4e350535a4b085531a1", null ],
      [ "p_frame", "classparser.html#a618ec5d241df3495f84d355a8c5f2244", null ],
      [ "p_next_frame", "classparser.html#a1e72895efc36a31f36989e3a61caabd9", null ],
      [ "p_current_element", "classparser.html#a31e444f106d4e1833d1476848615dd63", null ],
      [ "p_current_page", "classparser.html#a40e1f20aefdeeab4815e7adf6bf00471", null ],
      [ "p_element_tree", "classparser.html#a64d55626c7aea89ca01a7176302afcc3", null ],
      [ "p_current_depth", "classparser.html#a0dea4b756762844a6cec2b50ad775681", null ],
      [ "p_file", "classparser.html#ade14d0df1db40ea8ebcfa60a7a15809b", null ]
    ] ],
    [ "ELEMENT_BODY", "group__parser.html#gae8e7b534d34846dca038c03a341f53e6", null ]
];