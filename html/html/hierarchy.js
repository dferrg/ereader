var hierarchy =
[
    [ "blparams_t", "structblparams__t.html", null ],
    [ "ttf2c.character", "classttf2c_1_1character.html", null ],
    [ "dbdata", "structdbdata.html", [
      [ "mdhist", "structmdhist.html", null ]
    ] ],
    [ "dbtable", "classdbtable.html", null ],
    [ "dir_explorer", "classdir__explorer.html", null ],
    [ "element", "classelement.html", [
      [ "bloc", "classbloc.html", [
        [ "image", "classimage.html", [
          [ "jpgImage", "classjpgImage.html", null ]
        ] ],
        [ "line", "classline.html", null ],
        [ "paragraph", "classparagraph.html", [
          [ "listEntry", "classlistEntry.html", null ],
          [ "menuentry", "classmenuentry.html", null ]
        ] ]
      ] ],
      [ "segment", "classsegment.html", [
        [ "textWord", "classtextWord.html", null ]
      ] ]
    ] ],
    [ "explorer", "classexplorer.html", null ],
    [ "exti_trigger", "structexti__trigger.html", null ],
    [ "FILINFO", "structFILINFO.html", null ],
    [ "font", "group__fonts.html#structfont", null ],
    [ "ttf2c.Font", "classttf2c_1_1Font.html", null ],
    [ "font_char", "group__fonts.html#structfont__char", null ],
    [ "font_group", "group__fonts.html#structfont__group", null ],
    [ "frame", "classframe.html", [
      [ "menu", "classmenu.html", null ]
    ] ],
    [ "item< T >", "classitem.html", null ],
    [ "kerning_contour", "group__fonts.html#structkerning__contour", null ],
    [ "letter", "group__text.html#structletter", null ],
    [ "LibreBaskervilleRegular_19_group", "structLibreBaskervilleRegular__19__group.html", null ],
    [ "LibreBaskervilleRegular_21_group", "structLibreBaskervilleRegular__21__group.html", null ],
    [ "LibreBaskervilleRegular_25_group", "structLibreBaskervilleRegular__25__group.html", null ],
    [ "LibreBaskervilleRegular_33_group", "structLibreBaskervilleRegular__33__group.html", null ],
    [ "list< T >", "classlist.html", null ],
    [ "list< bloc >", "classlist.html", null ],
    [ "list< int >", "classlist.html", null ],
    [ "list< line >", "classlist.html", null ],
    [ "list< textWord >", "classlist.html", null ],
    [ "margin", "group__render.html#structmargin", null ],
    [ "parser", "classparser.html", [
      [ "htmlParser", "classhtmlParser.html", null ],
      [ "mdParser", "classmdParser.html", null ]
    ] ],
    [ "RobotoRegular_21_group", "structRobotoRegular__21__group.html", null ],
    [ "RobotoRegular_25_group", "structRobotoRegular__25__group.html", null ],
    [ "RobotoRegular_33_group", "structRobotoRegular__33__group.html", null ],
    [ "screenCmd", "structscreenCmd.html", null ],
    [ "writeArea_t", "group__ED060SC4.html#structwriteArea__t", null ]
];