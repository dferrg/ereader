var group__fonts =
[
    [ "kerning_contour", "group__fonts.html#structkerning__contour", [
      [ "high_despl", "group__fonts.html#aa08fd95e7941f3815d3b9019330b6748", null ],
      [ "midHigh_despl", "group__fonts.html#aa9a4fe9e5cea1abd763831f5eb4f2cd5", null ],
      [ "midLow_despl", "group__fonts.html#aa222d90821e9e2fc9d356f57abb75e0f", null ],
      [ "low_despl", "group__fonts.html#a855aec426823cd1e1ef0ae1a1ffff4b5", null ]
    ] ],
    [ "font_char", "group__fonts.html#structfont__char", [
      [ "bitmapOffset", "group__fonts.html#ab036719c6e652ad99e2e14777a3079f7", null ],
      [ "posY", "group__fonts.html#a821b1f693e226a7e697af1f358748a47", null ],
      [ "posX", "group__fonts.html#a546602826d7b01b3eb397300c74353c5", null ],
      [ "h", "group__fonts.html#ae33b89946959069e164d54248553f0b5", null ],
      [ "w", "group__fonts.html#a51e96db4fef20a20ee8bb5147477dd68", null ],
      [ "kerningL", "group__fonts.html#a88f04c247eb98c621194c2ab9a88a767", null ],
      [ "kerningR", "group__fonts.html#a26e548aa93aa4d69f0687c00e94a01eb", null ]
    ] ],
    [ "font", "group__fonts.html#structfont", [
      [ "size", "group__fonts.html#a3f484bc38fdfd67c3d8dcfa49bb6e356", null ],
      [ "lineSpacing", "group__fonts.html#a8351e286e56b12d313804fae1800ac4b", null ],
      [ "charSpacing", "group__fonts.html#a26e5950e5a37a0f706f7a99cd074cd88", null ],
      [ "charLookUp", "group__fonts.html#aaed29237c3e84f4993e2372495c89332", null ],
      [ "bitmaps", "group__fonts.html#ad55e42056d3c892161d8ed39bd0095a2", null ]
    ] ],
    [ "font_group", "group__fonts.html#structfont__group", [
      [ "f_regular", "group__fonts.html#af963918017010f90ab15999e51d59733", null ],
      [ "f_bold", "group__fonts.html#a65e5083bc857baff1b30094205891c32", null ],
      [ "f_italic", "group__fonts.html#a57844ec3489d5c36ba6af20f6acceffc", null ]
    ] ]
];