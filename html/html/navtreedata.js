/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Ook! eReader", "index.html", [
    [ "Lector de llibres electrònics Ook!", "index.html", [
      [ "Programari", "index.html#autotoc_md2", [
        [ "Simulador", "index.html#autotoc_md3", null ]
      ] ],
      [ "Maquinari", "index.html#autotoc_md4", null ],
      [ "Línies de continuació", "index.html#autotoc_md5", null ]
    ] ],
    [ "Arxius del projecte", "arxius.html", "arxius" ],
    [ "Carcasa impresa en 3D", "carcasa.html", null ],
    [ "Galeria d'imatges", "galeria.html", null ],
    [ "Compilació del codi font", "instalacio.html", [
      [ "Obté el codi", "instalacio.html#autotoc_md34", null ],
      [ "Afegeix l'arquitectura i386 al gestor de paquets", "instalacio.html#autotoc_md35", null ],
      [ "Instal·la les dependències", "instalacio.html#autotoc_md36", null ],
      [ "Compilació del simulador", "instalacio.html#autotoc_md37", null ],
      [ "Compilació pel dispositiu", "instalacio.html#autotoc_md38", null ],
      [ "Gravat", "instalacio.html#autotoc_md39", null ]
    ] ],
    [ "Presentació del projecte", "presentacio.html", null ],
    [ "Proposta d'implementació", "prop.html", null ],
    [ "Mòduls", "modules.html", "modules" ],
    [ "Estructures de Dades", "annotated.html", [
      [ "Estructures de Dades", "annotated.html", "annotated_dup" ],
      [ "Índex d'Estructures de Dades", "classes.html", null ],
      [ "Jerarquia de Classes", "hierarchy.html", "hierarchy" ],
      [ "Camps de Dades", "functions.html", [
        [ "Tot", "functions.html", null ],
        [ "Funcions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Fitxers", "files.html", [
      [ "Llista dels Fitxers", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Alimentacions.html",
"classparagraph.html#a76e9e1e1ec8d9727f9f2832f91cae559",
"presentacio.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';