var classdbtable =
[
    [ "dbtable", "classdbtable.html#ab1ba63c5aebbfc87d3e5df7f52406936", null ],
    [ "getEntry", "classdbtable.html#ae00d087250d969f6e36dbf636eb7276f", null ],
    [ "getEntry", "classdbtable.html#adad3610451a3579df503635594245d30", null ],
    [ "addEntry", "classdbtable.html#ac91c61fc0370a9a7b505e5ef3633b29d", null ],
    [ "addEntry", "classdbtable.html#aa9bfe43dde241c7cbcbef0fbe9ac8d47", null ],
    [ "readNextEntry", "classdbtable.html#a2d3b621cd7727fb96cac6253ff009fdc", null ],
    [ "readNextHeader", "classdbtable.html#a40bba9270c74d458151ac27479ae4fce", null ],
    [ "copyBuffer", "classdbtable.html#af47fa1aa17f1f4958ded479bf057aa4c", null ],
    [ "size", "classdbtable.html#a92a61863ec530c3ed056ec61bace4691", null ],
    [ "path", "classdbtable.html#aaac29d559d4b6983f0481b4896025569", null ]
];