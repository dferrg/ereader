# Informe inicial {#infinic}
\htmlonly
<a name="changelog"></a><h2 id=changelog">Historial de canvis</h2>
<div>
<table class="changelog">
<tr><th> Versió </th><th>Data del canvi</th><th>Motiu del canvi</th><th>Canvi efectuat</th></tr>
<tr><td>1.0 (proposta)</td><td>18/02/2021</td><td>Redacció inicial</td><td>Redacció inicial de la introducció i els objectius. Portada, index i estructura del document.</td></tr>
<tr><td>1.0 (proposta)</td><td>23/02/2021</td><td>Redacció inicial</td><td>Redacció inicial de la metodologia. Canvis a la redacció dels objectius, afegides fonts d'informació a la introducció.</td></tr>
<tr><td>1.1</td><td>24/02/2021</td><td>Indicació del tutor</td><td>Mogudes les referències del peu de pàgina al final.</td></tr>
<tr><td>1.1</td><td>25/02/2021</td><td>Redacció inicial</td><td>Afegida la planificació.</td></tr>
<tr><td>1.1</td><td>27/02/2021</td><td>Indicació del tutor.</td><td>Afegida explicació del concepte de kerning en fonts de text. (pg.6, peu)</td></tr>
<tr><td>1.1</td><td>27/02/2021</td><td>Indicació del tutor.</td><td>Explicació libopencm3, canvi en l'explicació del bootloader.(pg.5, para.4)</td></tr>
<tr><td>1.1</td><td>27/02/2021</td><td>Indicació del tutor.</td><td>Esmentar el consum energètic reduït d'un micro vers un SoC. (pg.3, para.1)</td></tr>
<tr><td>1.1</td><td>27/02/2021</td><td>Redacció inicial</td><td>Repàs general.</td></tr>
</tr>
</table>
</div>
<a name="document"></a><h2 id=document">Document</h2>
<embed src="../content/Documentació/informes/Informe Inicial.pdf" class="fullwidth" width="100%" style="min-height: 800px !important; min-width: 1000px;" href="../content/Documentació/informes/Informe Inicial.pdf"></embed>
\endhtmlonly

































