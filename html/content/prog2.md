# Informe de progrés II {#infprog2}
\htmlonly
<a name="changelog"></a><h2 id=changelog">Historial de canvis</h2>
<div>
<table class="changelog">
<tr><th> Versió </th><th>Data del canvi</th><th>Motiu del canvi</th><th>Canvi efectuat</th></tr>
<tr><td>1.0 (proposta)</td><td>20/05/2021</td><td>Redacció inicial</td><td>Redacció inicial de la introducció i la metodologia actualitzada. Portada, index i estructura del document.</td></tr>
<tr><td>1.0 (proposta)</td><td>22/05/2021</td><td>Redacció inicial</td><td>Explicació del que s'ha desenvolupat. Simulador i renderització.</td></tr>
<tr><td>1.0 (proposta)</td><td>24/05/2021</td><td>Redacció inicial</td><td>Explicació del que s'ha desenvolupat. Ajustos previs i Lectura d'arxius.</td></tr>
<tr><td>1.0 (proposta)</td><td>24/05/2021</td><td>Redacció inicial</td><td>Resultats.</td></tr>
<tr><td>1.0 (proposta)</td><td>24/05/2021</td><td>Redacció inicial</td><td>Repàs general.</td></tr>
<tr><td>1.1</td><td>27/05/2021</td><td>Indicació del tutor</td><td>Ampliació de l'explicació del parser.</td></tr>
<tr><td>1.1</td><td>27/05/2021</td><td>Indicació del tutor</td><td>Explicació de l'esquema de memòria usat i prevenció de fragmentació.</td></tr>
<tr><td>1.1</td><td>27/05/2021</td><td>Indicació del tutor</td><td>Canvis menors.</td></tr>
</tr>
</table>
</div>
<a name="document"></a><h2 id=document">Document</h2>
<embed src="../content/Documentació/informes/Informe de progrés II.pdf" width="100%" style="min-height: 800px !important; min-width: 1000px;" href="../content/Documentació/informes/Informe de progrés II.pdf"></embed>
\endhtmlonly

































