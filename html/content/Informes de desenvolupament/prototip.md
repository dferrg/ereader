# Prototip {#Prototip}
A meitat de projecte, un cop provats els diversos mòduls per separat, s'ha dissenyat un prototip que hauria de ser el hardware final del projecte. S'ha intentat mantindre dins del possible tot el disseny anterior, corregint els errors, per tal d'evitar sorpreses, però ha estat inevitable introduir alguns canvis. També s'ha dissenyat la carcassa del dispositiu.

## Forma
El primer que es va fixar va ser la part mecànica del llibre, ja que la superfície del llibre és força gran i el hardware relativament senzill com per cabre-hi sense problemes. A la vora de la pantalla s'han situat els botons (UP, DOWN, OK, BACK, INDEX, HOME i POWER) i forats pel muntatge. A més, s'ha intentat que la pantalla quedés situada de forma simètrica. La bateria queda al mig de la placa, sota la pantalla. El resultat es mostra a la imatge.

![content](images/2021/04/vista-frontal.png)

## Alimentacions
El circuit s'ha de poder alimentar a partir d'una bateria de liti. Això requereix per començar d'un gestor de càrrega, en aquest cas un MCP73031, que carrega primer a una intensitat constant de 500mA i, un cop arriba a 4.2V, mantingui aquesta tensió fins la càrrega completa.

En la fase de descàrrega la bateria ofereix una tensió depenent de la càrrega restant, tal com es mostra a la imatge:

![content](images/2021/04/grafic-de-descarrega-d-una-bateria-de-liti.png)

Si volem obtenir una tensió fixa de 3,3v, hem d'usar un regulador de baixa caiguda (LDO) per tal de poder aprofitar al màxim la bateria. Una altra opció que ens donaria més flexibilitat seria usar un regulador de 3,1v, però podria donar problemes en certs components com la targeta SD.

Com que volem que la bateria es carregui amb intensitat constant, durant la càrrega hauríem d'alimentar el circuit des de l'USB, cosa que requereix d'un circuit de selecció de font de corrent. Algunes opcions són:
 * **Un circuit integrat de gestió de bateria**. Implicaria substituir el gestor de càrrega per un IC més complex. Són força més cars i complexos, però ofereixen una protecció extra per la bateria. En aquest cas, la bateria ja porta les proteccions necessàries.
 * **Usar un diode des de la bateria**. Quan l'USB no està connectat, la bateria alimentaria el circuit. Quan es connecta, la tensió de l'USB és més alta i el diode tanca el pas. El problema d'aquesta implementació és, precisament, que els diodes tenen una caiguda de tensió massa alta i ens obliga a usar un regulador de 3,1v.
 * **Usar un PMOS**. El PMOS s'activa mitjançant un pull-down quan l'USB no està connectat i es desactiva quan es connecta, tancant el pas a la bateria. La resistència interna del PMOS quan està actiu és de menys d'una dècima d'ohm, el que permet caigudes de tensió de 5 centèsimes de volt en condicions normals.

S'ha optat pel PMOS per la simplicitat i el cost. Així, els reguladors poden ser de 3,3v. Pel que fa al consum, hem de tenir en compte que cada regulador és capaç d'oferir fins a 300mA constants i fins a 500mA en moments determinats, cosa que no seria suficient per a tot el circuit. El mòdul bluetooth necessita un regulador propi, i s'ha afegit un altre per a la pantalla, sumant 3 en total. El PMIC s'ha deixat sense canvis amb respecte al circuit de prova, tret d'un canvi en els inductors. En la imatge es veu el gestor de càrrega a dalt a l'esquerra, la selecció de font a dalt a la dreta i el regulador principal a sota.

![content](images/2021/04/gestio-d-alimentacions.png)

## Assignació de pins
S'han usat en total 51 entrades i sortides del micro. En resum:
 * Per a la pantalla s'usen 8 línies de GPIO contigues per tal de poder accedir a elles mitjançant DMA. Les línies de clock s'han seleccionat per que puguin ser controlades per un timer sincronitzat al DMA del bus de dades.
 * El PMIC usa una connexió I2S a part de diverses E/S digitals.
 * El mòdul WiFi/bluetooth usa UART i SPI, a més d'un parell de línies per a la selecció de boot i el reset.
 * S'ha afegit un DAC d'audio per a futur desenvolupament que es controla mitjanant I2S.
 * La SD necessita 6 linies per al protocol SDIO.

## Connectivitat
Pel mòdul bluetooth s'ha escollit un ESP32 S2. La idea original era usar el mòdul wroover, que incorpora la circuiteria bàsica per al mòdul incloent l'antena, però ocupa 3,5mm d'alçada i obligaria a fer més gruix tot el dispositiu. Així doncs s'ha optat per usar només el microcontrolador i implementar la circuiteria necessària, tot i que per fer proves s'ha deixat a la placa les connexions per al mòdul sencer.
