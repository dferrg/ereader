# Driver ED060SC4 {#Pantalla}
La pantalla funciona internament amb dos registres de desplaçament, un per les línies i un altre per les columnes, i un registre de dades, que guarda tota una columna. El funcionament bàsic és el següent:
1. S'inicialitza el frame, posant el registre de desplaçament de columnes a zero.
2. S'inicialitza la columna, posant el registre de desplaçament de files a zero.
3. S'envien de 4 en 4 les dades a escriure, podent escriure cada píxel en blanc, en negre o deixant-lo com estigui.
4. S'escriu tota la columna d'una vegada.
5. Es torna al punt 2 mentre no acabem el frame.

![content](../img/eink_panel-medium.png)

Cada una d'aquestes accions es realitza generant en els senyals de control una forma d'ona predefinida, i algunes d'elles varien en funció del mètode d'escriptura. Quan volem escriure en escala de grisos, a més de variar les formes d'ona, hem d'escriure en diversos passos, escrivint un frame sencer per a cada tonalitat, i variant el temps d'escriptura en funció d'aquesta.\\

S’ha implementat un driver simple amb funcions per a esborrar i escriure la pantalla sencera o regions d’aquesta, tant en blanc i negre com en escala de grisos. En el segon prototip, però, no hi ha hagut temps d'ajustar els temps d'escriptura per a una bona gamma de grisos.

# Driver D'alt nivell
Més endavant es va usar aquest driver per a implementar un de més alt nivell d'abstracció, integrat amb freeRTOS. Aquest driver s'executa en una tasca a part, que va rebent comandes i segments del bitmap de la pantalla mitjançant cues, imprimint-les a la pantalla. Això, a més d'oferir una interfície més simple al programari d'aplicació, permetrà renderitzar alhora que s'escriu per pantalla un cop s'implementi el driver amb timers i DMA, agilitzant tot el procés.

# Referències
 * https://essentialscrap.com/eink
 * http://spritesmods.com/?art=einkdisplay
 * https://github.com/take-i/espeink/blob/master/user/eink.c
