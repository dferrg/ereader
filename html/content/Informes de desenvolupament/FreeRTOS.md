# Sistema Operatiu {#SO}
Per tal d'implementar funcions multitasking com, per exemple, la carrega de contingut en el rerefons, és necessari usar un kernel com FreeRTOS. D'entre totes les opcions disponibles s'ha triat FreeRTOS per la documentació disponible i per la simplicitat. Alternativament es pot tractar d'usar ChibiOS o usar el kernel d'un altre RTOS com mynewt o NuttX.

Seria interessant usar un RTOS complet enlloc de només el kernel, però això implica un esforç extra per tal d'adaptar el codi i implementar correctament els drivers (especialment en NuttX) que queda fora de l'abast del projecte.

## Codi base
Abans d'adaptar el codi existent a FreeRTOS s'ha generat una plantilla que compila i executa un exemple amb FreeRTOS i libopencm3. Realitzar aquesta plantilla no ha estat trivial. A més de les multiples variables de configuració que ofereix FreeRTOS, ha calgut adaptar certes parts del codi a la llibreria libopencm3. Ha sigut problematic allotjar correctament les rutines de servei d'interrupció del sistema (systick entre altres) en la taula de vectors, i fer que el codi funcionés amb un bootloader.

S'ha optat per compilar FreeRTOS com una llibreria estàtica i incloure-la al linkador de l'aplicació. Pel que fa als ISRs, s'han de cridar des de l'aplicació, per tal de separar FreeRTOS del driver de dispositiu (libopencm3). Així, es defineix en algun punt de l'aplicació la ISR que cridarà a la funció corresponent de FreeRTOS.

## FatFs
La llibreria de sistema de fitxers ja comptava amb un mecanisme de locks implementat usant els SO mes comuns, entre ells FreeRTOS. Només ha fet falta canviiar certs paràmetres de configuració i incloure les llibreries de sistema a les del FS.

## IDLE
Per tal de reduir el consum energètic, s'apagarà el nucli ARM quan no hi hagi cap tasca en marxa. S'implementarà mitjançant una IDLE Hook de Freertos, que consisteix en una funció que crida el sistema quan no hi ha cap tasca executant-se. Aqui, es posarà el micro en mode sleep amb la instrucció WFI, i tornarà a funcionar quan hi hagi alguna interrupció. Més endavant es valorarà desactivar alguns dels perifèrics abans d'entrar en mode sleep o desactivar alguna interrupció com el systick.

## Pantalla
El funcionament de la pantalla amb el sistema operatiu és una mica més complexe. No es pot refrescar la pantalla sencera d'una sola vegada ja que no tenim prou memòria disponible. Tampoc es pot permetre que una tasca interrompi l'escriptura d'una altra (encara que això no hauria de passar ja que usem un sol renderitzador). Així doncs s'implementarà un lock que asseguri un únic accés concurrent.

A més, tot i que ara mateix no s'està usant DMA, es pot aprofitar el temps d'escriptura de cada línia per anar renderitzant les següents. S'ha d'implementar un doble buffer de manera que el mòdul de pantalla vagi llegint d'una meitat mentre l'aplicació va escrivint a l'altre. La comunicació es pot efectuar mitjançant semàfors. Això ha d'ocorrer de la manera més transparent possible, amb les següents funcions.
  * Inici d'escriptura: inclou els paràmetres usats (àrea a escriure/esborrar, profunditat de gris, etc). El mòdul de pantalla es bloqueja i es queda a l'espera de rebre les dades, si cal. Si es vol esborrar només es bloqueja fins que acaba l'operació, despres es desbloqueja automàticament.
  * Envia dades: envia una porcio de les dades i espera a poder modificar l'altra part del buffer. Evita accés simultani per part del driver i l'aplicació amb una visió força transparent.

## Referències
 * Guia d'iniciació de FreeRTOS: https://www.freertos.org/a00104.html#getting-started
 * Beginning STM32: Developing with FreeRTOS, libopencm3 and GCC - Warren Gay
 * FreeRTOS porting guide: https://www.freertos.org/FreeRTOS-porting-guide.html
 * Drivers amb NuttX: https://nuttx.apache.org/docs/latest/guides/drivers.html
 * Configuracions de FreeRTOS: https://www.freertos.org/a00110.html
