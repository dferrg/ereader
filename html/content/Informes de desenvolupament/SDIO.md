# SDIO {#SD}
En aquest apartat es tracta la implementació del driver de la SD mitjançant el perifèric SDIO del microcontrolador.
## Driver de perifèric
Pel que fa al driver del micro en sí, no es va preveure que la llibreria libopencm3 no en dongués suport. Tot i contenir les definicions dels registres, no hi ha cap funció implementada, ni tan sols per enviar comandes. Aquest codi, per tant, s'ha hagut d'implementar de zero. No ha estat una tasca massa difícil ja que el micro usat disposa d'una bona documentació oficial i, tot i que el perifèric és complexe i té molts paràmetres de configuració, majoritàriament serveix la configuració per defecte.

Un problema que va sorgir en aquest punt va ser la configuració dels pins de sortida, que no portaven el pull-up necessari a la placa de desenvolupament i s'ha hagut d'usar els interns del micro un cop trobat el problema. No s'ha de confiar cegament en HWs aliens.

![L'SD hauria de portar pull-up en les línies de dades, CMD i CD](images/2021/03/sd-connection.png)

## Driver SD
Pel que fa al driver de la SD la cosa no ha estat tan simple. Tot i ser un protocol tan estès, no està gaire bé documentat. Es troben a faltar manuals de referència i documentació essencials sobre el protocol d'inicialització, els paràmetres de configuració de la SD i els registres d'error d'aquesta. Sovint fa l'efecte que estàs provant a cegues fins que funcioni. S'ha hagut de consultar altres codis com a referència. S'ha perdut una setmana del projecte per aquest problemes, a més del que ja s'havia planificat.

Molta cura amb els temps entre lectura i escritura, s'ha d'assegurar abans que la tarja estigui disponible. Possible punt de fallida de tot el sistema.

Un altre problema va ser que es va cometre un error en l'ordre dels arguments a la funció d'escriptura de blocs.

## FatFS
La integració amb FatFs ha resultat força simple. S'han implementat les funcions disk_initialize, disk_status, disk_read, disk_write i disk_ioctl. Pel que fa a l'ultima s'hauràn de fer modificacions més endavant, però per a l'abast d'aquest projecte és suficient. També caldrà implementar get_fattime un cop funcioni el rellotge RTC del sistema.


## Altres aspectes

## Referències
 * Codi d'exemple: https://github.com/yigiter/Sample-STM32F4-codes/blob/master/SDIOLib/src/SDIO.c
 * Especificació SD: http://ugweb.cs.ualberta.ca/~c274/resources/hardware/SDcards/SD_SDIO_specsv1.pdf
 * Manual de referència stm32: https://www.st.com/resource/en/reference_manual/dm00031020-stm32f405-415-stm32f407-417-stm32f427-437-and-stm32f429-439-advanced-arm-based-32-bit-mcus-stmicroelectronics.pdf
 * Codis d'exemple stm32cubeIDE
 * Ús de SD via SPI (relacionat): http://elm-chan.org/docs/mmc/mmc_e.html
 * Descripció de la inicialització d'una SD: http://bikealive.nl/sd-v2-initialization.html
 * Descripció de les comandes de SDIO: https://www.fatalerrors.org/a/sd-card-initialization-and-command-details.html
