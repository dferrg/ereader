# Bootloader {#bootloader}
Pel que fa al bootloader s'ha optat per adaptar el codi d'exemple de DFU de la lliberia libopencm3 al projecte. DFU és un standard d'actualitzacions de firmware mitjançant USB. El codi d'exemple ens proporciona els descriptors de dispositiu, configuració, interfície i endpoints de USB per al bootloader, i s'ha hagut d'implementar el control d'accés a aplicació i l'escriptura i esborrat de la flash. També s'ha implementat la part d'aplicació, que consisteix en una modificació dels descriptors anteriors que permet a entrar al bootloader des de l'eina de línia de comandes dfu-util.

## Complicacions
La part més complexa ha estat el reinicis entre bootloader i aplicació. El micro usat té una petita regió de memòria que es manté entre resets sempre que es mantingui tensió a la línia Vbat. Són els registres de backup, i en podem usar un per comunicar-nos entre l'aplicació i el bootloader. Així, quan l'aplicació vulgui entrar en mode bootloader (per que rep una petició des del PC), només ha d'escriure un valor concret en aquest registre i reiniciar. Quan el bootloader, que és el primer que s'inicia en arrencar, vegi aquest valor, sabrà que ha d'entrar en mode programació.

Quan aquest valor no hi sigui a l'arrencada, se saltarà a l'aplicació. Per fer-ho cal canviar l'adreça del vector d'interrupcions i reinicialitzar l'stack pointer.

# Referències
 * Exemple d'implementació amb libopencm3: https://github.com/libopencm3/libopencm3-examples/blob/master/examples/stm32/f1/stm32-h103/usb_dfu/usbdfu.c
 * Documentació oficial de la classe DFU: https://www.usb.org/sites/default/files/DFU_1.1.pdf
 * Bootloader implementat amb libopencm3: https://github.com/lupyuen/codal-libopencm3/blob/master/stm32/bootloader/bootloader.c
