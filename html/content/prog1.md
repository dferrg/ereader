# Informe de progrés I {#infprog1}
\htmlonly
<a name="changelog"></a><h2 id=changelog">Historial de canvis</h2>
<div>
<table class="changelog">
<tr><th> Versió </th><th>Data del canvi</th><th>Motiu del canvi</th><th>Canvi efectuat</th></tr>
<tr><td>1.0 (proposta)</td><td>10/04/2021</td><td>Redacció inicial</td><td>Redacció inicial de la introducció i la metodologia actualitzada. Portada, index i estructura del document.</td></tr>
<tr><td>1.0 (proposta)</td><td>12/04/2021</td><td>Redacció inicial</td><td>Redacció del desenvolupament del prototip i els drivers.</td></tr>
<tr><td>1.0 (proposta)</td><td>14/04/2021</td><td>Redacció inicial</td><td>Redacció de l'adaptació al SO i del prototip candidat.</td></tr>
<tr><td>1.0 (proposta)</td><td>18/04/2021</td><td>Redacció inicial</td><td>Redacció la nova planificació i el seguiment. Actualització d'objectius.</td></tr>
<tr><td>1.0 (proposta)</td><td>20/04/2021</td><td>Redacció inicial</td><td>Repàs general</td></tr>
<tr><td>1.1</td><td>22/04/2021</td><td>Indicació del tutor</td><td>Canvia el terme "llibreria" pel més correcte "biblioteca".</td></tr>
</tr>
</table>
</div>
<a name="document"></a><h2 id=document">Document</h2>
<embed src="../content/Documentació/informes/Informe de progrés I.pdf" width="100%" style="min-height: 800px !important; min-width: 1000px;" href="../content/Documentació/informes/Informe de progrés I.pdf"></embed>
\endhtmlonly

































