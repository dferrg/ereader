# Informe final {#inffin}
<div>
<table class="changelog">
<tr><th> Versió </th><th>Data del canvi</th><th>Motiu del canvi</th><th>Canvi efectuat</th></tr>
<tr><td>1.0 (proposta)</td><td>20/06/2021</td><td>Redacció inicial</td><td>Entrega proposta.</td></tr>

<tr><td>1.1</td><td>24/06/2021</td><td>Indicació del tutor</td><td>Afegir fotos.</td></tr>
<tr><td>1.1</td><td>24/06/2021</td><td>Indicació del tutor</td><td>Canviar captura del simulador per una més clarificadora.</td></tr>
<tr><td>1.1</td><td>24/06/2021</td><td>Indicació del tutor</td><td>Canvis en l'explicació de la decodificació de pàgines (5.3.2).</td></tr>
<tr><td>1.1</td><td>24/06/2021</td><td>Indicació del tutor</td><td>Afegit annex amb resum de markdown i esquemàtic.</td></tr>
<tr><td>1.1</td><td>24/06/2021</td><td>Indicació del tutor</td><td>Ampliada l'explicació del pas de C a C++.</td></tr>

</tr>
</table>
</div>
\htmlonly
<embed src="../content/Documentació/informes/ereader.pdf" width="100%" style="min-height: 800px !important; min-width: 1000px;" href="../content/Documentació/informes/ereader.pdf"></embed>
\endhtmlonly

































