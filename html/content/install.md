# Compilació del codi font {#instalacio}
#### Nota: nstruccions provades a una instal·lació fresca de Xubuntu 20.10
## Obté el codi
> sudo apt update
> sudo apt upgrade
> sudo apt install git

> git clone https://gitlab.com/dferrg/ereader
> cd ereader

## Afegeix l'arquitectura i386 al gestor de paquets
> sudo dpkg --add-architecture i386
> sudo apt update 

## Instal·la les dependències
> sudo apt install python3 python3-pip build-essential gcc-multilib g++-multilib libgtk-3-dev libgtk-3-dev:i386 crossbuild-essential-i386 pkgconf:i386 pkg-config:i386 mtools doxygen
> pip3 install freetype-py numpy opencv-python 

## Compilació del simulador
> make pc
> bin/ook

## Compilació pel dispositiu
> make

## Gravat
> sudo dfu-util -s 0x08004000 -D bin/ook.bin -R
