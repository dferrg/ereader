/*-----------------------------------------------------------------------*/
/* Low level disk I/O module SKELETON for FatFs     (C)ChaN, 2019        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "ff.h"			/* Obtains integer types */
#include "diskio.h"		/* Declarations of disk functions */
#include "SD.h"
/* Definitions of physical drive number for each drive */
#define DEV_RAM		0	/* Example: Map Ramdisk to physical drive 0 */
#define DEV_MMC		1	/* Example: Map MMC/SD card to physical drive 1 */
#define DEV_USB		2	/* Example: Map USB MSD to physical drive 2 */

static volatile DSTATUS sd_status;

/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
	BYTE pdrv		/* Physical drive nmuber to identify the drive */
)
{
	if (pdrv)
		return STA_NOINIT;			/* Respon només pel disc 0 (SD) */

	return sd_status;
}



/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
	BYTE pdrv				/* Physical drive nmuber to identify the drive */
)
{

	if (pdrv)
		return STA_NOINIT;			/* Inicialitza només el disc 0 (SD) */

	sdio_init();
	if(SD_init())
		return STA_NODISK;

	return 0; // STA_OK
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
	BYTE pdrv,		/* Physical drive nmuber to identify the drive */
	BYTE *buff,		/* Data buffer to store read data */
	LBA_t sector,	/* Start sector in LBA */
	UINT count		/* Number of sectors to read */
)
{

	if (pdrv || !count) return RES_PARERR;		/* Check parameter */
	if (sd_status & STA_NOINIT) return RES_NOTRDY;	/* Check if drive is ready */

	if(count>1)
		    for(int i=0;i<count; i++)
		sdio_read_block(buff+i*512, sector+i);
	else
		sdio_read_block(buff, sector);

	return RES_OK;
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if FF_FS_READONLY == 0

DRESULT disk_write (
	BYTE pdrv,			/* Physical drive nmuber to identify the drive */
	const BYTE *buff,	/* Data to be written */
	LBA_t sector,		/* Start sector in LBA */
	UINT count			/* Number of sectors to write */
)
{
	if (pdrv || !count) return RES_PARERR;		/* Check parameter */
	if (sd_status & STA_NOINIT) return RES_NOTRDY;	/* Check if drive is ready */

	if(count>1){
	    int i=0;
	    //for(int i=0;i<count; i+=10)
		sdio_write_blocks(buff, sector, count);
	}else
		sdio_write_block(buff, sector);

	return RES_OK;
}

#endif


/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
	DRESULT res;
	BYTE n;
	DWORD csd[4];
	DWORD st, ed, csize;
	LBA_t *dp;


	if (pdrv) return RES_PARERR;					/* Check parameter */
	if (sd_status & STA_NOINIT) return RES_NOTRDY;	/* Check if drive is ready */

	res = RES_ERROR;

	switch (cmd) {
		case CTRL_SYNC :
			break;

		case GET_SECTOR_COUNT :	/* Get drive capacity in unit of sector (DWORD) */
            *((LBA_t*)buff) = sdio_get_sector_count();
            res = RES_OK;
			break;

		case GET_BLOCK_SIZE :	/* Get erase block size in unit of sector (DWORD) */

			*(DWORD*)buff = sdio_get_block_size();
			break;

		case CTRL_TRIM :	/* Erase a block of sectors (used when _USE_ERASE == 1) */
			/*if (disk_ioctl(drv, MMC_GET_CSD, csd)) break;
			if (!(csd[10] & 0x40)) break;
			dp = buff; st = (DWORD)dp[0]; ed = (DWORD)dp[1];
			if (!(CardType & CT_BLOCK)) {
				st *= 512; ed *= 512;
			}
			if (send_cmd(CMD32, st) == 0 && send_cmd(CMD33, ed) == 0 && send_cmd(CMD38, 0) == 0 && wait_ready(30000)) {
				res = RES_OK;
			}*/
			break;

		/* Following commands are never used by FatFs module */

		case MMC_GET_TYPE:		/* Get MMC/SDC type (BYTE) */
			*(BYTE*)buff = SDHC;
			res = RES_OK;
			break;

		case MMC_GET_CSD:		/* Read CSD (16 bytes) */
			sdio_read_csd(buff);
			res = RES_OK;

			break;

		case MMC_GET_CID:		/* Read CID (16 bytes) */
			sdio_read_cid(buff);
			res = RES_OK;

			break;

		case MMC_GET_OCR:		/* Read OCR (4 bytes) */
			((DWORD*)buff)[0]=sdio_get_ocr();
			res = RES_OK;

			break;

		case MMC_GET_SDSTAT:	/* Read SD status (64 bytes) */
			/*if (send_cmd(ACMD13, 0) == 0) {	// SD_STATUS
				xchg_spi(0xFF);
				if (rcvr_datablock((BYTE*)buff, 64)) res = RES_OK;
			}*/
			break;

		default:
			res = RES_PARERR;
	}

	return res;
}

DWORD get_fattime (void){
	return 0x6052213D;
}
