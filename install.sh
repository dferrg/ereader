#!/bin/bash

apt-get install debootstrap schroot

# Create chroot
mkdir -p ${PWD}/build/chroot
cat > /etc/schroot/chroot.d/ereader << EOF
[build_i386]
description=debian build environment for i386
directory=${PWD}/build/chroot
root-users=${USER}
type=directory
users=${USER}
EOF

debootstrap --variant=buildd --arch i386 bullseye ${PWD}/build/chroot http://deb.debian.org/debian/

schroot -l || { echo "Failed generating chroot" ; exit 1 ; }


# install build dependencies inside chroot
sudo schroot -c chroot:build_i386 -u root -- sh -c "apt install python3 python3-pip build-essential gcc g++ libgtk-3-dev pkgconf mtools doxygen"
sudo schroot -c chroot:build_i386 -u root -- sh -c "pip3 install freetype-py numpy"
