# Lector de llibres electrònics Ook!
Lector de llibres electrònics desenvolupat com a projecte de fi d'estudis d'enginyeria informàtica. El projecte se centra en la construcció d'un dispositiu lliure i de baix cost, basat en un microcontrolador ARM i la pantalla de tinta electrònica ED060SC4. Presenta ara per ara la funcionalitat mínima per al seu us comú, i es troba sota desenvolupament actiu.

![content](https://dferrg.gitlab.io/ereader/img/prototip.jpg)

## Programari
Actualment el dispositiu presenta un explorador d'arxius des del qual seleccionar documents a obrir. És capaç de recordar el punt on s'ha quedat l'usuari mitjançant una petita [base de dades]() a la carpeta /.config. Pel que fa a la [lectura de documents](group__parser.html), és capaç de [renederitzar text](group__render.html) amb tots els formats necessaris (text pla, cursiva, negreta, títols), a més de llistes multinivell. Es compatible amb tot el set de caràcters de l'estandard ISO-8859-15 (alfabet llatí més símbols d'ús comú a europa).

Per a una descripció més completa del programari, visiteu l'apartat [Mòduls](modules.html).

### Simulador
Per facilitat a l'hora de desenvolupar, s'ha implementat un simulador del dispositiu per a linux, des del qual es pot provar tot el programari del dispositiu.
![content](https://dferrg.gitlab.io/ereader/img/simulador.png)


## Maquinari
Els components principals del dispositiu són els següents:
* [Pantalla ED060SC4](group__ED060SC4.html), disponible a multiples tendes online a preus assequibles.
* Microcontrolador STM32F427: 256kB RAM, 1MB ROM, 168MHz, SDIO, USB...
* [Targeta SD](group__SD.html)
* [PMIC LT3463](group__alimentacio.html), per alimentar la pantalla juntament amb els reguladors LDO TL79L15 i TL78L15.
* Bateria Li-Ion
* Carregador MCP73831
* Reguladors MIC5504 a 3.3V

A més, s'han dissenyat els circuits per a futures ampliacions amb un DAC I2S i un mòdul wireles ESP32 S2. En termes generals, el diagrama del prototip és el següent:

![content](../img/diagrama.png)

## Línies de continuació
Per tal de ser usable en el dia a dia de forma més còmode, encara hi ha una sèrie de qüestions que es poden tractar. Primer, s'hauria d'acabar la renderització d'imatges, per tal de cobrir tots els objectius del projecte. També cal millorar la gestió de l'historial, guardant la posició (byte) d'inici de cada pàgina i variables d'estat, per tal de poder reprendre la lectura i retrocedir pàgines sense haver de llegir tot el document de nou. \\

Després, cal implementar el parser d'HTML, principalment per tal de poder renderitzar taules incrustades a markdown, i com a base per a llegir llibres en format epub més endavant. També és necessari incloure alguna implementació de l'algorisme inflate per descomprimir tant els arxius epub com imatges en format png\cite{Deflate}.\\

S'ha de millorar també el driver USB, permetent la càrrega d'arxius a la SD per mitjà d'aquest, implementant una interfície MSC\cite{MSC}. En la mateixa línia, cal provar el mòdul bluetooth i la comunicació amb el micro principal, i començar a implementar una aplicació android que permeti enviar-li contingut. Una opció interessant que obre això és la lectura de pdf, que es podria renderitzar al telèfon mòbil, enviant el bitmap al lector i rebent comandes des d'aquest per al pas de pàgines. També s'ha de provar el DAC d'audio, i el rendiment de diversos codecs com OGG, FLAC o WAV.
