PREFIX?=arm-none-eabi-
CC=$(PREFIX)g++ 
PP=$(PREFIX)g++
OBJCOPY=$(PREFIX)objcopy
OD=bin

PROJECT=FreeRTOS-template
CFILES=$(wildcard *.c)
CFILES+=$(wildcard extern/picojpeg/picojpeg.c)
CFILES+=$(wildcard extern/FatFs/*.c)
CFILES+=$(wildcard drivers/*.c)
CFILES+=$(wildcard src/*.c)
CFILES+=$(wildcard src/*.cpp)
CFILES+=$(wildcard src/fonts/*.c)
CFILES+=$(wildcard src/render/*.cpp)
CFILES+=$(wildcard src/ui/*.cpp)
CFILES+=$(wildcard src/parser/*.cpp)
LINKER_SCRIPT=ld.stm32.basic
CDEFS=-DPIN_LED1=GPIO5

#include config.mk

SFLAGS=  -nostartfiles -std=gnu++17  -O3 -Werror=implicit-function-declaration
SFLAGS+= -fno-common -ffunction-sections -fdata-sections -fpermissive 
SFLAGS+= -I./extern/libopencm3/include -L./extern/libopencm3/lib
SFLAGS+= -I./extern/ -I./extern/FatFs   -I./drivers
SFLAGS+= -I./extern/FreeRTOS/Source/include -I./extern/FreeRTOS/Source/portable/GCC/ARM_CM4F -I./extern/picojpeg -I./drivers/include  -I./src -I./src/fonts -I./src/render -I./src/parser -I./src/ui
SFLAGS+= -L./extern/FreeRTOS

LFLAGS+=-Wl,--start-group -lc -lgcc -lg -lnosys -Wl,--end-group  -Xlinker -Map=output.map -specs=nosys.specs
    
M4FH_FLAGS= $(SFLAGS) -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16
LFLAGS_STM32=$(LFLAGS) $(CFILES) -T $(LINKER_SCRIPT) #linker and source
STM32F4_CFLAGS=$(M4FH_FLAGS) -DSTM32F4 $(CDEFS) $(LFLAGS_STM32) -lopencm3_stm32f4 -lfreertos#LITTLE_BIT

all: $(OD)/$(PROJECT).bin doc

$(OD)/$(PROJECT).bin: $(OD)/$(PROJECT).elf
	@#printf "  OBJCOPY $(*).bin\n"
	$(OBJCOPY) -Obinary $(OD)/$(PROJECT).elf $(OD)/$(PROJECT).bin

$(OD)/$(PROJECT).elf: $(CFILES) libopencm3/lib/libopencm3_stm32f4.a FreeRTOS/libfreertos.a src/fonts/fonts.h
	@#printf "  OBJCOPY $(*).bin\n"
	mkdir -p $(OD)
	$(CC)  $(STM32F4_CFLAGS) $(CDEFS)  -o $(OD)/$(PROJECT).elf

libopencm3/lib/libopencm3_%.a: extern/libopencm3/Makefile
	$(MAKE) -C extern/libopencm3

libopencm3/Makefile:
	@echo "Initializing libopencm3 submodule"
	git submodule update --init
	
FreeRTOS/libfreertos.a:
	make -C extern/FreeRTOS clean
	make -C extern/FreeRTOS

src/fonts/fonts.h: src/fonts/fonts.xml src/fonts/xmlfont.py src/fonts/ttf2c.py
	make -C src/fonts

clean:
	$(RM) -r $(OD)/$(PROJECT).bin $(OD)/$(PROJECT).elf $(OD)
	make -C extern/FreeRTOS clean
	make -C extern/libopencm3 clean
	
doc:
	doxygen doxygen.conf



#PC BUILD

PC_CDEFS = -DPC_SIM=1
PC_CC=g++
PC_OBJCOPY=objcopy

PROJECT=ook
PC_CFILES=$(wildcard *.c)

PC_CFILES+=$(wildcard extern/picojpeg/picojpeg.c)
PC_CFILES+=$(wildcard drivers/drivers_pc/*.c)
PC_CFILES+=$(wildcard src/*.c)
PC_CFILES+=$(wildcard src/*.cpp)
PC_CFILES+=$(wildcard src/fonts/*.c)
PC_CFILES+=$(wildcard src/render/*.cpp)
PC_CFILES+=$(wildcard src/parser/*.cpp)
PC_CFILES+=$(wildcard src/ui/*.cpp)
    
PC_SFLAGS=  -m32 -std=gnu++17  -O2 -Werror=implicit-function-declaration
PC_SFLAGS+= -fno-common -ffunction-sections -fdata-sections -fpermissive -g
PC_SFLAGS+= -I./extern/ -I./drivers/drivers_pc/include -I./drivers/drivers_pc/finestra
PC_SFLAGS+= -I./extern/FreeRTOS/Source/include -I./extern/FreeRTOS/Source/portable/ThirdParty/GCC/Posix -I./extern/picojpeg -I./src -I./src/fonts -I./src/render -I./src/parser -I./src/ui
PC_SFLAGS+= -L./extern/FreeRTOS
PC_LFLAGS+=-Wl,--start-group -lc -lgcc  -Wl,--end-group  -Xlinker -Map=output.map 



PC_CFLAGS= $(PC_SFLAGS) -DSTM32F4 $(PC_CDEFS) $(PC_LFLAGS) -lfreertosPC -lpthread #LITTLE_BIT
GTK_C=$(wildcard drivers/drivers_pc/finestra/finestra.c)
pc: $(OD)/$(PROJECT) doc

$(OD)/$(PROJECT): $(PC_CFILES) $(OD)/fs.bin 
	make -C src/fonts
	make -C drivers/drivers_pc/finestra
	make -C extern/FreeRTOS -f MakefilePC clean
	make -C extern/FreeRTOS -f MakefilePC
	#make -C drivers/drivers_pc/finestra
	mkdir -p $(OD)
	export PKG_CONFIG_PATH=/usr/lib/i386-linux-gnu/pkgconfig:$PKG_CONFIG_PATH
	$(PC_CC) `pkg-config --cflags gtk+-3.0`   $(PC_CFILES) $(GTK_C) $(PC_CFLAGS) -o $(OD)/$(PROJECT) -L/usr/lib/i386-linux-gnu -I/usr/include/i386-linux-gnu `pkg-config --libs gtk+-3.0`
	mcopy -n -o -i $(OD)/fs.bin drivers/drivers_pc/SD_content/* ::/
	
$(OD)/fs.bin:
	mkdir -p $(OD)
	/sbin/mkdosfs -f 1 -n "EMBEDDED FS" -r 16 -s 64 -S 512 -v -C $(OD)/fs.bin 524256
	


