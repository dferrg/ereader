#include "TPS65186.h"


void PMIC_i2c_init(){

    rcc_periph_clock_enable(RCC_I2C2);
  	rcc_periph_clock_enable(RCC_GPIOB);

	gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO10);
	gpio_set_output_options(GPIOB, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ,
		GPIO10);
	gpio_set_af(GPIOB, GPIO_AF4, GPIO10);

	gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO11);
	gpio_set_output_options(GPIOB, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ,
		GPIO11);
	gpio_set_af(GPIOB, GPIO_AF4, GPIO11);

  	i2c_peripheral_disable(I2C2); /* disable i2c during setup */
  	i2c_reset(I2C2);


	i2c_set_fast_mode(I2C2);
	i2c_set_clock_frequency(I2C2, I2C_CR2_FREQ_42MHZ);
	i2c_set_ccr(I2C2, 35);
	i2c_set_trise(I2C2, 43);

	//i2c_set_7bit_addr_mode(I2C1);
	i2c_peripheral_enable(I2C2);

    i2c_set_own_7bit_slave_address(I2C2, 0x00);
}
void PMIC_GPIO_init(){
  	rcc_periph_clock_enable(RCC_GPIOE);
    /*//PWR_EN
	gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO12);*/

	//VCOM_CTRL
    //-20
	gpio_mode_setup(GPIOE, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO10);
    gpio_clear(GPIOE,GPIO10);

	//WKUP
	//gpio_mode_setup(GPIOE, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO11);

	//PWRUP
    //+22
	gpio_mode_setup(GPIOE, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO9);
  gpio_clear(GPIOE,GPIO9);

  gpio_mode_setup(GPIOE, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO11);

  gpio_clear(GPIOE, GPIO11);
  //gpio_mode_setup(GPIOE, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO8);

}
void PMIC_3v_on(){
    char rv[2]={0x01,0x20};
    i2c_transfer7(I2C2, PMIC_ADDRESS, rv, 2,rv,0); //Write to nonvolatile
}
void PMIC_3v_off(){
    char rv[2]={0x01,0x00};
    i2c_transfer7(I2C2, PMIC_ADDRESS, rv, 2,rv,0); //Write to nonvolatile
}
void PMIC_wakeup(){
    gpio_set(GPIOE,GPIO11);
}
void PMIC_wait_pwrgood(){
  /*for (uint32_t i = 0; i<50000000; i++)
      __asm("nop;");*/
    while(!gpio_get(GPIOE, GPIO8));
}
void PMIC_pwrup(){

    //PMIC_3v_on();
    //PWRUP
    gpio_set(GPIOE, GPIO8);
    gpio_set(GPIOE, GPIO14);

    gpio_set(GPIOE,GPIO10);
    for (int i = 0; i<500000; i++)
        __asm("nop;");
    gpio_set(GPIOE,GPIO9);
    gpio_set(GPIOE, GPIO11);
    for (int i = 0; i<500000; i++)
        __asm("nop;");
    //PMIC_wait_pwrgood();
    /*
    */

}

void PMIC_standby(){
    //PWRUP

        gpio_clear(GPIOE, GPIO11);
        for (int i = 0; i<10000; i++)
            __asm("nop;");

        gpio_clear(GPIOE,GPIO9);
        for (int i = 0; i<10000; i++)
            __asm("nop;");

    gpio_clear(GPIOE,GPIO10);
    gpio_clear(GPIOE, GPIO8);
    gpio_clear(GPIOE, GPIO14);



    //down 3.3
    //PMIC_3v_off();


}

void PMIC_VCOM_HiZ(){
    char rv[2]={PMIC_VCOM2,2};
    char addr=0x07;
    //i2c_transfer7(I2C1, PMIC_ADDRESS, rv, 2,rv,0); //Write to nonvolatile
}
void PMIC_VCOM_Hi(){
    gpio_set(GPIOE,GPIO10);
}
void PMIC_VCOM_Lo(){
        gpio_clear(GPIOE,GPIO10);
}
void PMIC_init(){
    //PMIC_i2c_init();
    PMIC_GPIO_init();

    /*PMIC_wakeup();
    for (int i = 0; i<1000000; i++)
        __asm("nop;");

    char addr_val[2];
    PMIC_VCOM_HiZ();
    //VCOM
    addr_val[0]=PMIC_VCOM1;
    addr_val[1]=PMIC_VCOM;
    //i2c_transfer7(I2C1, PMIC_ADDRESS, addr_val, 2,addr_val,0); //Write VCOM value
    addr_val[0]=PMIC_VCOM2;
    addr_val[1]=PMIC_VCOM_WRITE;
    //i2c_transfer7(I2C1, PMIC_ADDRESS, addr_val, 2,addr_val,1); //Write to nonvolatile

    char rv[2]={0x0A,0xFF};
    //i2c_transfer7(I2C2, PMIC_ADDRESS, rv, 2,rv,0); //Write to nonvolatile

    //PMIC_VCOM_HiZ();
    //cut 3.3*/
}

uint8_t PMIC_test(){
    char addr_val[2];
    addr_val[0]=PMIC_VCOM1;
    addr_val[1]=0;
    //i2c_transfer7(I2C1, PMIC_ADDRESS, addr_val, 1,&addr_val[1],1); //Write to nonvolatile
    return addr_val[1]==EPD_VCOM;
}

uint16_t PMIC_read_int(){
    char rv[2]={0,0};
    char addr=0x07;
    i2c_transfer7(I2C2, PMIC_ADDRESS, &addr, 1,rv,2); //Write to nonvolatile
    uint16_t res=(rv[0]<<8)+rv[1];
    return res;
}
