#include <libopencm3/stm32/gpio.h>
#include "FreeRTOS.h"
#include "semphr.h"
#include "portable.h"

void eink_gpio_init();
void eink_init();
void eink_clear(uint16_t color, uint8_t time);
#define eink_clear_white() eink_clear(ALL_WHITE)
#define eink_clear_black() eink_clear(ALL_BLACK)

#define eink_set(GPN) gpio_set(GPIOA, GPN)
#define eink_lclear(GPN) gpio_clear(GPIOA, GPN)
void eink_clktest();

#define G_GMODE GPIO5
#define G_SPV   GPIO6
#define G_CKV   GPIO0
#define S_CL    GPIO8
#define S_LE    GPIO3
#define S_OE    GPIO2
#define S_SPH   GPIO9
#define S_SHR   GPIO4
#define S_RL    GPIO14

#define LINES_PER_ITERATION 10
#define MAX_SCREEN_BUFF_SIZE (800/2) * LINES_PER_ITERATION //10 lines in 4bpp


#define ALL_WHITE       0x000F //01010101
#define ALL_BLACK       0x00F0 //10101010

#define PIXEL_MASK(x)   (0x0010<<x | 0x0008>>x) //00000011
#define PIXEL_BLACK(x)  (PIXEL_MASK(x)&ALL_BLACK)
#define PIXEL_WHITE(x)  (PIXEL_MASK(x)&ALL_WHITE)

#define BLACK_COND(x,s) (s ? PIXEL_BLACK(x) : 0) //writes black if s is True
#define WHITE_COND(x,s) (s ? PIXEL_WHITE(x) : 0)



void eink_start_frame();
void eink_write_row();
void eink_write_row_BN(uint32_t wt);
void eink_skip_row();
void eink_end_frame();

//source functions
void eink_start_row();
void eink_start_row_BN();
void eink_send_data(uint8_t data[], uint16_t data_start, uint16_t data_len);
void eink_end_row();
void eink_timer_setup();
void eink_init();
void eink_row_convert(uint8_t *data_in, uint8_t *data_out, uint16_t start, uint16_t end, uint8_t bpp);
void eink_screen_send_row(uint8_t data[]);
#define eink_send_data(x,y,z) eink_screen_send_row(x)
void eink_chess();
/**
 *
 *@}
 */
