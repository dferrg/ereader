
/**
 * @addtogroup buttons
 * \ingroup drivers
 * @{
 */
 
///> \brief Inicialitza les interrupcions externes usades pels botons.
void buttons_exti_setup();
void init_buttons();
///> \brief Espera a que es premi algun botó. El botó premut haurà de ser llegit des de l'aplicació.
void wait_button_press();
typedef enum{BTN_OK, BTN_UP, BTN_DWN, BTN_BACK, BTN_IND, BTN_HOME} button_event;

///> \brief Cua amb els botons premuts (button_event). S'escriu des de les interrupcions i es llegeix des de l'aplicació.
extern QueueHandle_t buttonsQueue;
/**
 * @}
 */
