#ifndef _USB_
#define _USB_
#include <libopencm3/usb/usbd.h>
#include <libopencm3/usb/dfu.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/usb/dwc/otg_fs.h>
#include "./usbdfu.h"
#include "./usbserial.h"

/**
 * @addtogroup USB
 * \ingroup drivers
 *@{
 */

/** \brief Inicialitzacions de l'USB, incloent totes les interfícies.
 *
 * Realitza les inicialitzacions necessàries per al perifèric USB i
 * les diferents interfícies. Per ara estan implementades les interfícies
 * DFU (Device Firmware Upgrade) i CDC (Comunication Class Device)
 */
void usb_init();
extern usbd_device *usbd_dev;

/**
 *@}
 */
#endif
