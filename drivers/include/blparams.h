#ifndef _BLPARAMS_
#define _BLPARAMS_

/*//#include <libopencm3/stm32/f4/rtc.h>
#include <libopencm3/stm32/f4/gpio.h>
#include <libopencm3/cm3/scb.h>*/
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rtc.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/stm32/pwr.h>
#include <libopencm3/stm32/rcc.h>
typedef struct{
    bool app_present;
    bool flash_succeded;
} blparams_t;

//extern const volatile blparams_t blparams;

uint32_t write_blparams(uint8_t* params);
void read_blparams(blparams_t* params);

#define BL_BUTTON_RCC RCC_GPIOA
#define BL_BUTTON_GPIO GPIOA
#define BL_BUTTON_NUM  GPIO0

//registre backup d'entrada al bootloader
#define ENTER_BL            RTC_BKPXR(0)
#define ENTER_BL_TRUE       0x85a0acab
#define ENTER_BL_FALSE      0x1312f68a

//ignora o no el signat de la imatge
#define KEY_IGNORE          RTC_BKPXR(1)
#define KEY_IGNORE_TRUE     0x2088c89d
#define KEY_IGNORE_FALSE    0x784b432c

//comprova que l'applicacio carrega correctament
#define APP_CHECK           RTC_BKPXR(3)
#define APP_CHECK_OK        0xb42f49c5
#define APP_CHECK_KO        0xe23e4321
#define APP_CHECK_PL        APP_CHECK_KO

/**
 * @addtogroup BOOTLOADER
 * \ingroup drivers
 * L'unic propòsit d'aquest mòdul és la capacitat de saltar al bootloader des de l'aplicació. Això permet programar el dispositiu sense necessitat d'obrir-lo, mitjançant l'eina dfu-util.
 *@{
 */
//condicions d'entrada al bootloader
/** \brief Reinicia el dispositiu en mode bootloader. Cridada per DFU.
 */
void reset_bl();
/**
 *
 *@}
 */
#endif
