#include <libopencm3/stm32/rcc.h>

//#ifndef FSE_FREQ
#define HSE_FREQ 25
//#endif

#define PLL_PRE_DIV HSE_FREQ //Configura entrada del PLL a 1MHz
#define PLL_COMMON_MUL 336 //frequencia interna PLL: 2*168MHz
#define PLL_CLOCK_DIV 2 //clock: 168MHz (max)
#define PLL_USB_DIV 7 //48MHz

void init_rcc();
void sleep();
void power_off();
