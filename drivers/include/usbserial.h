#ifndef _USBSERIAL_
#define _USBSERIAL_
#include <libopencm3/usb/usbd.h>
#include <libopencm3/usb/dfu.h>
#include "usb.h"

extern const struct usb_interface_descriptor comm_iface[];
extern const struct usb_interface_descriptor data_iface[];
/**
 * @addtogroup USB
 * \ingroup drivers
 *@{
 */
 /** \brief Gestiona les peticions relatives a CDC.
  */
enum usbd_request_return_codes cdcacm_control_request(usbd_device *usbd_dev,
	struct usb_setup_data *req, uint8_t **buf, uint16_t *len,
	void (**complete)(usbd_device *usbd_dev, struct usb_setup_data *req));

/** \brief Configuració inicial de la classe CDC.
 * Només s'hauria de cridar des del propi driver, arrel d'una interrupció.
 */
void cdcacm_set_config(usbd_device *usbd_dev, uint16_t wValue);

/** \brief Envia una cadena de caràcters per USB.
 *
 * @param a Cadena de caràcters finalitzada en '\0'.
 */
void print(char* a);

char* itoa(uint32_t num, char* str, int base);
/**
 *@}
 */

#endif
