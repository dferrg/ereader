#include <libopencm3/stm32/i2c.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>


#define PMIC_ADDRESS 0x48


//VCOM
#define PMIC_VCOM1  0x03
#define EPD_VCOM    141 //1.41v
#define PMIC_VCOM   EPD_VCOM

#define PMIC_VCOM2  0x04
#define PMIC_VCOM_WRITE 0x40


//power up
#define PMIC_UPSEQ0 0x09


//power down
#define PMIC_DWNSEQ0		0x0B
#define PMIC_DWNSEQ0_VAL	0x00
#define PMIC_DWNSEQ1		0x0C
#define PMIC_DWNSEQ1_VAL	0x00

//thermistor
#define PMIC_TMST1	0x0D
#define PMIC_TMST1_READ 0x83
#define PMIC_TMST2 	0x0E
#define PMIC_TMST2_VAL 	0x78 //0 a 50 graus


/**
 * @addtogroup alimentacio Alimentació
 * \ingroup drivers
 *@{
 */

/* \brief Activa les fonts d'alimentació del PMIC. */
void PMIC_pwrup(); 

/* \brief Desactiva les fonts d'alimentació. */
void PMIC_standby(); 



void PMIC_init(); ///< Inicialització del PMIC.

/**
 *
 *@}
 */

/* \brief Habilita el PMIC, sense activar les alimentacions de la pantalla. */
void PMIC_wakeup(); 
void PMIC_3v_on();
void PMIC_3v_off();
uint16_t PMIC_read_int(); ///< Llegeig el motiu d'interrupció del PMIC. Útil per tractar errors.
