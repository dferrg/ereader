#include "FreeRTOS.h"
#include "semphr.h"
#include "portable.h"
#define LINES_PER_ITERATION 10
#define MAX_SCREEN_BUFF_SIZE (800/2) * LINES_PER_ITERATION //10 lines in 4bpp

#define ALL_BLACK       0x0055 //01010101
#define ALL_WHITE       0x00AA //10101010

#define PIXEL_MASK(x)   (0x0003<<((3-x)*2)) //00000011
#define PIXEL_BLACK(x)  (PIXEL_MASK(x)&ALL_BLACK)
#define PIXEL_WHITE(x)  (PIXEL_MASK(x)&ALL_WHITE)

#define BLACK_COND(x,s) (s ? PIXEL_BLACK(x) : 0) //writes black if s is True
#define WHITE_COND(x,s) (s ? PIXEL_WHITE(x) : 0)
/**
 * \defgroup drivers
 * En aquesta secció s'explica el funcionament dels drivers de cada un dels dispositius. 
 * 
 * @addtogroup ED060SC4
 * \ingroup drivers
 * @{
 * El mòdul ED060SC4 s'encarrega del control de la pantalla electroforètica.
 * Es divideix en dos blocs: el control de baix nivell i un driver amb control de reentrada.
 * Aquí només es tracta el driver d'alt nivell ja que és el que ofereix una interfície apropiada
 * per usar-se amb un RTOS.
 * ### Driver amb reentrada
 * Degut a la limitació de memòria del dispositiu, els frames s'han d'enviar a la pantalla
 * per parts. Aquest driver ofereix una interfície simple per fer-ho:
 * * Es crida a una funció d'inici d'escriptura s_eink_start_write()
 * * Es crida s_eink_write_buffer() amb noves dades fins que acabem el frame.
 * La funció s'encarrega de fer les esperes necessàries.
 *
 * Internament s'utilitza una tasca (thread) independent que espera comandes des de la part pública del driver.
 * S'usa una cua per a les comandes i una altra per al buffer de dades.
 */

typedef struct writeArea_t{
    uint16_t x;
    uint16_t y;
    uint16_t width;
    uint16_t height;
} writeArea_t;

//Thread-safe functions
/** \brief Inicialitzacions de la pantalla, incloent cues FreeRTOS.
 *
 * Realitza les inicialitzacions necessàries per a la pantalla, incloent
 * la inicialització de perifèrics de baix nivell (GPIO,DMA) i les cues
 * necessàries per al multitasking.
 */
uint32_t screen_init();

/** \brief Tasca dedicada al control de la pantalla.
 *
 * Roman en espera fins que rep una comanda. Quan s'ha d'escriure,
 * s'ha de fer a través de les comandes s_eink_clear(), s_eink_start_write()
 * i s_eink_write_buffer().
 */
void screen_task(void *args __attribute__((unused)));

/** \brief Esborra un àrea de la pantalla.
 *
 * @param area Descriu l'area en píxels a escriure en pantalla. Per ara, ha d'estar aliniat a 4 píxels.
 * @param color ALL_BLACK o ALL_WHITE
 *
 * @return pdFalse si hi ha algun error.
 */
BaseType_t s_eink_clear(writeArea_t area, uint32_t color);
BaseType_t s_eink_clear_cycle(writeArea_t area, uint32_t color);
/** \brief Inicia una escriptura de pantalla.
 *
 * Nota: Aquesta funció no escriu res per si sola. Veure s_eink_write_buffer();
 *
 * @param area Descriu l'area en píxels a escriure en pantalla. Per ara, ha d'estar aliniat a 4 píxels.
 * @param bitDepth Buff_1bpp, Buff_2bpp, Buff_4bpp
 *
 * @return Punter al buffer on escriure els píxels. Escriure 10 files per iteració.
 */
uint8_t* s_eink_start_write(writeArea_t area, uint8_t bitDepth);
/** \brief Escriu 10 línies a la pantalla.
 *
 * Escriu 10 línies (o, si no en queden, menys de 10) des del buffer proporcionat pel driver prèviament.
 * Nota: cal cridar s_eink_start_write() abans.
 *
 * @param area Descriu l'area en píxels a escriure en pantalla. Per ara, ha d'estar aliniat a 4 píxels.
 * @param bitDepth Buff_1bpp, Buff_2bpp, Buff_4bpp
 *
 * @return Punter al buffer on escriure els següents píxels. Escriure 10 files per iteració.
 */
uint8_t* s_eink_write_buffer();

/**
 * @addtogroup ED060SC4
 * \ingroup drivers
 */
 #define epdsys_init() screen_init()
//void epdsys_init();
