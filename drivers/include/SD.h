#include <libopencm3/stm32/sdio.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#define SD_BLOCK_LEN 512

#define SDIO_4bit_bus   0x00000800
#define SDIO_PWRSAVE    0x00000200
#define SDIO_CLKDIV_MASK 0x000000FF



extern uint8_t SDHC;
#define ACMD41_HCS           0x40000000
#define ACMD41_SDXC_POWER    0x10000000
#define ACMD41_S18R          0x04000000
#define ACMD41_VOLTAGE       0x00ff8000
#define ACMD41_ARG_HC        (ACMD41_HCS|ACMD41_SDXC_POWER|ACMD41_VOLTAGE)
#define ACMD41_ARG_SC        (ACMD41_VOLTAGE)

#define sdio_response    SDIO_RESP1
#define sdio_check_response(x) SDIO_RESP1 & (uint32_t)0xFDFFE008
/**
 * @addtogroup SD
 * \ingroup drivers
 * \brief control i accés a la SD.
 *@{
 */

void sdio_init();   ///<  Inicialitza la interfície SDIO.
int SD_init();      ///<  Inicialitza la targeta SD.
void sdio_dma_init(); ///<  Inicialitza el DMA per a SDIO.

/** \brief Envia comandes a la SD.
 *
 * Envia comandes mitjançant la interfície SDIO.
 *
 * @param cmd Codi de la comanda.
 * @param arg Argument de la comanda.
 * @param wait_resp Temps que s'ha d'esperar a la resposta.
 */
uint32_t sdio_cmd_send(uint8_t cmd, uint32_t arg, uint8_t wait_resp);

/** \brief Llegeig un bloc de la SD.
 *
 * @param data Array on s'escriurà el sector.
 * @param address Direcció del sector. En base a byte per a SDSC i en base a sector per a SDHC.
 */
void sdio_read_block(uint8_t data[], uint32_t address);
/** \brief Escriu un bloc a la SD.
 *
 * @param data Array amb les dades a escriure.
 * @param address Direcció del sector. En base a byte per a SDSC i en base a sector per a SDHC.
 */
void sdio_write_block(uint8_t data[], uint32_t address);

/** \brief Escriu varis blocs de la SD.
 *
 * @param data Array amb les dades a escriure.
 * @param address Direcció del primer sector. En base a byte per a SDSC i en base a sector per a SDHC.
 */
void sdio_write_blocks(uint8_t data[], uint32_t address, uint32_t n_blocks);

/** \brief Llegeix varis blocs a la SD.
 *
 * @param data Array on s'escriuran els sectors.
 * @param address Direcció del primer sector. En base a byte per a SDSC i en base a sector per a SDHC.
 */
void sdio_read_blocks(uint8_t data[], uint32_t address, uint32_t n_blocks);

/**
 *
 *@}
 */

void uart_cmd(int n, uint32_t resp);
uint32_t sdio_get_sector_count();
uint32_t sdio_get_block_size();
void sdio_read_csd(uint32_t buff[]);

void sdio_read_cid(uint32_t buff[]);
#define sdio_get_ocr() sdio_cmd_send(58, 0, SDIO_CMD_WAITRESP_LONG)
