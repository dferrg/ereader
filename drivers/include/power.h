#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/adc.h>
#include <libopencm3/stm32/rcc.h>


/**
 * @addtogroup alimentacio
 * \ingroup drivers
 * \brief Gestio de les alimentacions.
 *@{
 */

uint32_t power_get_battery_voltage(); ///< Obté el nivell actual de bateria en milivolts.
uint32_t power_get_battery_percentage(); ///< Obté el percentatge de bateria.

void power_off(); ///< Apaga el dispositiu.
void power_init(); ///< Inicialitza els perifèrics usats pel mòdul d'alimentació.

extern void power_battery_charged_cb(); ///< Funció a cridar quan la bateria estigui completament carregda.
/**
 *
 *@}
 */
