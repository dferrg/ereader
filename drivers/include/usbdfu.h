#include <libopencm3/usb/usbd.h>
#include <libopencm3/usb/dfu.h>

/**
 * @addtogroup USB
 * \ingroup drivers
 *@{
 */

extern const struct usb_interface_descriptor dfu_iface[];

/** \brief Gestiona les peticions relatives a DFU.
 */
enum usbd_request_return_codes usbdfu_control_request(usbd_device *usbd_dev, struct usb_setup_data *req, uint8_t **buf,
		uint16_t *len, void (**complete)(usbd_device *usbd_dev, struct usb_setup_data *req));
/**
 *@}
 */
