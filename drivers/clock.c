#include "clock.h"
#include "power.h"
#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/f4/nvic.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/stm32/gpio.h>
const struct rcc_clock_scale rcc_hse_20mhz_3v3 = { /* 168MHz */
	pllm : 20,
	plln : 336,
	pllp : 2,
	pllq : 7,
	pllr : 0,
	pll_source : RCC_CFGR_PLLSRC_HSE_CLK,
	flash_config : FLASH_ACR_DCEN | FLASH_ACR_ICEN |
			FLASH_ACR_LATENCY_5WS,
	hpre : RCC_CFGR_HPRE_DIV_NONE,
	ppre1 : RCC_CFGR_PPRE_DIV_4,
	ppre2 : RCC_CFGR_PPRE_DIV_2,
	voltage_scale : PWR_SCALE1,
	ahb_frequency  : 168000000,
	apb1_frequency : 42000000,
	apb2_frequency : 84000000
};
void pwr_exti_setup();
void init_rcc(){

	rcc_clock_setup_pll(&rcc_hse_20mhz_3v3);
    //rcc_clock_setup_pll(&rcc_conf168);
    pwr_exti_setup();
}

void sleep(){
	__asm volatile( "dsb" );
	__asm volatile( "wfi" );
	__asm volatile( "isb" );
}

#define RELEASED 0
#define WAITING 1

//uint16_t clock_pwr_btn = WAITING;


void pwr_exti_setup(){
	rcc_periph_clock_enable(RCC_GPIOC);
    gpio_mode_setup(GPIOC, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO7);

    rcc_periph_clock_enable(RCC_SYSCFG);
    exti_select_source(EXTI7, GPIOC);

	exti_set_trigger(EXTI7, EXTI_TRIGGER_FALLING);
	exti_enable_request(EXTI7);
	nvic_enable_irq(NVIC_EXTI9_5_IRQ);

}

/*
void exti9_5_isr(void){
		exti_reset_request(EXTI7);
        if(clock_pwr_btn==WAITING) clock_pwr_btn=RELEASED;
		else {

		    sys_deinit();
		    power_off();
		}
}*/
