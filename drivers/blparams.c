#include "blparams.h"

#define BL_ADDRESS 0x08004000
void rtc_init(){
    rcc_periph_clock_enable(RCC_PWR);
    pwr_disable_backup_domain_write_protect();
    rcc_periph_clock_enable(RCC_BKPSRAM);
    rtc_unlock();
}
void rtc_deinit(){
    rtc_lock();
rcc_periph_clock_disable(RCC_BKPSRAM);
pwr_enable_backup_domain_write_protect();
    rcc_periph_clock_disable(RCC_PWR);


}
void reset(){
    __asm__ volatile("dsb":::"memory");
    SCB_AIRCR = (0x5FA << 16) | SCB_AIRCR_SYSRESETREQ; // Cause a RESET
    __asm__ volatile("dsb":::"memory");
    while(1);
}
void reset_bl(){

    void (*app)();
    app = (void (*)(void)) (*((uint32_t *)(BL_ADDRESS + 4)));

    rtc_init();
    ENTER_BL = ENTER_BL_TRUE;
    rtc_deinit();

    reset();

}
