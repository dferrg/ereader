#include "ED060SC4.h"
#include "TPS65186.h"
#include "epdsys.h"
#include "usbserial.h"

QueueHandle_t cmdQueue;         //la tasca de la pantalla espera comandes aqui
QueueHandle_t dataQueue;
SemaphoreHandle_t accessSmphr;
uint8_t screenBuffer[2][MAX_SCREEN_BUFF_SIZE];

volatile uint8_t currentBuffer=0;
void screen_task(void *args __attribute__((unused)));
void vQueueFlush( xQueueHandle pxQueue );
enum bpp_t{Buff_1bpp=1, Buff_2bpp=2, Buff_4bpp=4};
enum operation_t{None, Clear, ClearCycle, Write};
typedef struct screenCmd{
    writeArea_t area;
    enum operation_t operation;
    uint32_t color;
    enum bpp_t bpp;
    SemaphoreHandle_t wait;
} screenCmd;


uint32_t epdsys_init(){
    cmdQueue =  xQueueCreate(2, sizeof(screenCmd));
    dataQueue =   xQueueCreate( 1, sizeof(uint8_t*));
    accessSmphr = xSemaphoreCreateMutex();
    xTaskCreate(screen_task, "epdsys", 500, NULL,3, NULL);
    eink_init();
    return 0;
}

void t_screen_clear(screenCmd cmd){
    PMIC_pwrup();
    taskENTER_CRITICAL();     // print("driver\r\n");

    eink_clear(cmd.color,200);
    taskEXIT_CRITICAL();
}

void t_screen_clear_cycle(screenCmd cmd){
    PMIC_pwrup();
    eink_clear( cmd.color,100);
    vTaskDelay(30);
    eink_clear(~cmd.color,100);
    vTaskDelay(30);
    eink_clear(~cmd.color,100);
    vTaskDelay(60);
    eink_clear(cmd.color,150);
    vTaskDelay(30);
    eink_clear(cmd.color,150);
}

void t_screen_write(screenCmd cmd){

    //xSemaphoreGive(cmd.wait);//task is being processed

    int written=0;
    uint8_t* buffer;
    uint8_t dl[200]; //dades a enviar
    PMIC_pwrup();

    eink_start_frame();

    while(written<cmd.area.width){
      BaseType_t qresp;
      //Espera a rebre una comanda
      do{

          qresp = xQueueReceive(dataQueue, &buffer, 1);
      }while(qresp==pdFALSE);
      for(int i=0; i<LINES_PER_ITERATION; i++){
        eink_row_convert(&buffer[(LINES_PER_ITERATION-i-1)*100*cmd.bpp], dl, 0, 800, 2);
//          for (int j=0; j<200; j++) dl[j]=ALL_WHITE;
        eink_start_row_BN();
        eink_send_data(dl, 0, 800);
        eink_write_row_BN(100);
      }
      written+=LINES_PER_ITERATION;
    }


}

void screen_task(void *args __attribute__((unused))) {
    while(1){
        BaseType_t qresp;
        screenCmd cmd;
        //Espera a rebre una comanda
        do{
            cmd.operation=None;
            qresp = xQueueReceive(cmdQueue, &cmd, 1000);
            //eink_end_frame();
print("driver\r\n");
}while(cmd.operation==None);
        //
        print("driverOUT\r\n");

       switch(cmd.operation){
            case Clear:
                t_screen_clear(cmd);
                break;
            case ClearCycle:
                t_screen_clear_cycle(cmd);
                break;
            case Write:
                t_screen_write(cmd);
                break;

        }

        if(xQueuePeek(cmdQueue, &cmd, 1)==pdFALSE){
           //vTaskDelay(75);
           //PMIC_standby();
         }

    }
}
BaseType_t s_eink_clear(writeArea_t area, uint32_t color){ //color pot ser ALL_BLACK o ALL_WHITE
    screenCmd cmd;
    cmd.area=area;
    cmd.operation=Clear;
    cmd.color=color;
    cmd.wait=NULL;

    return xQueueSend(cmdQueue, &cmd, 2000);
}
BaseType_t s_eink_clear_cycle(writeArea_t area, uint32_t color){ //color pot ser ALL_BLACK o ALL_WHITE
    screenCmd cmd;
    cmd.area=area;
    cmd.operation=ClearCycle;
    cmd.color=color;
    cmd.wait=NULL;

    return xQueueSend(cmdQueue, &cmd, 2000);
}
uint8_t* s_eink_start_write(writeArea_t area, uint8_t bitDepth){ //Retorna una estructura amb el buffer on escriure. 1bpp, 2bpp o 4bpp
    screenCmd cmd;
    cmd.area=area;
    cmd.operation=Write;
    cmd.color=ALL_BLACK;
    cmd.bpp=bitDepth;
    cmd.wait=xSemaphoreCreateBinary();
    //vQueueFlush(dataQueue);


    if (xQueueSend(cmdQueue, &cmd, 10000) == pdFALSE)
        return NULL;

    //xSemaphoreTake(cmd.wait, portMAX_DELAY);//espera a que la comanda sigui processada

    currentBuffer = 0;
    return &screenBuffer[currentBuffer][0];
}

uint8_t* s_eink_write_buffer(){// modifica el handle i retorna el nombre de línies restants.
    uint8_t* addr=&screenBuffer[currentBuffer][0];
    xQueueSend(dataQueue, &addr, 2000); //Espera que estigui disponible i envia el buffer.
                                                              //L'espera assegura que no es comença a modificar l'altra meitat del buffer abans que el driver l'utilitzi
    currentBuffer^=1; //Alterna entre les dos meitats del buffer
    return &screenBuffer[currentBuffer][0];

}



void vQueueFlush( xQueueHandle pxQueue )
{
  BaseType_t qresp;
  screenCmd cmd;

  do{
       qresp = xQueueReceive(cmdQueue, &cmd, 1);
  }while(qresp==pdTRUE);
}
