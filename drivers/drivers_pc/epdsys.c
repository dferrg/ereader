#include "epdsys.h"
#include "finestra.h"
#include <stdlib.h>
QueueHandle_t cmdQueue;         //la tasca de la pantalla espera comandes aqui
QueueHandle_t dataQueue;
SemaphoreHandle_t accessSmphr;
uint8_t screenBuffer[2][MAX_SCREEN_BUFF_SIZE];

volatile uint8_t currentBuffer=0;
void screen_task(void *args __attribute__((unused)));
void vQueueFlush( xQueueHandle pxQueue );
enum bpp_t{Buff_1bpp=1, Buff_2bpp=2, Buff_4bpp=4};
enum operation_t{Clear, Write};
typedef struct screenCmd{
    writeArea_t area;
    enum operation_t operation;
    uint32_t color;
    enum bpp_t bpp;
    SemaphoreHandle_t wait;
} screenCmd;

void screen_init(){
  eink_init();
    cmdQueue =  xQueueCreate(10, sizeof(screenCmd));
    dataQueue =   xQueueCreate( 1, sizeof(uint8_t*));
    accessSmphr = xSemaphoreCreateMutex();
    xTaskCreate(gtk_task, "screen", 1000, NULL,1, NULL);
    xTaskCreate(screen_task, "epdsys", 500, NULL,3, NULL);

}

void t_screen_clear(screenCmd cmd){
    eink_clear(cmd.color);
}

void t_screen_write(screenCmd cmd){

    vQueueFlush(dataQueue);
    xSemaphoreGive(cmd.wait);//task is being processed

    int written=0;
    uint8_t* buffer;
    uint8_t dl[200]; //dades a enviar

    eink_start_frame();

    while(written<cmd.area.width){
      BaseType_t qresp;
      //Espera a rebre una comanda
      do{
          qresp = xQueueReceive(dataQueue, &buffer, 1000);
      }while(qresp==pdFALSE);
      for(int i=9; i>=0; i--){
        eink_row_convert(&buffer[i*100*cmd.bpp], dl, 0, 800, 2);
//          for (int j=0; j<200; j++) dl[j]=ALL_WHITE;
        eink_start_row_BN();
        eink_send_data(dl, 0, 800);
        eink_write_row_BN(100);
            //eink_end_frame();
      }
      written+=10;
    }

    eink_end_frame();
}
void screen_task(void *args __attribute__((unused))) {
    while(1){
      vTaskDelay(100);
        BaseType_t qresp;
        screenCmd cmd;
        //Espera a rebre una comanda
        do{
            qresp = xQueueReceive(cmdQueue, &cmd, 1000);

        }while(qresp==pdFALSE);
        //

       switch(cmd.operation){
            case Clear:
                t_screen_clear(cmd);
                break;
            case Write:
                t_screen_write(cmd);
                break;
        }

    }
}
BaseType_t s_eink_clear(writeArea_t area, uint32_t color){ //color pot ser ALL_BLACK o ALL_WHITE
    screenCmd cmd;
    cmd.area=area;
    cmd.operation=Clear;
    cmd.color=color;
    cmd.wait=NULL;

    return xQueueSend(cmdQueue, &cmd, 1000);
}
BaseType_t s_eink_clear_cycle(writeArea_t area, uint32_t color){ //color pot ser ALL_BLACK o ALL_WHITE
    screenCmd cmd;
    cmd.area=area;
    cmd.operation=Clear;
    cmd.color=color;
    cmd.wait=NULL;

    return xQueueSend(cmdQueue, &cmd, 1000);
}
uint8_t* s_eink_start_write(writeArea_t area, uint8_t bitDepth){ //Retorna una estructura amb el buffer on escriure. 1bpp, 2bpp o 4bpp
    screenCmd cmd;
    cmd.area=area;
    cmd.operation=Write;
    cmd.color=ALL_BLACK;
    cmd.bpp=bitDepth;
    cmd.wait=xSemaphoreCreateBinary();

    if (xQueueSend(cmdQueue, &cmd, 1000) == pdFALSE)
        return NULL;

    xSemaphoreTake(cmd.wait, portMAX_DELAY);//espera a que la comanda sigui processada

    currentBuffer = 0;
    return &screenBuffer[currentBuffer][0];
}
uint8_t* s_eink_write_buffer(){// modifica el handle i retorna el nombre de línies restants.
    uint8_t* addr=&screenBuffer[currentBuffer][0];
    xQueueSend(dataQueue, &addr, 1000); //Espera que estigui disponible i envia el buffer.
                                                              //L'espera assegura que no es comença a modificar l'altra meitat del buffer abans que el driver l'utilitzi
    currentBuffer^=1; //Alterna entre les dos meitats del buffer
    return &screenBuffer[currentBuffer][0];

}



void vQueueFlush( xQueueHandle pxQueue )
{
  BaseType_t qresp;
  screenCmd cmd;

  do{
       qresp = xQueueReceive(cmdQueue, &cmd, 1);
  }while(qresp==pdTRUE);
}
