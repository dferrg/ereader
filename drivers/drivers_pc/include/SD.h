
#include <stdint.h>
extern uint8_t SDHC;
void sdio_init();
int SD_init();
void sdio_dma_init();
uint32_t sdio_cmd_send(uint8_t cmd, uint32_t arg, uint8_t wait_resp);
void sdio_read_block(uint8_t data[], uint32_t address);
void sdio_write_block(uint8_t data[], uint32_t address);
void sdio_write_blocks(uint8_t data[], uint32_t address, uint32_t n_blocks);
void sdio_read_blocks(uint8_t data[], uint32_t address, uint32_t n_blocks);
void uart_cmd(int n, uint32_t resp);

uint32_t sdio_get_sector_count();
uint32_t sdio_get_block_size();
void sdio_read_csd(uint32_t buff[]);

void sdio_read_cid(uint32_t buff[]);
#define sdio_get_ocr() 0
