#include "FreeRTOS.h"
#include "semphr.h"
#include "portable.h"
#define LINES_PER_ITERATION 10
#define MAX_SCREEN_BUFF_SIZE (800/2) * LINES_PER_ITERATION //10 lines in 4bpp

#define ALL_BLACK       0x0055 //01010101
#define ALL_WHITE       0x00AA //10101010

#define PIXEL_MASK(x)   (0x0003<<((3-x)*2)) //00000011
#define PIXEL_BLACK(x)  (PIXEL_MASK(x)&ALL_BLACK)
#define PIXEL_WHITE(x)  (PIXEL_MASK(x)&ALL_WHITE)

#define BLACK_COND(x,s) (s ? PIXEL_BLACK(x) : 0) //writes black if s is True
#define WHITE_COND(x,s) (s ? PIXEL_WHITE(x) : 0)
typedef struct writeArea_t{
    uint16_t x;
    uint16_t y;
    uint16_t width;
    uint16_t height;
} writeArea_t;

void screen_init();
BaseType_t s_eink_clear(writeArea_t area, uint32_t color);
BaseType_t s_eink_clear_cycle(writeArea_t area, uint32_t color);

uint8_t* s_eink_start_write(writeArea_t area, uint8_t bitDepth); //Retorna una estructura amb el buffer on escriure. 1bpp, 2bpp o 4bpp
uint8_t* s_eink_write_buffer();// modifica el handle i retorna el nombre de línies restants.
