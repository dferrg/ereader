#include <stdio.h>
#include <stdint.h>
#include <dirent.h>
#include <sys/types.h>
typedef int FIL;
typedef int FRESULT;
typedef int8_t TCHAR;
typedef int32_t TINT;
typedef uint32_t UINT;
typedef uint8_t BYTE;
typedef size_t FSIZE_t;

typedef DIR *DIR_t;

#ifndef __FF_STRUCTS__
#define __FF_STRUCTS__
//typedef struct {
//    size_t fsize;               /* File size */
//    uint16_t    fdate;               /* Last modified date */
//    uint16_t    ftime;               /* Last modified time */
//    uint8_t    fattrib;             /* Attribute */
//#if FF_USE_LFN
//    uint8_t   altname[FF_SFN_BUF + 1]; /* Alternative object name */
//    uint8_t   fname[FF_LFN_BUF + 1];   /* Primary object name */
//#else
//    uint8_t   fname[12 + 1];       /* Object name */
//#endif
//} FILINFO;
#define AM_DIR DT_DIR

typedef size_t FSIZE_T;

typedef struct {
   ino_t          d_ino;       /* Inode number */
   off_t          d_off;       /* Not an offset; see below */
   unsigned short d_reclen;    /* Length of this record */
   unsigned char  fattrib;      /* Type of file; not supported
                                  by all filesystem types */
   char           fname[256]; /* Null-terminated filename */
} FILINFO; 
#endif
#define FA_READ 0x01
#define FA_WRITE 0x02
#define FA_OPEN_EXISTING 0x04
#define FA_CREATE_NEW 0x08
#define FA_CREATE_ALWAYS 0x10
#define FA_OPEN_ALWAYS 0x20
#define FA_OPEN_APPEND 0x40


FRESULT f_open (
  FIL* fp,           /* [OUT] Pointer to the file object structure */
  const TCHAR* path, /* [IN] File name */
  BYTE mode          /* [IN] Mode flags */
);

FRESULT f_close (
  FIL* fp     /* [IN] Pointer to the file object */
);

FRESULT f_read (
  FIL* fp,     /* [IN] File object */
  void* buff,  /* [OUT] Buffer to store read data */
  UINT btr,    /* [IN] Number of bytes to read */
  UINT* br     /* [OUT] Number of bytes read */
);

FRESULT f_write (
  FIL* fp,          /* [IN] Pointer to the file object structure */
  const void* buff, /* [IN] Pointer to the data to be written */
  UINT btw,         /* [IN] Number of bytes to write */
  UINT* bw          /* [OUT] Pointer to the variable to return number of bytes written */
);

FRESULT f_lseek (
  FIL*    fp,  /* [IN] File object */
  FSIZE_t ofs  /* [IN] Offset of file read/write pointer to be set */
);

FSIZE_t f_tell (
  FIL* fp   /* [IN] File object */
);
#define f_rewind(fp) f_lseek((fp), 0)


FRESULT f_opendir (
  DIR** dp,           /* [OUT] Pointer to the directory object structure */
  const TCHAR* path  /* [IN] Directory name */
);

FRESULT f_readdir (
  DIR** dp,      /* [IN] Directory object */
  FILINFO* fno  /* [OUT] File information structure */
);

FRESULT f_rewinddir (
  DIR** dp      /* [IN] Directory object */
);

FSIZE_T f_size( FIL* fp );

FRESULT f_mkdir (
  const TCHAR* path /* [IN] Directory name */
);

FRESULT f_stat (
  const TCHAR* path,  /* [IN] Object name */
  FILINFO* fno        /* [OUT] FILINFO structure */
);



