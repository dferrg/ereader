#include "ff.h"
#include <iostream>
#include <sys/stat.h>
#include <stdlib.h>
    #include <stdio.h>
FRESULT f_open (
  FIL* fp,           /* [OUT] Pointer to the file object structure */
  const TCHAR* path, /* [IN] File name */
  BYTE mode          /* [IN] Mode flags */
){
    switch(mode){
        case FA_READ:
            *fp=fopen(path, "r");
            break;
        case FA_READ | FA_WRITE:
            *fp=fopen(path, "r+");
            break;
        case FA_WRITE:
        case FA_CREATE_ALWAYS:
        case FA_CREATE_ALWAYS | FA_WRITE:
            *fp=fopen(path, "w");
            break;
        case FA_CREATE_ALWAYS | FA_WRITE | FA_READ:
            *fp=fopen(path, "w+");
            break;
        case FA_OPEN_APPEND | FA_WRITE:
            *fp=fopen(path, "a");
            break;
        case FA_OPEN_APPEND | FA_WRITE | FA_READ:
            *fp=fopen(path, "a+");
            break;
        case FA_CREATE_NEW | FA_WRITE:
            *fp=fopen(path, "wx");
            break;
        case FA_CREATE_NEW | FA_WRITE | FA_READ:
            *fp=fopen(path, "w+x");
            break;

        default:
            *fp=0;
    }
    return !(*fp);
}

FRESULT f_close (
  FIL* fp     /* [IN] Pointer to the file object */
){
    return fclose(*fp);

}

FRESULT f_read (
  FIL* fp,     /* [IN] File object */
  void* buff,  /* [OUT] Buffer to store read data */
  UINT btr,    /* [IN] Number of bytes to read */
  UINT* br     /* [OUT] Number of bytes read */
){
    *br=fread(buff, 1, btr, *fp);
    return !(*br);
}

FRESULT f_write (
  FIL* fp,          /* [IN] Pointer to the file object structure */
  const void* buff, /* [IN] Pointer to the data to be written */
  UINT btw,         /* [IN] Number of bytes to write */
  UINT* bw          /* [OUT] Pointer to the variable to return number of bytes written */
){
    *bw=fwrite(buff, 1, btw, *fp);
    return !(*bw);
}

FRESULT f_lseek (
  FIL*    fp,  /* [IN] File object */
  FSIZE_t ofs  /* [IN] Offset of file read/write pointer to be set */
){
    return fseek(*fp, ofs, SEEK_SET);
}

FSIZE_t f_tell (
  FIL* fp   /* [IN] File object */
){
    return ftell(*fp);
}
FRESULT f_opendir (
  DIR** dp,           /* [OUT] Pointer to the directory object structure */
  const TCHAR* path  /* [IN] Directory name */
){
    if(path[0])
        *dp=opendir(path);
    else
        *dp=opendir("/");
    if(*dp==NULL) exit(0);//return 1;
    return 0;
}
FRESULT f_readdir (
  DIR** dp,      /* [IN] Directory object */
  FILINFO* fno  /* [OUT] File information structure */
){
    dirent*de=readdir(*dp);
    if(de){
        *((dirent*)fno) = *de;
        return 0;
    }else{
        return 1;
    }
    
}

FRESULT f_rewinddir (
  DIR** dp      /* [IN] Directory object */
){
    rewinddir(*dp);
    return 0;
}

FSIZE_T f_size( FIL* fp ){
    int prev=ftell(*fp);
    fseek(*fp, 0L, SEEK_END);
    int sz=ftell(*fp);
    fseek(*fp,prev,SEEK_SET); //go back to where we were
    return sz;
}

FRESULT f_mkdir (
  const TCHAR* path /* [IN] Directory name */
){
    return mkdir(path, 0700);
}   

FRESULT f_stat (
  const TCHAR* path,  /* [IN] Object name */
  FILINFO* fno        /* [OUT] FILINFO structure */
){
struct stat info;
    return stat(path, &info);
}

