#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gio/gio.h>
#include "builder.bundle.c"
#include <stdint.h>
#include "finestra.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "buttons.h"

SemaphoreHandle_t  window_mutex;
SemaphoreHandle_t  button_mutex;
QueueHandle_t buttonsQueue;

void print(char*){}
void itoa(int, char*, int){}
static void
print_hello (GtkWidget *widget,
             gpointer   data)
{
  g_print ("Hello World\n");
}
GdkPixbuf * PixBuf;
GtkWidget *drawing;
void button_up_p_isr(GtkWidget *widget,
             gpointer   data);
void button_down_p_isr(GtkWidget *widget,
             gpointer   data);
void button_up_r_isr(GtkWidget *widget,
             gpointer   data);
void button_down_r_isr(GtkWidget *widget,
             gpointer   data);
void button_ok_p_isr(GtkWidget *widget,
             gpointer   data);
void button_ok_r_isr(GtkWidget *widget,
             gpointer   data);
void put_pixel (GdkPixbuf *pixbuf, int x, int y, guchar red, guchar green, guchar blue, guchar alpha)
{
  //while(xSemaphoreTake(window_mutex, 1000)==pdFALSE);
  //xSemaphoreTake(pixel_mutex, 0);
  int width, height, rowstride, n_channels;
  guchar *pixels, *p;

  //rowstride = gdk_pixbuf_get_rowstride (pixbuf);
  pixels = gdk_pixbuf_get_pixels (pixbuf);
  //n_channels = gdk_pixbuf_get_n_channels (pixbuf);

  p = pixels + y * 600*3 + (599-x) * 3;
  p[0] = red;
  p[1] = green;
  p[2] = blue;
  //xSemaphoreGive(window_mutex);

}
void eink_update(){
  while(xSemaphoreTake(window_mutex, 1000)==pdFALSE);
  //vTaskDelay(100);
  cairo_t *cr;
  cr = gdk_cairo_create (gtk_widget_get_window(drawing));
  //    cr = gdk_cairo_create (da->window);
  gdk_cairo_set_source_pixbuf(cr, PixBuf, 0, 0);
  cairo_paint(cr);

  //    cairo_fill (cr);
  cairo_destroy (cr);
  //    return FALSE;*/
  //xSemaphoreGive(pixel_mutex);
  xSemaphoreGive(window_mutex);
}
static gboolean
on_window_draw (GtkWidget *da, GdkEvent *event, gpointer data)
{
    (void)event; (void)data;

    GError *err = NULL;
    /* Create pixbuf */

    if(err)
    {
        printf("Error : %s\n", err->message);
        g_error_free(err);
        return FALSE;
    }
    cairo_t *cr;
    cr = gdk_cairo_create (gtk_widget_get_window(da));
    //    cr = gdk_cairo_create (da->window);
    gdk_cairo_set_source_pixbuf(cr, PixBuf, 0, 0);
    cairo_paint(cr);
    //    cairo_fill (cr);
    cairo_destroy (cr);
    //    return FALSE;
}
gboolean mutexLet(gpointer user_data){
  xSemaphoreGive(window_mutex);
  vTaskDelay(100);
  while(xSemaphoreTake(window_mutex, 1000)==pdFALSE);
  vTaskDelay(100);
  //while(xSemaphoreTake(pixel_mutex, 1000)==pdFALSE);
  return true;
}

__attribute__((weak)) void btn_up(){
    button_event e=BTN_UP;
    xQueueSendFromISR(buttonsQueue,&e,NULL);
}
__attribute__((weak)) void btn_ok(){
    button_event e=BTN_OK;
    xQueueSendFromISR(buttonsQueue,&e,NULL);
};
__attribute__((weak)) void btn_dwn(){
    button_event e=BTN_DWN;
    xQueueSendFromISR(buttonsQueue,&e,NULL);
};
__attribute__((weak)) void btn_ind(){
    button_event e=BTN_IND;
    xQueueSendFromISR(buttonsQueue,&e,NULL);
};
__attribute__((weak)) void btn_bck(){
    button_event e=BTN_BACK;
    xQueueSendFromISR(buttonsQueue,&e,NULL);
};

void gtk_task(void *args __attribute__((unused)))

{
  GtkBuilder *builder;
  GObject *window;
  GObject *button;
  GResource *gr= builder_get_resource ();
  PixBuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB,
                FALSE,
                8,
                600,
                800);
  GError *error = NULL;
  int argc  = 0;
  char** argv;

  gtk_init (&argc, &argv);

  /* Construct a GtkBuilder instance and load our UI description */
  builder = gtk_builder_new_from_resource("/org/gtk/ook/screen.ui");

  /* Connect signal handlers to the constructed widgets. */
  drawing = (GtkWidget*)gtk_builder_get_object (builder, "drawing");
  /*g_signal_connect ( drawing, "draw", (GCallback) on_window_draw, NULL);*/
      gtk_widget_set_app_paintable(drawing, TRUE);

  window = gtk_builder_get_object (builder, "window");
  g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

  button = gtk_builder_get_object (builder, "bdwn");
  g_signal_connect (button, "pressed", G_CALLBACK (btn_dwn), NULL);

  button = gtk_builder_get_object (builder, "bup");
  g_signal_connect (button, "pressed", G_CALLBACK (btn_up), NULL);

    button = gtk_builder_get_object (builder, "bok");
  g_signal_connect (button, "pressed", G_CALLBACK (btn_ok), NULL);


      button = gtk_builder_get_object (builder, "back");
    g_signal_connect (button, "pressed", G_CALLBACK (btn_bck), NULL);

  /*button = gtk_builder_get_object (builder, "button2");
  g_signal_connect (button, "clicked", G_CALLBACK (print_hello), NULL);

  button = gtk_builder_get_object (builder, "quit");
  g_signal_connect (button, "clicked", G_CALLBACK (gtk_main_quit), NULL);*/
  g_idle_add (mutexLet,
            NULL);
  gtk_main ();
  while(1);
  /*for(int i=0; i<600; i++)
      for(int j=0;j<800;j++)
  put_pixel(PixBuf,i,j,255,255,255,0);*/

  return;
}


/** eInk emulation functions */
uint16_t frame_pos, line_pos;
uint8_t eink_color;

void eink_init(){
  window_mutex=xSemaphoreCreateBinary();

  //pixel_mutex=xSemaphoreCreateBinary();
}
void eink_start_frame(){
  frame_pos=0;
  line_pos=0;
}
void eink_end_frame(){
  //exit(0);
  eink_update();
}
void eink_write_row_BN(uint16_t wt){
  frame_pos++;

  //eink_update();
}
void eink_skip_row(){
  frame_pos++;
}
void eink_start_row_BN(){
  line_pos=0;
}
void eink_set_data(uint16_t color){
  eink_color=color;
}

void eink_send_data(uint8_t data[], uint16_t data_start, uint16_t data_len){
  for (int i=data_start; i<data_len+data_start;i++){
    if(PIXEL_IS_BLACK(i%4, data[i/4])){
      put_pixel(PixBuf, frame_pos,i, 0, 0, 0, 255);
    }else if(PIXEL_IS_WHITE(i%4, data[i/4])){
      put_pixel(PixBuf, frame_pos,i, 255, 255, 255, 255);
    }
    line_pos=i;
  }
}


void eink_clear(uint16_t color){
    for (int i=0; i<800; i++)
      for (int j=0; j<600; j++){
        if(PIXEL_IS_BLACK(i%4, color)){
          put_pixel(PixBuf, j,i, 0, 0, 0, 255);
        }else if(PIXEL_IS_WHITE(i%4, color)){
          put_pixel(PixBuf, j,i, 255, 255, 255, 255);

        }
      }
      eink_update();

}

void eink_row_convert(uint8_t *data_in, uint8_t *data_out, uint16_t start, uint16_t end, uint8_t bpp){
  for (int i=0;i<800; i+=4){
    if(i>end || (i+bpp)<start){ // Area que no s'ha d'escriure a 0
      data_out[i/4]=0;
      continue;
    }
    data_out[i/4]=eink_data_convert(bpp,data_in[(i*bpp)/8], 0x1 << (bpp-1));
  }
}
void eink_row_convert(uint8_t *data_in, uint8_t *data_out, uint8_t bpp){
  eink_row_convert(data_in, data_out, 0,800, bpp);
}
uint16_t eink_data_convert(uint8_t bpp, uint8_t data, uint8_t thresh){
  uint16_t r=0;
  switch (bpp){
    case 1:
        for(int i=0; i<8; i++){
          r |= (BLACK_COND(i%4, data&(0x1<<i)) | WHITE_COND(i%4, ~data&(0x1<<i)))<<(i/4);
        }
        return r;
    case 2:
        for(int i=0; i<4; i++){
          r |= (BLACK_COND(i, !(data&(0xC0>>(i*2))) ) |
                WHITE_COND(i, data&(0xC0>>(i*2))));

        }
        return r;

  }
}

void init_buttons(){
      button_mutex=xSemaphoreCreateBinary();
          buttonsQueue =   xQueueCreate( 10, sizeof(button_event));
}
void wait_button_press(){
    //xSemaphoreTake(button_mutex, portMAX_DELAY);
}
uint8_t b_up, b_down, b_ok;


uint8_t read_button_up(){
    return b_up;
}
uint8_t read_button_down(){
    return b_down;
}
uint8_t read_button_ok(){
    return b_ok;
}
