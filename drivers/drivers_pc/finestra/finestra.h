#define ALL_WHITE       0x000F //01010101
#define ALL_BLACK       0x00F0 //10101010

#define PIXEL_MASK(x)   (0x0010<<x | 0x0008>>x) //00000011
#define PIXEL_BLACK(x)  (PIXEL_MASK(x)&ALL_BLACK)
#define PIXEL_WHITE(x)  (PIXEL_MASK(x)&ALL_WHITE)

#define BLACK_COND(x,s) (s ? PIXEL_BLACK(x) : 0) //writes black if s is True
#define WHITE_COND(x,s) (s ? PIXEL_WHITE(x) : 0)

#define MASKED_COLOR(p,c) (PIXEL_MASK(p)&c)

#define PIXEL_IS_BLACK(p,c) ((MASKED_COLOR(p,c)&ALL_BLACK) && !(MASKED_COLOR(p,c)&ALL_WHITE) )
#define PIXEL_IS_WHITE(p,c) (!(MASKED_COLOR(p,c)&ALL_BLACK) && (MASKED_COLOR(p,c)&ALL_WHITE) )
#define PIXEL_IS_NONE(p,c) ( !PIXEL_IS_BLACK(p,c) && !PIXEL_IS_WHITE(p,c))

void gtk_task(void *args __attribute__((unused)));
uint16_t eink_data_convert(uint8_t bpp, uint8_t data, uint8_t thresh);
void eink_row_convert(uint8_t *data_in, uint8_t *data_out, uint16_t start, uint16_t end, uint8_t bpp);

void eink_start_frame();
void eink_end_frame();
void eink_write_row_BN(uint16_t wt);
void eink_skip_row();
void eink_start_row_BN();
void eink_set_data(uint16_t color);
void eink_clear(uint16_t color);

void eink_init();

void eink_send_data(uint8_t data[], uint16_t data_start, uint16_t data_len);


void init_buttons();
#define buttons_exti_setup() init_buttons()

void wait_button_press();
uint8_t read_button_up();
uint8_t read_button_down();
uint8_t read_button_ok();
