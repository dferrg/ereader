#include <libopencm3/stm32/exti.h>
#include <libopencm3/stm32/f4/nvic.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "portable.h"
#include "usbserial.h"
#include "buttons.h"
#include "clock.h"
#include "power.h"
#define NULL 0

QueueHandle_t debounceQueue;
QueueHandle_t buttonsQueue;
struct exti_trigger{
    uint32_t exti;
    uint32_t trigger;
    TickType_t tick;
};
void debounce_task(void *args __attribute__((unused))) {
    struct exti_trigger exti;
    BaseType_t qresp;

    while(1){
        do{
            qresp = xQueueReceive(debounceQueue, &exti, 1000);
                    //print("debounce\r\n");
        }while(qresp==pdFALSE);
        while(xTaskGetTickCount()-exti.trigger<50) vTaskDelay(10);
	    exti_set_trigger  (exti.exti, exti.trigger);
    }
}

void buttons_exti_setup(){
	rcc_periph_clock_enable(RCC_GPIOB);
	rcc_periph_clock_enable(RCC_GPIOC);
	rcc_periph_clock_enable(RCC_GPIOD);
   rcc_periph_clock_enable(RCC_SYSCFG);

    gpio_mode_setup(GPIOD, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO10);//UP
    gpio_mode_setup(GPIOD, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO11);//OK
    gpio_mode_setup(GPIOD, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO13);//DWN

    gpio_mode_setup(GPIOB, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO4); //HOME
    gpio_mode_setup(GPIOB, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO6); //IND
    gpio_mode_setup(GPIOB, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO7); //BACK

    gpio_mode_setup(GPIOB, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO1); //PWR

    exti_select_source(EXTI6, GPIOB); //IND
	exti_set_trigger  (EXTI6, EXTI_TRIGGER_RISING);
	exti_enable_request(EXTI6);

    exti_select_source(EXTI4, GPIOB); //HOME
	exti_set_trigger  (EXTI4, EXTI_TRIGGER_RISING);
	exti_enable_request(EXTI4);

    exti_select_source(EXTI7, GPIOB); //BACK
	exti_set_trigger  (EXTI7, EXTI_TRIGGER_RISING);
	exti_enable_request(EXTI7);

    exti_select_source(EXTI1, GPIOB); //PWR
	exti_set_trigger  (EXTI1, EXTI_TRIGGER_FALLING);
	exti_enable_request(EXTI1);

    exti_select_source(EXTI10, GPIOD); //UP
	exti_set_trigger  (EXTI10, EXTI_TRIGGER_RISING);
	exti_enable_request(EXTI10);

    exti_select_source(EXTI11, GPIOD);//OK
	exti_set_trigger  (EXTI11, EXTI_TRIGGER_RISING);
	exti_enable_request(EXTI11);

    exti_select_source(EXTI13, GPIOD);//DWN
	exti_set_trigger  (EXTI13, EXTI_TRIGGER_RISING);
	exti_enable_request(EXTI13);

    debounceQueue =   xQueueCreate( 10, sizeof(struct exti_trigger));
    buttonsQueue =   xQueueCreate( 10, sizeof(button_event));

    uint32_t r=xTaskCreate(debounce_task, "debouncing", 200, NULL, 4, NULL);

	nvic_enable_irq(NVIC_EXTI15_10_IRQ);
	nvic_enable_irq(NVIC_EXTI9_5_IRQ);
	nvic_enable_irq(NVIC_EXTI4_IRQ);
	nvic_enable_irq(NVIC_EXTI1_IRQ);


}
__attribute__((weak)) void btn_up(){
    button_event e=BTN_UP;
    xQueueSendFromISR(buttonsQueue,&e,NULL);
}
__attribute__((weak)) void btn_ok(){
    button_event e=BTN_OK;
    xQueueSendFromISR(buttonsQueue,&e,NULL);
};
__attribute__((weak)) void btn_dwn(){
    button_event e=BTN_DWN;
    xQueueSendFromISR(buttonsQueue,&e,NULL);
};
__attribute__((weak)) void btn_ind(){
    button_event e=BTN_IND;
    xQueueSendFromISR(buttonsQueue,&e,NULL);
};
__attribute__((weak)) void btn_bck(){
    button_event e=BTN_BACK;
    xQueueSendFromISR(buttonsQueue,&e,NULL);
};
__attribute__((weak)) void btn_home(){/*print("home\r\n");*/};
__attribute__((weak)) void btn_pwr(){/*print("power\r\n");*/};
__attribute__((weak)) void usb_connected(){};
__attribute__((weak)) void sys_deinit (void) {};


#define RELEASED 0
#define WAITING 1

volatile uint16_t clock_pwr_btn = WAITING;
void exti1_isr(void){ //POWER BUTTON
	exti_reset_request(EXTI1);
    //read GPIO value and handle
    //if(!gpio_get(GPIOC,GPIO7)){
        if(clock_pwr_btn==WAITING) clock_pwr_btn=RELEASED;
	    else {
	        sys_deinit();
	        power_off();
	    }
	//}
    exti_set_trigger  (EXTI1, EXTI_TRIGGER_RISING);
    struct exti_trigger exti={EXTI1, EXTI_TRIGGER_FALLING, xTaskGetTickCountFromISR()};
    xQueueSendFromISR(debounceQueue, &exti, NULL);
}
void exti4_isr(void){ //HOME BUTTON
	exti_reset_request(EXTI4);
	//read GPIO value and handle
	if(gpio_get(GPIOB,GPIO4))
	    btn_home();

    exti_set_trigger  (EXTI4, EXTI_TRIGGER_FALLING);
    struct exti_trigger exti={EXTI4, EXTI_TRIGGER_RISING, xTaskGetTickCountFromISR()};
	xQueueSendFromISR(debounceQueue, &exti, NULL);
}

void exti9_5_isr(void){
    if(exti_get_flag_status(EXTI6)){ //INDEX BUTTON
    	exti_reset_request(EXTI6);
	    //read GPIO value and handle
	    if(gpio_get(GPIOB,GPIO6))
	        btn_ind();

        exti_set_trigger  (EXTI6, EXTI_TRIGGER_FALLING);
        struct exti_trigger exti={EXTI6, EXTI_TRIGGER_RISING, xTaskGetTickCountFromISR()};
	    xQueueSendFromISR(debounceQueue, &exti, NULL);
    }
    if(exti_get_flag_status(EXTI7)){ //BACK BUTTON
    	exti_reset_request(EXTI7);
	    //read GPIO value and handle
	    if(gpio_get(GPIOB,GPIO7))
	        btn_bck();

        exti_set_trigger  (EXTI7, EXTI_TRIGGER_FALLING);
        struct exti_trigger exti={EXTI7, EXTI_TRIGGER_RISING, xTaskGetTickCountFromISR()};
	    xQueueSendFromISR(debounceQueue, &exti, NULL);
    }

}
void exti15_10_isr(void){
    if(exti_get_flag_status(EXTI10)){
    	exti_reset_request(EXTI10);
	    //read GPIO value and handle
	    if(gpio_get(GPIOD,GPIO10))
	        btn_up();

        exti_set_trigger  (EXTI10, EXTI_TRIGGER_FALLING);
        struct exti_trigger exti={EXTI10, EXTI_TRIGGER_RISING, xTaskGetTickCountFromISR()};
	    xQueueSendFromISR(debounceQueue, &exti, NULL);
    }
    if(exti_get_flag_status(EXTI11)){
    	exti_reset_request(EXTI11);
	    //read GPIO value and handle
	    if(gpio_get(GPIOD,GPIO11))
	        btn_ok();

        exti_set_trigger  (EXTI11, EXTI_TRIGGER_FALLING);
        struct exti_trigger exti={EXTI11, EXTI_TRIGGER_RISING, xTaskGetTickCountFromISR()};
	    xQueueSendFromISR(debounceQueue, &exti, NULL);
    }
    if(exti_get_flag_status(EXTI13)){
    	exti_reset_request(EXTI13);
	    //read GPIO value and handle
	    if(gpio_get(GPIOD,GPIO13))
	        btn_dwn();

        exti_set_trigger  (EXTI13, EXTI_TRIGGER_FALLING);
        struct exti_trigger exti={EXTI13, EXTI_TRIGGER_RISING, xTaskGetTickCountFromISR()};
	    xQueueSendFromISR(debounceQueue, &exti, NULL);
    }
    if(exti_get_flag_status(EXTI15)){
    	exti_reset_request(EXTI15);
	    //read GPIO value and handle
	    if(gpio_get(GPIOD,GPIO15))
	        usb_connected();

        exti_set_trigger  (EXTI15, EXTI_TRIGGER_FALLING);
        struct exti_trigger exti={EXTI15, EXTI_TRIGGER_RISING, xTaskGetTickCountFromISR()};
	    xQueueSendFromISR(debounceQueue, &exti, NULL);
    }

}

void init_buttons(){}
void wait_button_press(){
    button_event v;
    xQueuePeek(buttonsQueue, &v, portMAX_DELAY);
}
uint8_t read_button_up(){return 0;}
uint8_t read_button_down(){return 0;}
uint8_t read_button_ok(){return 0;}
