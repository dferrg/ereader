#include "ED060SC4.h"
#include "TPS65186.h"
static inline void delay_us(uint32_t t){
    for (uint32_t i=0; i<t*7; i++)
    __asm("nop");
}

const int contrast_cycles_4[15] = { 35, 20, 20, 20,10,10,10,10,10,10,3,3,3,3,10};
const int contrast_cycles_white[15] = {30, 30, 20, 20, 30,  30,  30, 40,
                                   40, 200, 50, 50, 100, 200, 300};
void eink_gpio_init();
static inline void eink_cl_pos_tick(uint32_t t);

//gate functions
void eink_start_frame();
void eink_write_row();
void eink_skip_row();
void eink_end_frame();

//source functions
void eink_start_row();
void eink_send_data(uint8_t data[], uint16_t data_start, uint16_t data_len);
void eink_end_row();
void eink_timer_setup();
void eink_init(){
    PMIC_init();
    eink_gpio_init();
    //eink_timer_setup();
}

void eink_timer_setup(){
  /** TIM2 as master, counts to 200 **/
  rcc_periph_clock_enable(RCC_TIM2);
  rcc_periph_reset_pulse(RST_TIM2);

  timer_set_mode(TIM2, TIM_CR1_CKD_CK_INT,
		TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);

	timer_set_prescaler(TIM2, 168);
  timer_disable_preload(TIM2);
  timer_continuous_mode(TIM2);
  timer_set_period(TIM2, 201);

  timer_set_oc_mode(TIM2, TIM_OC1, TIM_OCM_TOGGLE);
  timer_enable_oc_output(TIM2, TIM_OC1);
  timer_set_oc_value(TIM2, TIM_OC1,200);


  timer_set_master_mode(TIM2, TIM_CR2_MMS_COMPARE_OC1REF);
  timer_clear_flag(TIM2, TIM_SR_UIF);

  /** TIM4 as slave, controls CL **/
  rcc_periph_clock_enable(RCC_TIM4);
  rcc_periph_reset_pulse(RST_TIM4);

  timer_set_mode(TIM4, TIM_CR1_CKD_CK_INT,
		TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);

	timer_set_prescaler(TIM4, 168/2);

  timer_disable_preload(TIM4);
  timer_continuous_mode(TIM4);
  timer_set_period(TIM4, 1);


  timer_slave_set_mode(TIM4, TIM_SMCR_SMS_GM);
  timer_slave_set_trigger(TIM4, 	TIM_SMCR_TS_ITR1);

    timer_set_oc_mode(TIM4, TIM_OC3, TIM_OCM_PWM1);
    timer_enable_oc_output(TIM4, TIM_OC3);
    timer_set_oc_value(TIM4, TIM_OC3,1);
  timer_enable_counter(TIM4);


}
/*
 * Realitza les inicialitzacions de sortides GPIO necessàries
 */
void eink_gpio_init(){

    //gate
    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_GPIOB);
    gpio_mode_setup(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, G_GMODE); //GMODE
    gpio_mode_setup(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, G_SPV); //SPV
    gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, G_CKV); //CKV
    eink_set(G_SPV);

    //source
    gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, S_CL); //CL
    gpio_mode_setup(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, S_LE); //LE
    gpio_mode_setup(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, S_OE); //OE
    gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, S_SPH); //SPH
    gpio_mode_setup(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, S_SHR); //SHR
    gpio_clear(GPIOA, S_SHR);

    //data lines
    rcc_periph_clock_enable(RCC_GPIOE);
    gpio_mode_setup(GPIOE, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO0);
    gpio_mode_setup(GPIOE, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO1);
    gpio_mode_setup(GPIOE, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO2);
    gpio_mode_setup(GPIOE, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO3);
    gpio_mode_setup(GPIOE, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO4);
    gpio_mode_setup(GPIOE, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO5);
    gpio_mode_setup(GPIOE, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO6);
    gpio_mode_setup(GPIOE, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO7);

    gpio_mode_setup(GPIOE, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, S_RL);
    gpio_set(GPIOE, S_RL);

    gpio_mode_setup(GPIOE, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO8);
    gpio_set(GPIOE, GPIO8);

}


/****************************************************************************/
/*                                                                          */
/*                                GATE DRIVER                               */
/*                                                                          */
/****************************************************************************/
void eink_ckv_hi(){
  gpio_set(GPIOB,G_CKV);
}
void eink_ckv_low(){
  gpio_clear(GPIOB,G_CKV);
}
void eink_ckv_neg_tick(uint32_t t){
    eink_ckv_low();
    delay_us(t);
    eink_ckv_hi();
    //delay_us(t);
}



//       ______________  _  _  _  _
//  CKV _|             || || || || ···
//       __________________________
//GMODE _|                         ···
//             _______   __________
//  SPV XXXXXXX      |___|         ···
//https://wavedrom.com/editor.html?%7Bsignal%3A%20%5B%0A%20%20%7Bname%3A%20%27CKV%27%2C%20wave%3A%20%27lh...........l.hppp%27%7D%2C%0A%20%20%7Bname%3A%20%27GMODE%27%2C%20wave%3A%20%27lh.................%27%7D%2C%0A%20%20%7Bname%3A%20%27SPV%27%2C%20wave%3A%20%27xxxxxxxxh...l...h..%27%7D%2C%0A%5D%7D%0A

void eink_start_frame(){

    //set CKV & GMODE
    eink_set(G_GMODE);
    eink_ckv_hi();
    delay_us(100);

    //pulse SPV & CKV
    eink_lclear(G_SPV);
    delay_us(50);
    eink_ckv_neg_tick(3);
    delay_us(3);
    eink_set(G_SPV);
    delay_us(1);
    eink_ckv_neg_tick(0);
    delay_us(10);
    eink_set(G_SPV);

    eink_set(S_OE);
    eink_ckv_neg_tick(1);
    delay_us(1);
}
//      _  _  _  _  _  _  _  _  _          ___
//  CKV  || || || || || || || || |________|   ···
//https://wavedrom.com/editor.html?%7Bsignal%3A%20%5B%0A%20%20%7Bname%3A%20%27CKV%27%2C%20wave%3A%20%27hpppppppppl.....h...%27%7D%2C%0A%5D%7D%0A
void eink_end_frame(){
    //8 CKV ticks

/*
    for(int i=0; i<8; i++)
        eink_ckv_neg_tick(3);

    eink_ckv_low();
    delay_us(30);
    eink_ckv_hi();
    delay_us(43);

        eink_lclear(S_OE);
        eink_lclear(G_GMODE);
        delay_us(100000);
        PMIC_standby();
        delay_us(1000000);
        PMIC_pwrup();
        delay_us(100000);
//    PMIC_standby();*/

eink_lclear(S_OE);
eink_lclear(G_GMODE);
for(int i=0; i<8; i++)
  eink_ckv_neg_tick(3);

eink_ckv_neg_tick(1);

gpio_clear(GPIOB, G_CKV);
eink_lclear(G_SPV);
}
//https://wavedrom.com/editor.html?%7Bsignal%3A%20%5B%0A%20%20%7Bname%3A%20%27CKV%27%2C%20wave%3A%20%27hpl.....nh.%27%7D%2C%0A%20%20%7Bname%3A%20%27CL%27%2C%20wave%3A%20%27lhl........%27%7D%2C%0A%20%20%7Bname%3A%20%27OE%27%2C%20wave%3A%20%27h.pl....h..%27%7D%2C%0A%20%20%7Bname%3A%20%27LE%27%2C%20wave%3A%20%27l........pl%27%7D%0A%5D%7D%0A
//potser millor:
//https://wavedrom.com/editor.html?%7Bsignal%3A%20%5B%0A%20%20%7Bname%3A%20%27CKV%27%2C%20wave%3A%20%27hl..h......%27%7D%2C%0A%20%20%7Bname%3A%20%27CL%27%2C%20wave%3A%20%27lpl........%27%7D%2C%0A%20%20%7Bname%3A%20%27OE%27%2C%20wave%3A%20%27h..........%27%7D%2C%0A%20%20%7Bname%3A%20%27LE%27%2C%20wave%3A%20%27l........pl%27%7D%0A%5D%7D%0A
void eink_write_row(uint32_t wt){
  eink_cl_pos_tick(0);
  eink_cl_pos_tick(0);


    eink_set(S_LE);
    eink_cl_pos_tick(0);
    eink_cl_pos_tick(0);


    eink_lclear(S_LE);
    eink_cl_pos_tick(0);
    eink_cl_pos_tick(0);


    eink_ckv_hi();
    delay_us(wt);
    eink_ckv_low();
    delay_us(wt);

    eink_lclear(S_OE);


    eink_cl_pos_tick(0);
    eink_cl_pos_tick(0);



}

void eink_write_row_BN(uint32_t wt){
  eink_cl_pos_tick(0);
  eink_cl_pos_tick(0);


    eink_ckv_low();
    eink_lclear(S_LE);
    eink_lclear(S_OE);

    eink_cl_pos_tick(0);
    eink_cl_pos_tick(0);

    eink_ckv_hi();


}

void eink_skip_row(){
    eink_ckv_neg_tick(10);
    delay_us(10);
}


/****************************************************************************/
/*                                                                          */
/*                               SOURCE DRIVER                              */
/*                                                                          */
/****************************************************************************/

static inline void eink_cl_pos_tick(uint32_t t){
    gpio_set(GPIOB, S_CL);
    //delay_us(t);
    gpio_clear(GPIOB, S_CL);
    //delay_us(t);
}

void eink_start_row(){

    eink_set(S_OE);
    eink_lclear(S_SPH);
}
void eink_start_row_BN(){
    eink_set(S_LE);
    eink_cl_pos_tick(1);
    eink_cl_pos_tick(1);
    eink_lclear(S_LE);
    eink_cl_pos_tick(1);
    eink_cl_pos_tick(1);
    eink_set(S_OE);
    eink_lclear(S_SPH);
}
void eink_end_row(){
    delay_us(1);
    eink_set(S_SPH);
}


/****************************************************************************/
/*                                                                          */
/*                              SCREEN CLEAR                                */
/*                                                                          */
/****************************************************************************/

void eink_set_data(uint16_t color){
    gpio_clear(GPIOE, 0x00FF);//clear data lines
    gpio_set(GPIOE, color & 0x00FF);
    delay_us(2);
}

void eink_clear_row(uint16_t color){
    eink_set_data(color);
    eink_start_row_BN();
    for(int i=0; i<200; i++){
        eink_cl_pos_tick(1);
    }
    eink_end_row();
}
void eink_DMA_clear_row(uint16_t color){
    eink_set_data(color);
    eink_start_row_BN();
//reset_timers

eink_timer_setup();
timer_set_counter(TIM2,0);
gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_NONE, S_CL); //CL
gpio_set_af(GPIOB, GPIO_AF2, S_CL);
delay_us(100);
 timer_enable_counter(TIM2);
 timer_clear_flag(TIM2, TIM_SR_UIF);

while(timer_get_counter(TIM2)<200);
gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, S_CL); //CL

    eink_end_row();
}
void eink_clear(uint16_t color, uint8_t time){
    //PMIC_pwrup();
    eink_start_frame();
    for (int i=0; i<600; i++){
        eink_clear_row(i%2 ? color : color);
        eink_write_row_BN(time);
    }
    eink_end_frame();
       // PMIC_standby();
}




/****************************************************************************/
/*                                                                          */
/*                              SCREEN WRITE                                */
/*                                                                          */
/****************************************************************************/
//converteix el "framebuffer" (4bpp) al format de la pantalla (2bpp)

uint8_t eink_fb2dl_black(uint8_t fb[],uint8_t thl, uint8_t thh){
    uint8_t dataout=0;
    uint8_t p1=((fb[0]&0xF0)>>4);
    uint8_t p2=(fb[0]&0x0F);
    uint8_t p3=((fb[1]&0xF0)>>4);
    uint8_t p4=(fb[1]&0x0F);

    dataout=BLACK_COND(0, p1<thh & p1>thl) | BLACK_COND(1, p2<thh & p2>thl) | BLACK_COND(2, p3<thh & p3>thl) | BLACK_COND(3, p4<thh & p4>thl); //pinta a negre cada pixel si es major que el limit

    return dataout;
}

uint8_t eink_fb2dl_white(uint8_t fb[],int thl, uint8_t thh){
    uint8_t dataout=0;
    uint8_t p1=((fb[0]&0xF0)>>4);
    uint8_t p2=(fb[0]&0x0F);
    uint8_t p3=((fb[1]&0xF0)>>4);
    uint8_t p4=(fb[1]&0x0F);

    dataout|=WHITE_COND(0, p1<thh & p1>thl) | WHITE_COND(1, p2<thh & p2>thl) | WHITE_COND(2, p3<thh & p3>thl) | WHITE_COND(3, p4<thh & p4>thl); //pinta a negre cada pixel si es major que el limit
    return dataout;
}
uint8_t eink_fb2dl_BN(uint8_t fb[],uint8_t threshold){
    uint8_t dataout=0;
    uint8_t p1=((fb[0]&0xF0)>>4) > threshold;
    uint8_t p2=(fb[0]&0x0F) > threshold;
    uint8_t p3=((fb[1]&0xF0)>>4) > threshold;
    uint8_t p4=(fb[1]&0x0F) > threshold;

    dataout=BLACK_COND(0, p1) | BLACK_COND(1, p2) | BLACK_COND(2, p3) | BLACK_COND(3, p4); //pinta a negre cada pixel si es major que el limit
    dataout|=WHITE_COND(0, !p1) | WHITE_COND(1, !p2) | WHITE_COND(2, !p3) | WHITE_COND(3, !p4); //pinta a negre cada pixel si es major que el limit
    return dataout;
}
void eink_screen_send_row(uint8_t data[]){
    eink_start_row();
    for(int i=0; i<200; i++){
        eink_set_data(data[i]);
        eink_cl_pos_tick(1);
    }
    eink_end_row();
}

void eink_screen_send_row_def(uint8_t data[],uint8_t thresh){
    eink_start_row();
    for(int i=0; i<400; i+=2){
        eink_set_data(eink_fb2dl_black(&data[i], thresh, 16));
        eink_cl_pos_tick(1);
    }
    eink_end_row();
}
void eink_screen_send_row_BN(uint8_t data[]){
    eink_start_row_BN();
    for(int i=0; i<400; i+=2){
        eink_set_data(eink_fb2dl_BN(&data[i], 14));
        eink_cl_pos_tick(1);
    }
    eink_end_row();
}
void eink_screen_send_row_part_BN(uint8_t data[],uint16_t start_column, uint16_t n_columns){
    start_column-=start_column%2;
    n_columns-=n_columns%2;
    eink_start_row_BN();
    eink_set_data(0x00);
    for(int i=0; i<start_column; i+=2)
            eink_cl_pos_tick(1);
    for(int i=0; i<n_columns; i+=2){
        eink_set_data(eink_fb2dl_BN(&data[i], 14));
        eink_cl_pos_tick(1);
    }

    eink_set_data(0x00);
    for(int i=start_column+n_columns; i<400; i+=2)
            eink_cl_pos_tick(1);
    eink_end_row();
}
void eink_screen_send_row_part_def(uint8_t data[],uint8_t thresh ,uint16_t start_column, uint16_t n_columns){
    start_column-=start_column%2;
    n_columns-=n_columns%2;
    eink_start_row();
        eink_set_data(0x00);
    for(int i=0; i<start_column; i+=2)
            eink_cl_pos_tick(1);
    for(int i=0; i<n_columns; i+=2){
        eink_set_data(eink_fb2dl_black(&data[i], thresh, 16));
        eink_cl_pos_tick(1);
    }
       eink_set_data(0x00);
    for(int i=start_column+n_columns; i<400; i+=2)
            eink_cl_pos_tick(1);
    eink_end_row();
}
void eink_screen_send_row_part_def_w(uint8_t data[],uint8_t thresh ,uint16_t start_column, uint16_t n_columns){
    start_column-=start_column%2;
    n_columns-=n_columns%2;
    eink_start_row();
        eink_set_data(0x00);
    for(int i=0; i<start_column; i+=2)
            eink_cl_pos_tick(1);
    for(int i=0; i<n_columns; i+=2){
        eink_set_data(eink_fb2dl_white(&data[i], 0, thresh));
        eink_cl_pos_tick(1);
    }
       eink_set_data(0x00);
    for(int i=start_column+n_columns; i<400; i+=2)
            eink_cl_pos_tick(1);
    eink_end_row();
}
void eink_null_row(){
    eink_clear_row(0x00);
    eink_write_row(1);
}
void eink_write_region_BN(uint8_t *data, uint16_t start_line, uint16_t n_lines,  //limits en files
                                         uint16_t start_column, uint16_t n_columns) //limits en columnes
{
        eink_start_frame();
        //jump to start_line

        if(start_line)
        eink_null_row();
        for (int i=1; i<start_line; i++)
            eink_skip_row();

        for (int i=0    ; i<n_lines; i++){
            //escriu dades per pantalla
                eink_screen_send_row_part_BN(&data[i*n_columns/2],start_column/2, n_columns/2);
                eink_write_row_BN(0);

        }
        eink_null_row();
        for (int i=n_lines+start_line+1; i<600; i++)
            eink_skip_row();
        //eink_end_frame();

}

void eink_write_region_def(uint8_t *data, uint16_t start_line, uint16_t n_lines,
                                          uint16_t start_column, uint16_t n_columns,
                                                uint8_t min_d, uint8_t max_d ){
    for (int j=min_d; j<15; j++){
        eink_start_frame();
        //jump to start_line

        if(start_line)
        eink_null_row();
        for (int i=1; i<start_line; i++)
            eink_skip_row();

        for (int i=0; i<n_lines+1; i++){
            //escriu dades per pantalla
                eink_screen_send_row_part_def(&data[i*n_columns/2], j, start_column/2, n_columns/2);
                eink_write_row(contrast_cycles_4[j]);

        }
        eink_null_row();
        for (int i=n_lines+start_line+1; i<600; i++)
            eink_skip_row();
        eink_end_frame();
    }
    for (int j=0; j<15; j++){
        eink_start_frame();
        //jump to start_line

        if(start_line)
        eink_null_row();
        for (int i=1; i<start_line; i++)
            eink_skip_row();

        for (int i=0; i<n_lines+1; i++){
            //escriu dades per pantalla
                eink_screen_send_row_part_def_w(&data[i*n_columns/2], j, start_column/2, n_columns/2);
                eink_write_row(2.5+j*0.6-j*j*0.05);

        }
        eink_null_row();
        for (int i=n_lines+start_line+1; i<600; i++)
            eink_skip_row();
        eink_end_frame();
    }
}


void eink_screen_write_BN(uint8_t data[][400], uint16_t start_line, uint16_t n_lines){
      //  eink_write_region_BN(data, start_line, n_lines, 0, 400);
}




void eink_screen_write_def(uint8_t data[][400], uint16_t start_line, uint16_t n_lines, uint8_t min_d, uint8_t max_d ){
    /*for (int j=min_d; j<max_d; j++){
        eink_start_frame();
        //jump to start_line

        if(start_line)
        eink_null_row();
        for (int i=1; i<start_line; i++)
            eink_skip_row();

        for (int i=0; i<n_lines+1; i++){
            //escriu dades per pantalla
                eink_screen_send_row_def(&data[i][0], j);
                eink_write_row(contrast_cycles_4[j]);

        }
        eink_null_row();
        for (int i=n_lines+start_line+1; i<600; i++)
            eink_skip_row();
        //eink_end_frame();
    }*/
  //  eink_write_region_def(data, start_line, n_lines, 0, 400, min_d, max_d);
}



void eink_test(){
    eink_start_frame();
    uint8_t line_data[200];
    for (int i=0; i<200; i++){
        line_data[i]=0x55;
    }
    for(int i=0; i<600; i++){
        eink_screen_send_row(line_data);
        eink_write_row(400);
    }
    eink_end_frame();
}

uint16_t eink_data_convert(uint8_t bpp, uint8_t data, uint8_t thresh);
void eink_row_convert(uint8_t *data_in, uint8_t *data_out, uint16_t start, uint16_t end, uint8_t bpp){
  for (int i=0;i<800; i+=4){
    if(i>end || (i+bpp)<start){ // Area que no s'ha d'escriure a 0
      data_out[i/4]=0;
      continue;
    }
    data_out[200-i/4]=eink_data_convert(bpp,data_in[(i*bpp)/8], 0x1 << (bpp-1));
  }
}
void eink_row_convert(uint8_t *data_in, uint8_t *data_out, uint8_t bpp){
  eink_row_convert(data_in, data_out, 0,800, bpp);
}
uint16_t eink_data_convert(uint8_t bpp, uint8_t data, uint8_t thresh){
  uint16_t r=0;
  switch (bpp){
    case 1:
        for(int i=0; i<8; i++){
          r |= (BLACK_COND(i%4, data&(0x1<<i)) | WHITE_COND(i%4, ~data&(0x1<<i)))<<(i/4);
        }
        return r;
    case 2:
        for(int i=0; i<4; i++){
          r |= (BLACK_COND(i, (data&(0xC0>>(i*2))) ) );/*|
                WHITE_COND(i, !(data&(0xC0>>(i*2)))));*/

        }
        return r;

  }
}

/****************************************************************************/
/*                                                                          */
/*                                 PATRONS                                  */
/*                                                                          */
/****************************************************************************/
/*uint8_t pattern[120][400];
void eink_chess(){
    //PMIC_pwrup();
for (int i=0; i<120; i++)
  for (int j=0; j<400; j++)
    pattern[i][j]=((i%60<30) ^ ((j%30)<15)) ? 0xff : 0;


    //eink_screen_write(pattern, 0, 60);
    eink_start_frame();

    for(int i=0; i<600; i++){
        eink_screen_send_row_BN(&pattern[i%120][0]);
        eink_write_row_BN(100);
    }
    eink_end_frame();


}*/
