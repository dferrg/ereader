 #include "SD.h"
#include "diskio.h"
uint8_t SDHC;
	BYTE n;
	DWORD csd[4];
	DWORD st, ed, csize;
void print(char* a);
uint32_t card_address;
char* itoa(uint32_t num, char* str, int base);
void sdio_transmit_block(uint8_t data[]);
void sdio_recv_block(uint8_t data[]);

void sdio_init(){
    rcc_periph_clock_disable(RCC_SDIO);

    //init GPIOs
    rcc_periph_clock_enable(RCC_GPIOC);
    rcc_periph_clock_enable(RCC_GPIOD);
    gpio_mode_setup(GPIOC, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO10);   //DAT2
        gpio_set_af(GPIOC, GPIO_AF12, GPIO10);
        //gpio_set_output_options(GPIOC, GPIO_OTYPE_OD, GPIO_OSPEED_100MHZ, GPIO10);

    gpio_mode_setup(GPIOC, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO11);   //DAT3
        gpio_set_af(GPIOC, GPIO_AF12, GPIO11);
        //gpio_set_output_options(GPIOC, GPIO_OTYPE_OD, GPIO_OSPEED_100MHZ, GPIO11);

    gpio_mode_setup(GPIOD, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO2);    //CMD
        gpio_set_af(GPIOD, GPIO_AF12, GPIO2);
        //gpio_set_output_options(GPIOD, GPIO_OTYPE_OD, GPIO_OSPEED_100MHZ, GPIO2);

    gpio_mode_setup(GPIOC, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO12);   //CLK
        gpio_set_af(GPIOC, GPIO_AF12, GPIO12);
        //gpio_set_output_options(GPIOC, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, GPIO12);

    gpio_mode_setup(GPIOC, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO8);   //DAT0
        gpio_set_af(GPIOC, GPIO_AF12, GPIO8);
        //gpio_set_output_options(GPIOC, GPIO_OTYPE_OD, GPIO_OSPEED_100MHZ, GPIO8);

    gpio_mode_setup(GPIOC, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO9);   //DAT1
        gpio_set_af(GPIOC, GPIO_AF12, GPIO9);
        //gpio_set_output_options(GPIOC, GPIO_OTYPE_OD, GPIO_OSPEED_100MHZ, GPIO9);


        rcc_periph_clock_enable(RCC_SDIO);

    //enable DMA if necessary

    //init SDIO
    SDIO_CLKCR = (SDIO_CLKCR_WIDBUS_1 |(SDIO_CLKCR_CLKDIV_MASK & 0x80));
                SDIO_CLKCR |= (SDIO_CLKCR_CLKEN);

    SDIO_POWER = SDIO_POWER_PWRCTRL_PWRON;

}
void uart_cmd(int n, uint32_t resp){
  char buf[10];
  print("CMD");
  itoa(n, buf, 10);
  print(buf);
  print(": ");
  itoa(resp, buf, 16);
  print(buf);
  print("\r\n");

}
static inline void delay_us(uint32_t t){
    for (uint32_t i=0; i<t*70; i++)
    __asm("nop");
}

//http://www.applelogic.org/files/SDIO.pdf
int SD_init(){

    uint32_t r=0, c=0;
    //posa la targeta en IDLE
    do{
        delay_us(100);
        sdio_cmd_send(52, 0x00000604, SDIO_CMD_WAITRESP_LONG);
        delay_us(10000);
        sdio_cmd_send(0, 0, SDIO_CMD_WAITRESP_LONG);
        delay_us(10000);
        c++;
        if(c>10){
            sdio_init();
            return SD_init();

        }
        //L'standard exigeix comprovar que es tracta d'una targeta de memòria
        //assumirem resposta afirmativa
    }while(sdio_cmd_send(8, 0x000001AA, SDIO_CMD_WAITRESP_LONG)!=0x1aa);
    delay_us(100);


    while(1){
        //Informa a la SD que la següent comanda no és standard (és propia de memories SD)
        sdio_cmd_send(55, 0, SDIO_CMD_WAITRESP_SHORT);


        //Llegeix les condicions de funcionament de la SD, principalment el voltatge i el mode d'adreçament (SDSC o SDHC)
        r = sdio_cmd_send(41, 0x40FF8000, SDIO_CMD_WAITRESP_LONG);
        //delay_us(100);


        if(r&0x80000000){
            SDHC=(r & 0x40000000) ? CT_BLOCK | CT_SDC2 : CT_SDC2;
            break;
        }
    }

    sdio_cmd_send(2, 0, SDIO_CMD_WAITRESP_LONG);

    r=sdio_cmd_send(3, 0, SDIO_CMD_WAITRESP_SHORT);

    card_address=r&0xFFFF0000;
    //Llegeig CSD. aporta informació sobre laSD com ara la capacitat.
    sdio_cmd_send(9, card_address, SDIO_CMD_WAITRESP_LONG);

    //selecciona la targeta
    sdio_cmd_send(7, card_address, SDIO_CMD_WAITRESP_SHORT);

    //estableix el bloc de dades en 512 bytes
    sdio_cmd_send(16, 512, SDIO_CMD_WAITRESP_SHORT);
    sdio_cmd_send(55, card_address, SDIO_CMD_WAITRESP_SHORT);
    sdio_cmd_send(6, 0x02, SDIO_CMD_WAITRESP_SHORT);

        SDIO_CLKCR = (SDIO_CLKCR_WIDBUS_4 |
                (SDIO_CLKCR_CLKDIV_MASK & 0x10) |
                SDIO_CLKCR_CLKEN);
    return 0;
}

void sdio_dma_init(){

}

uint32_t sdio_cmd_send(uint8_t cmd, uint32_t arg, uint8_t wait_resp){

    //SDIO_ICR=(SDIO_ICR_CCRCFAILC | SDIO_ICR_CTIMEOUTC | SDIO_ICR_CMDRENDC | SDIO_ICR_CMDSENTC);
    SDIO_ICR=0xFFFFFFFF;
    SDIO_ARG=arg;
    SDIO_CMD= ((cmd & SDIO_CMD_CMDINDEX_MASK) | SDIO_CMD_CPSMEN | wait_resp);
    //print("SENDING COMMAND\r\n");
    //delay_us(100000);
    /*if (wait_resp==SDIO_CMD_WAITRESP_NO_0 || wait_resp==SDIO_CMD_WAITRESP_NO_2)
        while (!(SDIO_STA & (SDIO_STA_CTIMEOUT | SDIO_STA_CMDSENT | SDIO_STA_CMDREND | SDIO_STA_CCRCFAIL)));
    else
        while (!(SDIO_STA & ( SDIO_STA_CMDREND | SDIO_STA_CCRCFAIL)));*/
    for (uint32_t i=0; i<200000; i++){
      __asm("nop;");
      if(!(SDIO_STA & SDIO_STA_CMDACT) && (SDIO_STA & ( SDIO_STA_CMDREND | SDIO_STA_CCRCFAIL)))
        break;

    }



    uint32_t response = sdio_response;
      //uart_cmd(cmd, response);
      return response;
}

void sdio_read_block(uint8_t data[], uint32_t address){
  /*if(!SDHC)
      address*=512;*/
      print("READ BLOCK\r\n");

  //SDIO_DCTRL |= 1<<11;
  SDIO_DCTRL = 0;
  for (uint32_t i=0; i<200000; i++)
    __asm("nop;");
    sdio_cmd_send(17, address, SDIO_CMD_WAITRESP_SHORT);
  //set block size
  SDIO_DCTRL |= 0x90;//2^9 = 512
  //set data timeout
  SDIO_DTIMER = 0xFFFFFFFF;
  //set data length
  SDIO_DLEN = 512;
  //set data direction
  SDIO_DCTRL |= ((0x2)); //set dir bit
  SDIO_ICR=(SDIO_ICR_CCRCFAILC | SDIO_ICR_CTIMEOUTC | SDIO_ICR_CMDRENDC | SDIO_ICR_CMDSENTC);
  //enable transmission
  SDIO_DCTRL |= 1;
  SDIO_ICR=0xFFFFFFFF;

  sdio_recv_block(data);

  while(!(SDIO_STA& (SDIO_STA_DBCKEND | SDIO_STA_DTIMEOUT | SDIO_STA_DCRCFAIL | SDIO_STA_STBITERR)));

  uart_cmd(67, SDIO_STA);
}
void sdio_write_block(uint8_t data[], uint32_t address){
    print("WRITE BLOCK\r\n");

    /*if(!SDHC)
        address*=512;*/

    //SDIO_DCTRL |= 1<<11;
    SDIO_DCTRL = 0;
  while(!(sdio_cmd_send(13, card_address, SDIO_CMD_WAITRESP_SHORT)&&0x100)){for(int i =0; i<20000; i++)__asm("nop;");};
  for (uint32_t i=0; i<200000; i++)
  __asm("nop;");

    sdio_cmd_send(24, address, SDIO_CMD_WAITRESP_SHORT);
    //set block size
    SDIO_DCTRL |= 0x90;//2^9 = 512
    //set data timeout
    SDIO_DTIMER = 0xFFFFFFFF;
    //set data length
    SDIO_DLEN = 512;
    //set data direction
    SDIO_DCTRL &= (~(0x2)); //clear dir bit
    SDIO_ICR=(SDIO_ICR_CCRCFAILC | SDIO_ICR_CTIMEOUTC | SDIO_ICR_CMDRENDC | SDIO_ICR_CMDSENTC);
    //enable transmission
    SDIO_DCTRL |= 1;
    SDIO_ICR=0xFFFFFFFF;

    sdio_transmit_block(data);

    while(!(SDIO_STA& (SDIO_STA_DBCKEND | SDIO_STA_DTIMEOUT | SDIO_STA_DCRCFAIL | SDIO_STA_STBITERR)));

    uart_cmd(67, SDIO_STA);
}
void sdio_write_blocks(uint8_t data[], uint32_t address, uint32_t n_blocks){
    SDIO_DCTRL = 0;
    print("WRITE MULTIPLE BLOCK\r\n");

  while(!(sdio_cmd_send(13, card_address, SDIO_CMD_WAITRESP_SHORT)&&0x100)){for(int i =0; i<20000; i++)__asm("nop;");};
  for (uint32_t i=0; i<200000; i++)
    __asm("nop;");
    //Multiple block write start
    sdio_cmd_send(25, address, SDIO_CMD_WAITRESP_SHORT);
    //set block size
    SDIO_DCTRL |= 0x90;//2^9 = 512
    //set data timeout
    SDIO_DTIMER = 0xFFFFFFFF;
    //set data length
    SDIO_DLEN = 512*n_blocks;
    //set data direction
    SDIO_DCTRL &= (~(0x2)); //clear dir bit
    SDIO_ICR=(SDIO_ICR_CCRCFAILC | SDIO_ICR_CTIMEOUTC | SDIO_ICR_CMDRENDC | SDIO_ICR_CMDSENTC);
    //enable transmission
    SDIO_DCTRL |= 1;
    SDIO_ICR=0xFFFFFFFF;

    //envia els blocs
    for (int i=0; i<n_blocks; i++){
        sdio_transmit_block(&data[i*512]);
    }
    while(SDIO_STA & SDIO_STA_TXACT);

    //espera a rebre la confirmació
    while(!(SDIO_STA& (SDIO_STA_DBCKEND | SDIO_STA_DTIMEOUT | SDIO_STA_DCRCFAIL | SDIO_STA_STBITERR)));

    //envia condició d'aturada
    sdio_cmd_send(12, 0, SDIO_CMD_WAITRESP_SHORT);

}
void sdio_read_blocks(uint8_t data[], uint32_t address, uint32_t n_blocks){
print("READ MULTIPLE BLOCK\r\n");
  SDIO_DCTRL = 0;
  for (uint32_t i=0; i<2000000; i++)
    __asm("nop;");
  sdio_cmd_send(18, address, SDIO_CMD_WAITRESP_SHORT);
  //set block size
  SDIO_DCTRL |= 0x90;//2^9 = 512
  //set data timeout
  SDIO_DTIMER = 0xFFFFFFFF;
  //set data length
  SDIO_DLEN = 512*n_blocks;
  //set data direction
  SDIO_DCTRL |= ((0x2)); //set dir bit
  SDIO_ICR=(SDIO_ICR_CCRCFAILC | SDIO_ICR_CTIMEOUTC | SDIO_ICR_CMDRENDC | SDIO_ICR_CMDSENTC);
  //enable transmission
  SDIO_DCTRL |= 1;
  SDIO_ICR=0xFFFFFFFF;

    for (int i=0; i<n_blocks; i++){
        sdio_recv_block(&data[i*512]);
    }

  while(!(SDIO_STA& (SDIO_STA_DBCKEND | SDIO_STA_DTIMEOUT | SDIO_STA_DCRCFAIL | SDIO_STA_STBITERR)));

  uart_cmd(67, SDIO_STA);
}


void sdio_transmit_block(uint8_t data[]){
    uint32_t *fifo=SDIO_FIFO;
    uint32_t *data32=(uint32_t*)data;
    int i=0;
    while(!(SDIO_STA & (SDIO_STA_TXUNDERR | SDIO_STA_DTIMEOUT | SDIO_STA_DCRCFAIL | SDIO_STA_DATAEND))){
        if(!(SDIO_STA&SDIO_STA_TXFIFOF)){
            SDIO_FIFO=data32[i];//data32[offset];
            i++;
        }
        if(i==128) break;

    }
}

void sdio_recv_block(uint8_t data[]){
    uint32_t *fifo=SDIO_FIFO;
    uint32_t *data32=(uint32_t*)data;
    int i=0;
    while(!(SDIO_STA & (SDIO_STA_DCRCFAIL | SDIO_STA_DTIMEOUT | SDIO_STA_RXOVERR | SDIO_STA_DATAEND | SDIO_STA_RXFIFOHF | SDIO_STA_STBITERR))){
        if(SDIO_STA&SDIO_STA_RXDAVL){
            data32[i]=SDIO_FIFO;
            i++;
        }
        if(i==128) break;
    }

}

uint32_t sdio_get_sector_count(){
	csd[0]=sdio_cmd_send(9, 0, SDIO_CMD_WAITRESP_LONG);
	csd[1]=SDIO_RESP2;
	csd[2]=SDIO_RESP3;
	csd[3]=SDIO_RESP4;

	BYTE *csd8=(BYTE*)csd;

	if ((csd8[0] >> 6) == 1) {	/* SDC CSD ver 2 */
		csize = csd8[9] + ((WORD)csd8[8] << 8) + ((DWORD)(csd8[7] & 63) << 16) + 1;
		return csize << 10;
	} else {					/* SDC CSD ver 1 or MMC */
		n = (csd8[5] & 15) + ((csd8[10] & 128) >> 7) + ((csd8[9] & 3) << 1) + 2;
		csize = (csd8[8] >> 6) + ((WORD)csd8[7] << 2) + ((WORD)(csd8[6] & 3) << 10) + 1;
		return csize << (n - 9);
	}

}

uint32_t sdio_get_block_size(){
	if (SDHC) {	/* SDC ver 2+ */
		csd[0]=sdio_cmd_send(13, 0, SDIO_CMD_WAITRESP_LONG);
		csd[1]=SDIO_RESP2;
		csd[2]=SDIO_RESP3;
		csd[3]=SDIO_RESP4;
	BYTE *csd8=(BYTE*)csd;

		return 16UL << (csd8[10] >> 4);


	} else {					/* SDC ver 1 or MMC */
		csd[0]=sdio_cmd_send(13, 0, SDIO_CMD_WAITRESP_LONG);
		csd[1]=SDIO_RESP2;
		csd[2]=SDIO_RESP3;
		csd[3]=SDIO_RESP4;
	BYTE *csd8=(BYTE*)csd;
		return (((csd8[10] & 63) << 1) + ((WORD)(csd8[11] & 128) >> 7) + 1) << ((csd8[13] >> 6) - 1);
	}
}

void sdio_read_csd(uint32_t buff[]){
	((DWORD*)buff)[0]=sdio_cmd_send(9, 0, SDIO_CMD_WAITRESP_LONG);
	((DWORD*)buff)[1]=SDIO_RESP2;
	((DWORD*)buff)[2]=SDIO_RESP3;
	((DWORD*)buff)[3]=SDIO_RESP4;
}

void sdio_read_cid(uint32_t buff[]){
	((DWORD*)buff)[0]=sdio_cmd_send(10, 0, SDIO_CMD_WAITRESP_LONG);
	((DWORD*)buff)[1]=SDIO_RESP2;
	((DWORD*)buff)[2]=SDIO_RESP3;
	((DWORD*)buff)[3]=SDIO_RESP4;
}
