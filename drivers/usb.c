#include "./usb.h"
#include <libopencm3/stm32/exti.h>
#include <libopencm3/stm32/f4/nvic.h>
#define NULL 0

//Descriptor de dsipositiu. defineix classe DFU, VID i PID, i USB v2.0
static const struct usb_device_descriptor dev = {
  bLength : USB_DT_DEVICE_SIZE,
  bDescriptorType : USB_DT_DEVICE,
  bcdUSB : 0x0200,
  bDeviceClass : 0, //multiclass
  bDeviceSubClass : 0,
  bDeviceProtocol : 0,
  bMaxPacketSize0 : 64,
  idVendor : 0xf055,
  idProduct : 0x1312,
  bcdDevice : 0x0200,
  iManufacturer : 1,
  iProduct : 2,
  iSerialNumber : 3,
  bNumConfigurations : 1,
};

static const char *usb_strings[] = {
  "dferrg's",
  "Ook! eReader",
  "40760SC4ACAB",
	"@Internal Flash   /0x08000000/1*016Ka,3*016Kg,1*064Kg,7*128Kg",
};

/* Notes about the dfuse string above:
 * /<start_address>/<number>*<page_size><multiplier><memtype>
 *  <number>: how many pages
 *  <page_size>: self explanatory
 *  <multiplier>: 'B'(ytes), 'K'(ilobytes), 'M'(egabytes)
 *  <memtype>: the bottom three bits are significant:
 *             writeable|erasable|readable
 * subsequent blocks separated by commas
 *
 * Using the internal page size: "@Internal Flash   /0x08000000/64*128Ba,448*128Bg"
 * Using 1K blocks: "@Internal Flash   /0x08000000/8*001Ka,56*001Kg"
 */

//Descriptor d'interficies CDC i DFU
const struct usb_interface ifaces[3] = {{
	NULL, 1, NULL, comm_iface
}, {
	NULL, 1, NULL, data_iface
}, {
	NULL, 1, NULL, dfu_iface
} };
//descriptor de configuracio
const struct usb_config_descriptor config = {
	bLength : USB_DT_CONFIGURATION_SIZE,
	bDescriptorType : USB_DT_CONFIGURATION,
	wTotalLength : 0,
	bNumInterfaces : 3,
	bConfigurationValue : 1,
	iConfiguration : 0,
	bmAttributes : 0x80,
	bMaxPower : 0x32,

	interface : ifaces, //interficie DFU
};



static enum usbd_request_return_codes usb_control_request(usbd_device *usbd_dev, struct usb_setup_data *req, uint8_t **buf,
		uint16_t *len, void (**complete)(usbd_device *usbd_dev, struct usb_setup_data *req))
{
    switch(req->wIndex & 0x0F){
        case 0:
            return cdcacm_control_request(usbd_dev, req, buf, len, complete);
            break;
        case 1:
            return cdcacm_control_request(usbd_dev, req, buf, len, complete);
            break;
        case 2:
            return usbdfu_control_request(usbd_dev, req, buf, len, complete);
            break;
    }
}
static void usb_set_config(usbd_device *usbd_dev, uint16_t wValue)
{
	(void)wValue;

    cdcacm_set_config(usbd_dev, wValue);

	usbd_register_control_callback(
				usbd_dev,
				USB_REQ_TYPE_CLASS | USB_REQ_TYPE_INTERFACE,
				USB_REQ_TYPE_TYPE | USB_REQ_TYPE_RECIPIENT,
				usb_control_request);
}

usbd_device *usbd_dev=NULL;
uint8_t usbd_control_buffer[1024];
void otg_fs_isr(void) {
  usbd_poll(usbd_dev);
}

void usb_connected(void){
		exti_reset_request(EXTI15);
	usbd_dev = usbd_init(&otgfs_usb_driver,
                     &dev,
                     &config,
                     usb_strings,
                     4,
                     usbd_control_buffer,
                     sizeof(usbd_control_buffer));

    OTG_FS_GCCFG |= OTG_GCCFG_NOVBUSSENS;

	usbd_register_set_config_callback(usbd_dev, usb_set_config);

	nvic_enable_irq(NVIC_OTG_FS_IRQ);

}
void usb_exti_setup(){
	rcc_periph_clock_enable(RCC_GPIOD);
    //rcc_periph_clock_enable(RCC_AFIO);
    gpio_mode_setup(GPIOD, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO15);
    	/* Enable EXTI0 interrupt. */

    rcc_periph_clock_enable(RCC_SYSCFG);
    exti_select_source(EXTI15, GPIOD);

	exti_set_trigger(EXTI15, EXTI_TRIGGER_RISING);
	exti_enable_request(EXTI15);
	nvic_enable_irq(NVIC_EXTI15_10_IRQ);
    if(gpio_get(GPIOD,GPIO15))
        usb_connected();
}

void usb_init(){

    rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_OTGFS);

    gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO11 | GPIO12);
    gpio_mode_setup(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO8);
	gpio_set_af(GPIOA, GPIO_AF10, GPIO11 | GPIO12);
    gpio_set(GPIOA, GPIO8);

    usb_exti_setup();
}
