#include "FreeRTOS.h"
#include "task.h"
#include "epdsys.h"
#include "markdown.h"
#include "text.h"
#include "explorer.h"
#include "buttons.h"
#include "image.h"
#ifndef PC_SIM
    #include "clock.h"
    #include <libopencm3/stm32/gpio.h>
    #include <libopencm3/cm3/scb.h>
    #include <libopencm3/stm32/usart.h>
    #include "TPS65186.h"
    #include "ED060SC4.h"
    #include "SD.h"
    #include "usb.h"
    #include "power.h"

#else
    #include <stdint.h>
        #include <iostream>
    #include "finestra.h"
    #include <stdlib.h>     /* exit, EXIT_FAILURE */
    #include "dummy.h"
    typedef uint8_t bool;
    #define false 0
    #define true 1
    typedef uint8_t FATFS;
#endif

FATFS fs;

static void task1(void *args __attribute__((unused))) {
//  parser* p=new mdParser("README.md");
//explorer *men=new explorer("/home/debian11/dani/Documents/");

//mdParser* m=new mdParser("/ASD.TXT");
explorer* e=new explorer("./");
int p=0;
    while(1){

        uint32_t g2,g=ALL_BLACK;
        writeArea_t a={0,0,0,0};

        //s_eink_clear_cycle(a, 0x0F);
         p++;
         taskENTER_CRITICAL();     // print("driver\r\n");

        //m->readPage(p);
        taskEXIT_CRITICAL();     // print("driver\r\n");

        //s_eink_clear_cycle(a, 0x0F);

         //vTaskDelay(2000);

            //gpio_toggle(GPIOD,GPIO7);
            //g=~g;
            //m->render();
            //m->render();
            e->start();
//print("task\r\n");
vTaskDelay(5000);

    }

}


float a=5.0;
float b=3.0;
char buf[20];
char img[]="asdf.jpg";

int main(){
          int re;
          char buff[10];
  #ifndef PC_SIM

    init_rcc();
    rcc_periph_clock_enable(RCC_GPIOD);
  	gpio_mode_setup(GPIOD, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO7);
  	gpio_clear(GPIOD, GPIO7);
  	//usart_setup();
  	usb_init();
    //while
    print("\r\n");
    //gpio_set(GPIOD, GPIO7);

//    buttons_exti_setup();
    power_init();
    sdio_init();
    for(int i=0; i<10000; i++) __asm("nop;");


    re=f_mount(&fs, "/", 1);
    gpio_set(GPIOD, GPIO7);

    itoa(re,buff, 16);
    print("\r\n");
    print("\n\rFatFs mount: ");
    print("0x");
    print(buff);
    print("\n\r");
    print("\r\n");
      epdsys_init();
      PMIC_pwrup();

      eink_clear(0x0F,100);
      for(int i=0; i<10000000; i++) __asm("nop;");

      PMIC_standby();
      for(int i=0; i<10000000; i++) __asm("nop;");



    print("ok\r\n");
  #endif
  screen_init();
  buttons_exti_setup();

/*  itoa((int)a*b, buff,10);
uint8_t data[512];
    sdio_read_block(data, 0);
    sdio_write_blocks(data,0,1);
    sdio_read_block(data, 0);
    sdio_write_block(data,0);
  jpgImage *a = new jpgImage(img);
      a->initialize();
  a->decode("/asdf.raw");
  while(1);*/
  /*PMIC_pwrup();

  eink_clear(0x0F,100);
  for(int i=0; i<10000000; i++) __asm("nop;");

  PMIC_standby();*/
  uint32_t r=xTaskCreate(task1, "LED", 1000, NULL, 2, NULL);
	vTaskStartScheduler();
	while(1);


}




#ifndef PC_SIM
void vApplicationIdleHook( void ){
            sleep();
      //while(1);
}

extern void vPortSVCHandler(void) __attribute__((naked));
extern void xPortPendSVHandler(void) __attribute__((naked));
extern void xPortSysTickHandler(void);

void sv_call_handler(void) { vPortSVCHandler(); }

void pend_sv_handler(void) { xPortPendSVHandler(); }

void sys_tick_handler(void) { xPortSysTickHandler(); }
#else
extern "C"{
  void vApplicationIdleHook( void ){
  }
}
#endif
